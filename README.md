McSwiggen, et. al.
A collection of scripts to quantify:
1) Single particle tracking data with implementation of Spot On (Hansen et. al. 2018)
2) Fluorescence In Situ Hybridization data after manual annotation
3) FRAP and FLIP analysis

David McSwiggen, April 2019
Questions about this code should be directed to dmcswiggen@berkeley.edu
