%%%%%% FRAP Quant %%%%%%%%%%%%%%%%%
%%%%%%%Copyright (C) 2018 David McSwiggen

% modified from SegmentQuantifyMovie_v2.m, Anders Sejr Hansen, Jan 2016

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script requires .tiff files of FRAP image sequences. One
%%%%%%% manually identifies the center pixel of the bleach spot, the
%%%%%%% nuclear intensity, and a background pixel. Then one manually
%%%%%%% accounts for drift by changing the "movement" vector. The inputs
%%%%%%% here match the raw data from McSwiggen et at 2019 FRAP data.

%%


clear; clc; close all;

%The purpose of this movie is to analyze FRAP data and quantify the
%intensity of a circular bleached region that is moving a little bit, using
%just manual adjustment of the circle movement. 
%imshow(images(:,:,1), []);

%VERSION 2 - SEGMENT THE WHOLE NUCLEUS
%Use inital fluorescence in the bleach spot region to normalize the final
%recovery - use a slightly larger circle to correct for noise. Use an extra
%radius of:
ExtraRadius = 0;
%Adjust the intensity threshold used across the movie to account for
%photobleaching. Assume ~18% photobleach over the 300 frames.
ExpFactor = 5; %exp(-endFrame/(ExpFactor*endFrame) = 0.8187

%Use WhatToDo = 1 to find BleachCentre. Use WhatToDo = 2 to quantify BleachCentre Movement
WhatToDo = 2;
SaveVal = 1; %Change to 1 if you want to save

%Muller, Wach, McNally, BioPhys J, 2008: Can ignore Gaussian shape of photobleach spot when the recovery is slow. 

SampleName = '3-4hpi_D2_002';
file = '3-4hpi_250msec-frame_561-speed12_11px-speed10_002.tif';
MakeMovie = 0;

%Define the FRAP circle:
FirstBleachFrame = 16;
FrameRate = 4; 
CircleRadius = 11; 
SmallCircleRadius = 6;
NewTime = 0;
%Define FRAP Cirlce movement
FrameUpdate = 40; %Update every 40th frame

%For some of the U2-OS movies, the background was hidden. Need to adjust
%the background manually (Otherwise FRAP recovery is negative in first
%time-point). Use:
U2OS_background = 0;

path = '/Volumes/DARZACQ_1/Imaging/DM_2_2/TIFF_stacks/Day_2/';

%SAMPLE SPECIFIC INFORMATION

%U2OS, uninfected, Day 1
if strcmp(SampleName, 'Uninfected_D1_001');
    display(SampleName);
    BleachCentre = [248, 285];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [423, 340];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];

elseif strcmp(SampleName, 'Uninfected_D1_002');
    display(SampleName);
    BleachCentre = [357, 348];
    IntThres = 50; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [487, 382];
    Movement = [0,0; 0,0; 0,0; -1,0; 0,0; 0,0; 0,0; -1,0; 0,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; -1,0; 0,0; -1,0; 0,0; -1,0; 0,0; -1,0; 0,0];

elseif strcmp(SampleName, 'Uninfected_D1_003');
    display(SampleName);
    BleachCentre = [240, 262];
    IntThres = 45; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [373, 259];
    Movement = [0,0; 0,0; -1,0; 0,0; -1,-1; 0,0; 0,0; -1,0; 0,0; 0,0; 0,0; -1,-1; 0,0; 0,0; 0,0; -1,-1; 0,0; 0,0; 0,0; -1,-1; 0,0; 0,0; -1,-1; 0,0; 0,0; -1,-1; 0,0; 0,0; -1,-1; 0,0];

elseif strcmp(SampleName, 'Uninfected_D1_004');
    display(SampleName);
    BleachCentre = [235, 286];
    IntThres = 45; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [313, 380];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; -1,1; 0,0; 0,0; -1,1; 0,0; 0,0; -1,0; 0,0; -1,1; 0,0; 0,0; -1,1; 0,0; 0,0; 1,-1; 0,0; 0,0; 1,0; 0,0; 1,-1; 0,0; 1,0; 0,0; 1,-1];

elseif strcmp(SampleName, 'Uninfected_D1_005');
    display(SampleName);
    BleachCentre = [274, 267];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [167, 346];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 1,1; 0,0; 0,0; 1,1; 0,0; 0,0; 1,0; 0,0; 1,1; 0,0; 1,0; 1,0; 0,0; 1,1; 0,0];

elseif strcmp(SampleName, 'Uninfected_D1_006');
    display(SampleName);
    BleachCentre = [265, 268];
    IntThres = 45; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [407, 316];
    Movement = [0,0; 0,0; 1,0; 0,0; 1,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 1,0; 0,0; 1,0; -1,0; -1,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; -1,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,1];
    
elseif strcmp(SampleName, 'Uninfected_D1_007');
    display(SampleName);
    BleachCentre = [326, 307];
    IntThres = 50; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [416, 330];
    Movement = [0,0; -1,0; -1,0; -1,0; -1,0; -1,0; -1,0; -1,0; -1,0; 0,0; -1,0; -1,0; -1,0; 0,0; -1,0; -1,0; -1,0; -1,0; 0,0; 0,1; 0,0; 0,0; -1,1; 0,0; 0,0; 0,1; 0,0; 0,0; 0,0; 0,0];

%U2OS, uninfected, Day 2

elseif strcmp(SampleName, 'Uninfected_D2_001');
    display(SampleName);
    BleachCentre = [214, 321];
    IntThres = 75; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [266, 257];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];

elseif strcmp(SampleName, 'Uninfected_D2_002');
    display(SampleName);
    BleachCentre = [248, 307];
    IntThres = 75; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [407, 404];
    Movement = [0,0; 0,0; 1,1; 0,0; 1,1; 0,0; 1,1; 0,0; 0,1; 0,1; 1,1; 1,1; 1,1; 1,1; 0,0; 1,1; 0,0; 1,1; 1,1; 1,1; 0,0; 0,1; 0,0; 1,1; 0,0; 1,1; 0,0; 1,1; 0,0; 1,1];

elseif strcmp(SampleName, 'Uninfected_D2_003');
    display(SampleName);
    BleachCentre = [207, 231];
    IntThres = 52; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [394, 353];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];

elseif strcmp(SampleName, 'Uninfected_D2_004');
    display(SampleName);
    BleachCentre = [276, 235];
    IntThres = 50; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [440, 399];
    Movement = [0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0];

elseif strcmp(SampleName, 'Uninfected_D2_005');
    display(SampleName);
    BleachCentre = [320, 258];
    IntThres = 52; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [120, 346];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,1; 0,0; 0,1; 0,0; 0,0; 0,1; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];

elseif strcmp(SampleName, 'Uninfected_D2_006');
    display(SampleName);
    BleachCentre = [278, 337];
    IntThres = 58; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [169, 410];
    Movement = [0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 1,0; 0,0; 1,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 0,0];

elseif strcmp(SampleName, 'Uninfected_D2_007');
    display(SampleName);
    BleachCentre = [250, 271];
    IntThres = 52; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [110, 353];
    Movement = [0,0; 0,0; 0,0-1; 0,0; 0,0; 0,-1; 0,0; 0,0; 0,-1; 0,0; 0,0; 0,0; 0,-1; 0,0; 0,0; 0,-1; 0,0; 0,0; 0,0; 0,-1; 0,0; 0,0; 0,0; 0,-1; 0,0; 0,0; 0,-1; 0,0; 0,0; 0,-1];

%3hpi, Day2
elseif strcmp(SampleName, '3-4hpi_D2_001');
    display(SampleName);
    BleachCentre = [363, 230];
    IntThres = 55; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [125,172];
    Movement = [1,0; 1,0; 0,-1; 1,0; 0,-1;
                0,-1; 1,0; 0,-1; 0,0; 1,-1;
                0,0; 1,-1; 0,0; 0,-1; 0,0;
                0,-1; 0,0; 0,0; 0,-1; 0,0;
                1,0; 0,-1; 0,0; 1,-1; 0,-1;
                0,0; 1,0; 0,-1; 0,0; 0,0];
            
elseif strcmp(SampleName, '3-4hpi_D2_002');
    display(SampleName);
    BleachCentre = [250, 356];
    IntThres = 50; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [125,386];
    Movement = [0,0; -1,0; 0,0; 0,0; 0,1;
                0,0; 0,0; 0,1; 0,0; -1,0;
                -1,0; 0,0; 0,0; -1,0; 0,0;
                0,0; -1,0; 0,0; 0,1; 0,0;
                -1,0; 0,0; 0,0; -1,0; 0,0;
                -1,0; 0,-1; 0,0; 0,0; -1,0];            


%U2OS, infected, 4hpi, Day 1

elseif strcmp(SampleName, '4-5hpi_D1_001');
    display(SampleName);
    BleachCentre = [246, 270];
    IntThres = 75; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [132, 367];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,-1; 0,0; 0,-1; 0,0; 0,0; 0,0; 0,-1; 0,0; 0,-1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,-1; 0,0; 0,-1; 0,-1];

elseif strcmp(SampleName, '4-5hpi_D1_002');
    display(SampleName);
    BleachCentre = [336, 239];
    IntThres = 52; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [109, 358];
    Movement = [0,0; -1,-1; 0,-1; -1,0; 0,0; -1,1; 0,1; 0,1; -1,1; 0,1; 0,0; 0,1; 0,0; 0,0; 0,1; 1,0; 0,0; 1,1; 1,0; 0,1; 1,0; 0,1; 0,0; 0,-1; 0,0; 0,-1; 0,0; 0,-1; 0,0; 0,0];

elseif strcmp(SampleName, '4-5hpi_D1_003');
    display(SampleName);
    BleachCentre = [331, 237];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [105, 167];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,1; 0,0; 0,1; 0,0; 0,0; 0,1; 0,0; 0,0; 0,1; 0,0; 0,0; 0,1; 0,0; 0,1; 0,0; 0,0; 0,0; 0,-1; 1,0; 0,-1; 0,0; -1,0; 0,-1; 0,0; -1,-1; -1,-1];

elseif strcmp(SampleName, '4-5hpi_D1_004');
    display(SampleName);
    BleachCentre = [332, 323];
    IntThres = 20; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [224, 332];
    Movement = [1,0; 1,1; 1,1; 1,1; 1,1; 
                1,0; 1,1; 1,1; 1,0; 1,1; 
                1,1; 1,0; 1,1; 1,1; 0,1; 
                1,0; 1,1; 1,1; 1,0; 1,1; 
                1,1; 0,0; 1,0; 0,0; 0,0; 
                0,0; 0,0; 0,0; 0,0; 0,0];
            
elseif strcmp(SampleName, '4-5hpi_D1_005');
    display(SampleName);
    BleachCentre = [289, 327];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [176, 356];
    Movement = [0,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 1,0; 0,0; 1,0; 0,0; 1,1; 1,0; 0,1; 1 ,0; 0,1; 0,0; 0,1; 0,1; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 1,0; 0,0; 0,0];

elseif strcmp(SampleName, '4-5hpi_D1_006');
    display(SampleName);
    BleachCentre = [237, 338];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [94, 219];
    Movement = [0,0; 0,0; -1,0; 0,0; -1,0; 0,0; 0,0; 1,0; 0,0; 1,0; 1,0; 1,0; 1,0; 1,0; 1,0; 1,0; 1,0; 0,0; 1,0; -1,0; -1,0; -1,0; -1,0; -1,0; -1,0; -1,0; 0,0; -1,0; 0,0; -1,0];

elseif strcmp(SampleName, '4-5hpi_D1_007');
    display(SampleName);
    BleachCentre = [165, 208];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [331,146];
    Movement = [0,0; 0,1; 0,-1; 0,0; 0,1; -1,0; 0,0; -1,0; 0,0; 0,0; 0,-1; 0,0; 0,-1; 0,0; 0,-1; 0,0; 1,0; 1,-1; 0,0; 1,0; 1,0; 0,0; 1,0; 0,-1; 0,0; 1,0; 1,0; 1,0; 1,0; 0,0];

%U2OS, infected, 4hpi, Day 2

elseif strcmp(SampleName, '4-5hpi_D2_001');
    display(SampleName);
    BleachCentre = [280, 255];
    IntThres = 78; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [357, 394];
    Movement = [0,0; 1,0; 0,0; 1,0; 0,0; 1,0; 1,0; 1,0; 2,0; 1,0; 2,0; 1,0; 2,0; 2,0; 2,0; 3,0; 5,0; 3,0; 5,0; 4,0; 5,0; 6,0; 6,0; 7,0; 6,0; 6,0; 3,0; 2,0; 2,0; 2,0];

elseif strcmp(SampleName, '4-5hpi_D2_002');
    display(SampleName);
    BleachCentre = [210, 247];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [368, 176];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; -1,0; 0,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];

elseif strcmp(SampleName, '4-5hpi_D2_003');
    display(SampleName);
    BleachCentre = [179, 287];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [368, 330];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,-1; 0,0; 0,-1; 0,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 1,0; 0,0; 1,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0];

elseif strcmp(SampleName, '4-5hpi_D2_004');
    display(SampleName);
    BleachCentre = [124, 214];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [57, 315];
    Movement = [0,0; 0,0; -1,0; -1,0; 0,0; -1,0; 0,0; 0,1; 0,0; 0,-1; 0,0; 0,-1; -1,0; 0,-1; -1,0; 0,-1; 0,0; 0,0; -1,-1; 0,0; 0,-1; -1,0; 0,-1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,1];

elseif strcmp(SampleName, '4-5hpi_D2_005');
    display(SampleName);
    BleachCentre = [309, 323];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [44, 392];
    Movement = [0,0; 1,0; 1,0; 1,0; 0,0; 0,-1; 0,0; 0,-1; 0,-1; -1,0; 0,-1; -1,0; 0,-1; -1,0; 0,-1; 0,-1; -1,-1; 0,-1; 0,-1; -1,-1; 0,-1; 1,-1; 1 ,1; 1,1; 1,0; 0,1; 1,0; 0,1; 1,0; 0,0];

elseif strcmp(SampleName, '4-5hpi_D2_006');
    display(SampleName);
    BleachCentre = [324, 261];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [338, 346];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,1; 0,0; 1,0; 0,0; 1,0; 0,1; 1,0; 0,1; 0,0; 1,0; 0,0; 0,0; -1,0; 0,0; 0,1; 0,0; 1,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];
    

%U2OS, infected, 5hpi, Day 1

elseif strcmp(SampleName, '5-6hpi_D1_001');
    display(SampleName);
    BleachCentre = [194, 229];
    IntThres = 44 ; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [86, 231];
    Movement = [0,-1; 1,-1; 0,-1; 1,-1; 0,-1; 0,1; 1,0; 1,0; 1,0; 1,0; 0,0; 1,0; 1,1; 0,1; 1,1; 0,1; 1,1; 1,0; 1,-1; 0,0; 2,0; 1,0; 2,-1; 1,-1; 2,-1; 1,-1; 2,-1; 1,0; 2,0; 1,0];

elseif strcmp(SampleName, '5-6hpi_D1_002');
    display(SampleName);
    BleachCentre = [223, 366];
    IntThres = 44 ; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [98, 192];
    Movement = [-1,-1; -1,-1; -1,-1; 1,-1; 0,-2; 0,-2; 0,-1; 0,1; 1,1; 1,2; 0,1; 0,0; 0,0; 0,1; 1,0; 0,0; 1,0; 0,0; 0,0; -1,0; 0,-1; -1,-1; 0,-1; -1,0; 0,-1; -1,0; 0,-1; -1,0; 0,-1; 0,0];

elseif strcmp(SampleName, '5-6hpi_D1_003'); %This movie is only 1027 frames! Maybe not able to include in quant
    display(SampleName);
    BleachCentre = [356, 280];
    IntThres = 44 ; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [139, 268];
    Movement = [0,0; 0,1; 0,0; 0,1; 0,1; 0,1; 0,1; 0,1; -1,1; 0,1; -1,1; 0,1; -1,1; 0,1; 0,1; 0,1; -1,1; 0,1; 0,2; -1,1; 0,2; 0,1; 0,1; 0,0];

elseif strcmp(SampleName, '5-6hpi_D1_004');
    display(SampleName);
    BleachCentre = [248, 268];
    IntThres = 40 ; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [152, 248];
    Movement = [0,0; -1,1; -1,1; 0,1; -1,1; 0,0; -1,0; -1,0; 0,1; 0,0; 0,1; 0,0; -1,0; 0,1; -1,0; 0,1; 0,0; 0,1; -1,0; 0,1; -1,0; 0,1; -1,0; 0,1; 0,0; 0,1; 0,0; 0,1; 0,0; 0,1];

elseif strcmp(SampleName, '5-6hpi_D1_005');
    display(SampleName);
    BleachCentre = [352, 273];
    IntThres = 35; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [128, 361];
    Movement = [0,0; 0,0; 0,-1; 0,0; 0,0; 0,-1; 0,-1; 0,0; 0,-1; 0,0; 0,1; 0,1; 0,1; 0,1; 0,1; 0,0; 0,0; 0,1; 0,0; 0,1; 0,0; 0,0; 0,1; 0,0; 0,0; 0,1; 0,0; 0,0; 0,0; 0,0];

%U2OS, infected, 5hpi, Day 2

elseif strcmp(SampleName, '5-6hpi_D2_001');
    display(SampleName);
    BleachCentre = [143, 314];
    IntThres = 35; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [57, 310];
    Movement = [0,0; 0,0; 0,0; -1,0; 0,0; 0,0; 0,0; 0,0; 0,1; 0,0; -1,0; 0,0; 0,0; 0,1; 0,0; -1,0; 0,0; -1,1; 0,0; -1,0; 0,1; -1,0; 0,0; -1,0; 0,1; 0,0; -1,0; 0,1; -1,0; 0,0];

elseif strcmp(SampleName, '5-6hpi_D2_002');
    display(SampleName);
    BleachCentre = [287, 299];
    IntThres = 28; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [133, 114];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,1; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0];

elseif strcmp(SampleName, '5-6hpi_D2_003');
    display(SampleName);
    BleachCentre = [290, 301];
    IntThres = 45; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [64, 389];
    Movement = [0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; 0,0; -1,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0];

elseif strcmp(SampleName, '5-6hpi_D2_004'); %only 1116 frames. May need to throw out
    display(SampleName);
    BleachCentre = [287, 260];
    IntThres = 30; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [177, 190];
    Movement = [-1,-1; 0,-1; 0,0; 0,-1; 0,0; 0,-1; 0,0; 0,-1; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; 0,0; -1,-1; 0,-1; -1,-1; 0,-1; -1,-1; 0,-1; -1,-1; 0,-1; -1,-1; 0,-1; -1,-1; 0,-1; -1,-1; 0,-1];

elseif strcmp(SampleName, '5-6hpi_D2_005');
    display(SampleName);
    BleachCentre = [223, 337];
    IntThres = 30; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [376, 207];
    Movement = [0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; 1,0; 0,0; 1,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0; 0,0; 1,0; 0,0; 0,0; 0,0];


else
    error('SampleName was not found. Please check your SampleName again');
end 
%%%%%%%%%%%%%% LOAD THE DATA %%%%%%%%%%%%%



%Open the file using the BioFormats package
%temp = bfopen([path, file]);
[stack, nbImages] = tiffread([path, file]);

%Convert to a matrix
%images = zeros(size(data{1,1},1), size(data{1,1},2), size(data,1));
images = zeros(stack(1,1).height, stack(1,1).width, size(stack,2));
ImHeight = stack(1,1).height; ImWidth = stack(1,1).width; 
for i=1:size(images,3)
    images(:,:,i) = stack(1,i).data;
end
if NewTime == 0
    time = -(FirstBleachFrame-1)/FrameRate:1/FrameRate:(size(images, 3)-FirstBleachFrame)/FrameRate;
elseif NewTime == 1 %mRAD21 JF646 movies start at 10 s
    time = [-(FirstBleachFrame-2)/FrameRate:0 10:(size(images, 3)-FirstBleachFrame+10)/FrameRate];
end

%Make smoothed images for segmentation of the nucleus
%Perform gaussian smoothing
smooth_filter = fspecial('gaussian', [6 6], 2);
smooth_images = zeros(stack(1,1).height, stack(1,1).width, size(stack,2));
for i=1:size(images,3)
    smooth_images(:,:,i) = imfilter(images(:,:,i), smooth_filter);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%WhatToDo = 1: determine bleach region
if WhatToDo == 1
    %Define the circle:
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    BW_circle = sqrt((rr-BleachCentre(1,1)).^2+(cc-BleachCentre(1,2)).^2)<=CircleRadius;
    %imshow(BW_circle) 
    figure('position',[100 100 700 250]); %[x y width height]
    subplot(1,3,1);
    imshow(images(:,:,FirstBleachFrame), []);
    
    subplot(1,3,2);
    overlay1 = imoverlay(mat2gray(images(:,:,FirstBleachFrame)), bwperim(BW_circle), [.3 1 .3]);
    imagesc(overlay1);
    title('Matching a circle to the region');
    
    %Make circles for background
    %Threshold to identify the nucleus
    Nucleus_binary = smooth_images(:,:,FirstBleachFrame) > IntThres*exp(-FirstBleachFrame/(ExpFactor*length(time)));
    %Now fill in any small holes in the middle of the nucleus:
    Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
    subplot(1,3,3);
    overlay2 = imoverlay(mat2gray(images(:,:,FirstBleachFrame)), bwperim(Nucleus_binary_filled), [1 0 0]);
    imagesc(overlay2);
    title('Nucleus segmentation');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%WhatToDo = 2: Determine Bleach Movement
if WhatToDo == 2
    FRAP_circle = zeros(1, round(size(images,3)/FrameUpdate));
    %Figure out how much the bleach circle moved
    for n=1:length(FRAP_circle)
        CumMove = Movement(1:n,:); %The cumulative movement of the circle
        CurrCentre = BleachCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
        currBackgroundCentre = BackgroundCentre;
        
        %Update the circle
        [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
        BW_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=CircleRadius;

        
        %Find the pixels inside the circle:
        [row,col,val] = find(BW_circle); clear val
        FRAP_vals = zeros(1,length(row));
        for i=1:length(row)
            FRAP_vals(1,i) = images(row(i), col(i), n*FrameUpdate);
        end
        FRAP_circle(1,n) = mean(FRAP_vals);
        %Plot the circle fit
        figure('position',[100 100 500 250]); %[x y width height]
        subplot(1,2,1);
        overlay1 = imoverlay(mat2gray(images(:,:,n*FrameUpdate)), bwperim(BW_circle), [.3 1 .3]);
        imagesc(overlay1);
        title(['FRAP spot in frame ', num2str(n*FrameUpdate)]);
        
        %Threshold to identify the nucleus
        Nucleus_binary = smooth_images(:,:,n*FrameUpdate) > IntThres*exp(-n*FrameUpdate/(ExpFactor*length(time)));
        %Now fill in any small holes in the middle of the nucleus:
        Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
        
        subplot(1,2,2);
        overlay2 = imoverlay(mat2gray(images(:,:,n*FrameUpdate)), bwperim(Nucleus_binary_filled), [1 0 0]);
        imagesc(overlay2);
        title(['Segmented nucleus in frame ', num2str(n)]);
        
        print('-dpng',['./images/', file, '_FRAME_', num2str(n), '.png' ]);
        close;
    end
    figure; 
    hold on;
    plot(1:length(FRAP_circle), FRAP_circle);
    
    hold off;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DETERMINE THE INITIAL FLUORESCENCE IN THE BLEACH AREA %%%
InitSpotIntensity = zeros(1,FirstBleachFrame-1);
for n = 1:FirstBleachFrame-1
    CumMove = Movement(1:max([round(n/FrameUpdate) 1]),:); %The cumulative movement of the circle
    CurrCentre = BleachCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    %Update the circle
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    InitSpot_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=CircleRadius+ExtraRadius;
    %Find pixels
    [row,col,val] = find(InitSpot_circle); clear val
    InitSpot_vals = zeros(1,length(row));
    for i=1:length(row)        
        InitSpot_vals(1,i) = images(row(i), col(i), n);
    end
    InitSpotIntensity(1,n) = mean(InitSpot_vals);
end

%%%%%% PLOT FRAP CURVES %%%%
FRAP_intensity = zeros(1, size(images, 3));
SmallCircle_intensity = zeros(1, size(images, 3));
Nuc_intensity = zeros(1, size(images, 3)); 
Black_intensity = zeros(1, size(images, 3));
for n = 1:size(images, 3)
    CumMove = Movement(1:max([round(n/FrameUpdate) 1]),:); %The cumulative movement of the circle
    CurrCentre = BleachCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    %currBackgroundCentre = BackgroundCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    currBackgroundCentre = BackgroundCentre;
    
    %Update the circle
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    BW_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=CircleRadius;
    Small_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=SmallCircleRadius;
    Black_circle = sqrt((rr-BackgroundCentre(1,1)).^2+(cc-BackgroundCentre(1,2)).^2)<=CircleRadius;
    %Find the pixels inside the circle:
    [row,col,val] = find(BW_circle); clear val
    FRAP_vals = zeros(1,length(row));
    for i=1:length(row)        
        FRAP_vals(1,i) = images(row(i), col(i), n);
    end
    FRAP_intensity(1,n) = mean(FRAP_vals);
    
    [row2,col2,val] = find(Small_circle); clear val
    Small_vals = zeros(1,length(row2));
    for i=1:length(row2)        
        Small_vals(1,i) = images(row2(i), col2(i), n);
    end
    SmallCircle_intensity(1,n) = mean(Small_vals);
    
    %SEGMENT THE NUCLEUS
    %Threshold to identify the nucleus
    Nucleus_binary = smooth_images(:,:,n) > IntThres*exp(-n/(ExpFactor*length(time)));
    %Now fill in any small holes in the middle of the nucleus:
    Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
    
    
    [nuc_row,nuc_col,val] = find(Nucleus_binary_filled); clear val
    [row4,col4,val] = find(Black_circle); clear val
    nuc_vals = zeros(1,length(nuc_row)); 
    Black_vals = zeros(1,length(row4));
    for i=1:length(nuc_row)        
        nuc_vals(1,i) = images(nuc_row(i), nuc_col(i), n);
    end
    for i=1:length(row4)        
        Black_vals(1,i) = images(row4(i), col4(i), n);
    end
    Nuc_intensity(1,n) = mean(nuc_vals);
    if U2OS_background > 0
        Black_intensity(1,n) = mean(Black_vals) - U2OS_background; 
    else
        Black_intensity(1,n) = mean(Black_vals);
    end
end
%PLOT the results
figure('position',[200 200 800 450]); %[x y width height]
subplot(1,3,1);
hold on;
plot(time, FRAP_intensity, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, SmallCircle_intensity, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);
plot(time, Black_intensity, 'ko', 'MarkerSize', 6);
plot(time, Nuc_intensity, 'o', 'Color', [100/255, 151/255, 75/255] , 'MarkerSize', 6);

plot(time(FirstBleachFrame:end), FRAP_intensity(FirstBleachFrame:end), '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 1);
plot(time(FirstBleachFrame:end), SmallCircle_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Black_intensity(FirstBleachFrame:end), 'k-', 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back1_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back2_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back3_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
legend('FRAP: r=10', 'FRAP: r=6', 'Black bg', 'whole Nuc', 'Location', 'NorthEast');
axis([min(time) max(time) 0 1.55*max(FRAP_intensity)]);
title('Raw FRAP quantification', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('Halo-mCTCF TMR fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%% PERFORM ANALYSIS AND PHOTOBLEACHING CORRECTION %%%%%%%%%%%%%%%%

%Rationale for the analysis: first of all, subtract the black background.
%Second, use the total nuclear fluorescence to correct for
%photobleaching and reduction in cell fluorescence overall. Furthermore, the cell 
%moves a bit so it is important to take into account that cell total and
%local fluorescence can change a bit. In some of the movies, cell moves
%quite a bit so this is important to do. 

%So do the following:
%1. subtract black background from all. 
%2. Divide the FRAP circle by the average nuclear background throughout. 

%1. SUBTRACT BLACK BACKGROUND
bsFRAP_intensity = FRAP_intensity - Black_intensity;
bsSmallCircle_intensity = SmallCircle_intensity - Black_intensity;
bsNuc_intensity = Nuc_intensity - Black_intensity;
InitSpotIntensity = InitSpotIntensity - Black_intensity(1:FirstBleachFrame-1);

%3. ADJUST FOR PHOTOBLEACHING
%adjust_FRAP = [bsFRAP_intensity(1:FirstBleachFrame-1)./ bsNuc_intensity(1:FirstBleachFrame-1) bsFRAP_intensity(FirstBleachFrame)/mean(bsNuc_intensity(1:FirstBleachFrame-1)) bsFRAP_intensity(FirstBleachFrame+1:end)./ bsNuc_intensity(FirstBleachFrame+1:end)];
%adjust_smallFRAP = [bsSmallCircle_intensity(1:FirstBleachFrame-1)./ bsNuc_intensity(1:FirstBleachFrame-1) bsSmallCircle_intensity(FirstBleachFrame)/mean(bsNuc_intensity(1:FirstBleachFrame-1)) bsSmallCircle_intensity(FirstBleachFrame+1:end)./ bsNuc_intensity(FirstBleachFrame+1:end)];
InitSpotIntensity = InitSpotIntensity./(bsNuc_intensity(1:FirstBleachFrame-1));

adjust_FRAP = bsFRAP_intensity ./smooth(bsNuc_intensity, 3)';
adjust_smallFRAP = bsSmallCircle_intensity ./smooth(bsNuc_intensity, 3)';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%% PLOT THE ADJUSTED FRAP CURVES %%%%%%%%%%%%%%%%%%%%%%%
subplot(1,3,2);
hold on;
plot(time, adjust_FRAP, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, adjust_smallFRAP, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);

plot(time(FirstBleachFrame:end), smooth(adjust_FRAP(FirstBleachFrame:end),7)', '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 2);
plot(time(FirstBleachFrame:end), smooth(adjust_smallFRAP(FirstBleachFrame:end),7)', '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 2);
legend('FRAP: r=10', 'FRAP: r=6',  'Location', 'NorthEast');
axis([min(time) max(time) 0 1.05*max(adjust_smallFRAP)]);
title('nuclear normalized FRAP', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('relative Halo-mCTCF TMR fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;


%%%%%%%%%%%%%%%%%%%%% NORMALIZE BASED ON INITIAL FLUORESCENCE %%%%%%%%%%%%%%%%%%%%%%%
Init_norm_FRAP = adjust_FRAP./mean(InitSpotIntensity);
Init_norm_smallFRAP = adjust_smallFRAP./mean(InitSpotIntensity);

subplot(1,3,3);
hold on;
plot(time, Init_norm_FRAP, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, Init_norm_smallFRAP, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);
plot([min(time) max(time)], [1 1], 'k--', 'LineWidth', 1);
plot(time(FirstBleachFrame:end), smooth(Init_norm_FRAP(FirstBleachFrame:end),7)', '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 2);
plot(time(FirstBleachFrame:end), smooth(Init_norm_smallFRAP(FirstBleachFrame:end),7)', '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 2);
legend('FRAP: r=10', 'FRAP: r=6',  'Location', 'NorthEast');
axis([min(time) max(time) 0 1.15]);
title('Initial spot normalized FRAP', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('relative Halo-mCTCF TMR fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;



%%%%%%%%%%%%%%%%%%%%%%%%%% SAVE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if SaveVal == 1;
    clearvars -except time FirstBleachFrame BleachCentre FRAP_intensity SmallCircle_intensity BackgroundCentre Movement SampleName adjust_FRAP adjust_smallFRAP bsNuc_intensity Init_norm_FRAP Init_norm_smallFRAP ExtraRadius ExpFactor
    
    save(['./FRAP_workspaces_v2/', SampleName, '.mat']);
    MakeMovie = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%% MAKE A MOVIE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if MakeMovie == 1
    %Adjust CumMove:
    FullCumMove = zeros(length(time), 2);
    for i=1:size(CumMove,1)
        CurrIndex = FrameUpdate + (i-1)*FrameUpdate;
        FullCumMove(CurrIndex,:) = CumMove(i,:);
    end
    prebleach = mean(adjust_smallFRAP(1:FirstBleachFrame-1));
    adjust_smallFRAP2 = [adjust_smallFRAP(1:FirstBleachFrame-1)./prebleach adjust_smallFRAP(FirstBleachFrame:end)];
           
    %Figure out how much the bleach circle moved
    for n=1:size(FullCumMove,1)
        %Update the FRAP circle centre
        CurrCentre = BleachCentre - [sum(FullCumMove(1:n,1)), sum(FullCumMove(1:n,2))];

        %Update the circle
        [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
        BW_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=CircleRadius;
        
        %Plot the circle fit
        figure('position',[100 100 650 250]); %[x y width height]
        subplot(1,2,1);
        overlay1 = imoverlay(mat2gray(images(:,:,n)), bwperim(BW_circle), [.3 1 .3]);
        imagesc(overlay1);
        title(['Halo-mCTCF FRAP image at ', num2str(n-FirstBleachFrame), ' seconds']);
        set(gca,'XTick',[]); set(gca,'YTick',[]);
        
        subplot(1,2,2);
        hold on;
        plot(time(1:n), Init_norm_smallFRAP(1:n), 'o', 'Color', [237/255, 28/255, 36/255] , 'MarkerSize', 6);
        axis([min(time) max(time) 0 1.15]);
        title('Quanfied FRAP recovery', 'FontSize',10, 'FontName', 'Helvetica');
        ylabel('relative TMR fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
        xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
        hold off;
        print('-dpng', '-r300',['./images/', file, '_FRAP_Movie_', num2str(n), '.png' ]);
        close;
    end
end