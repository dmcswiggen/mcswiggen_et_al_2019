%%%%%% Analyze Quantified FRAP data %%%%%%%%%%%%%%%%%
%%%%%%%Copyright (C) 2018 David McSwiggen

% modified from AnalyzeQuantifiedFRAPdata.m, Anders Sejr Hansen, Jan 2016

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script aggregates all of the .mat files generated in the "FRAP QUANT"
%%%%%%% script as inputs, and generates curves and model fits to the data. The inputs
%%%%%%% here match the raw data from McSwiggen et at 2019 FRAP data.

%%
clear; clc; close all; 

%Define what to analyze
AnalyzeWhat = 4; %1=interphase U2OS, 250msec, 1um bleach, JF549
                %2 = 3-4hpi, MOI 1, U2OS, 250msec, 1um bleach, JF549
                %3= infected, optimus, 4-5hpi, 250msec, 1um bleach, JF549
                %4= infected, optimus, 5-6hpi, 250msec, 1um bleach, JF549
DoModelFitting = 1;
FirstFitFrame = 16;
SaveVal = 1;


if AnalyzeWhat == 1 %uninfected, optimus
    %Workspaces:
    Workspaces = {'Uninfected_D1_001','Uninfected_D1_002','Uninfected_D1_003','Uninfected_D1_004','Uninfected_D1_005','Uninfected_D1_006','Uninfected_D1_007','Uninfected_D2_001','Uninfected_D2_002','Uninfected_D2_003','Uninfected_D2_004','Uninfected_D2_005','Uninfected_D2_006','Uninfected_D3_001','Uninfected_D3_002','Uninfected_D3_003','Uninfected_D3_004','Uninfected_D3_005','Uninfected_D3_006','Uninfected_D4_001','Uninfected_D4_002','Uninfected_D4_003','Uninfected_D4_004','Uninfected_D4_006','Uninfected_D5_001','Uninfected_D5_002','Uninfected_D5_004','Uninfected_D5_005','Uninfected_D5_006','Uninfected_D5_007'};
    ToKeep = [1,1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,5,5,5,5,5,5]; % N = 30
    ConditionName = 'Uninfected';
    Timestamp = 1;
    
elseif AnalyzeWhat == 2 %infected, optimus, 3-4hpi 
    %Workspaces:
    Workspaces = {'3-4hpi_D2_001','3-4hpi_D2_002','3-4hpi_D2_003','3-4hpi_D2_004','3-4hpi_D2_005','3-4hpi_D3_001','3-4hpi_D3_002','3-4hpi_D3_003','3-4hpi_D3_004','3-4hpi_D3_006','3-4hpi_D3_007','3-4hpi_D4_001','3-4hpi_D4_002','3-4hpi_D4_003','3-4hpi_D4_004','3-4hpi_D4_005','3-4hpi_D4_006','3-4hpi_D4_007','3-4hpi_D5_001','3-4hpi_D5_002','3-4hpi_D5_003','3-4hpi_D5_004','3-4hpi_D5_006','3-4hpi_D5_007'};
    ToKeep = [1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,3,4,4,4,4,4,4,4]; % N = 25
    ConditionName = '3hpi';
    Timestamp = 3;

elseif AnalyzeWhat == 3 %infected, optimus, 4-5hpi 
    %Workspaces:
    Workspaces = {'4-5hpi_D1_001','4-5hpi_D1_002','4-5hpi_D1_003','4-5hpi_D1_004','4-5hpi_D1_005','4-5hpi_D1_006','4-5hpi_D2_002','4-5hpi_D2_003','4-5hpi_D2_004','4-5hpi_D2_005','4-5hpi_D2_006','4-5hpi_D3_001','4-5hpi_D3_002','4-5hpi_D3_003','4-5hpi_D3_004','4-5hpi_D3_005','4-5hpi_D3_006','4-5hpi_D3_007','4-5hpi_D3_008','4-5hpi_D4_001','4-5hpi_D4_002','4-5hpi_D4_003','4-5hpi_D4_004','4-5hpi_D4_005','4-5hpi_D4_006','4-5hpi_D5_001','4-5hpi_D5_002','4-5hpi_D5_003','4-5hpi_D5_004','4-5hpi_D5_005','4-5hpi_D5_006'};
    ToKeep = [1,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,4,4,5,5,5,5,5,5]; % N = 31
    ConditionName = '4hpi';
    Timestamp = 4;

elseif AnalyzeWhat == 4 %infected, optimus, 5-6hpi 
    %Workspaces:
    Workspaces = {'5-6hpi_D1_001','5-6hpi_D1_002','5-6hpi_D1_004','5-6hpi_D1_005','5-6hpi_D2_001','5-6hpi_D2_002','5-6hpi_D2_003','5-6hpi_D2_005','5-6hpi_D3_001','5-6hpi_D3_002','5-6hpi_D3_003','5-6hpi_D3_004','5-6hpi_D3_005','5-6hpi_D3_006','5-6hpi_D3_007','5-6hpi_D3_008','5-6hpi_D4_001','5-6hpi_D4_002','5-6hpi_D4_003','5-6hpi_D4_004','5-6hpi_D4_005','5-6hpi_D4_006','5-6hpi_D4_007','5-6hpi_D4_008','5-6hpi_D5_001','5-6hpi_D5_002','5-6hpi_D5_003','5-6hpi_D5_004','5-6hpi_D5_005','5-6hpi_D5_006','5-6hpi_D5_007','5-6hpi_D5_008'};
    ToKeep = [1,1,1,1,2,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5]; % N = 32
    ConditionName = '5hpi';
    Timestamp = 5;
    
end

length(Workspaces)
length(ToKeep)
n_cells = length(ToKeep);
timestamp_array = (ones(length(ToKeep),1))*Timestamp;
    %Load the data
    iter = 1;
    figure('position',[100 100 1400 1000]); %[x y width height]
    for i=1:length(Workspaces)
        if ToKeep(i) >= 1
            load(['./FRAP_workspaces_v2/', Workspaces{i}]);
           if iter == 1
                %FRAP_data = Init_norm_smallFRAP;
                linear_spaced_time = xTIME_lin;
                log_spaced_time = log_time;
                FRAP_data_lin = Init_norm_FRAP;
                FRAP_data_log = log_FRAP_smooth;
                FRAP_data_BDC = log_FRAP_smooth_BDC;
                TwoExpFit_A = Exp_fit_A;
                TwoExpFit_B = Exp_fit_B;
                TwoExpFit_ka = Exp_fit_ka;
                TwoExpFit_kb = Exp_fit_kb;
                TwoExpFit_A_BDC = ExpBDC_fit_A;
                TwoExpFit_B_BDC = ExpBDC_fit_B;
                TwoExpFit_ka_BDC = ExpBDC_fit_ka;
                TwoExpFit_kb_BDC = ExpBDC_fit_kb;
                Bleach_depth_dist = Norm_bleach_depth;
                %Percent_area_RC = Total_RC_area;
                %Percent_PolII_recruitment = PolII_recruitment;
                if Time_to_50 < 4000
                    Time_to_50_percent = Time_to_50;
                else
                    Time_to_50_percent = NaN;
                end
                if Time_to_75 < 10000
                    Time_to_75_percent = Time_to_75;
                else
                    Time_to_75_percent = NaN;
                end
                if Time_to_85 < 10000
                    Time_to_85_percent = Time_to_85;
                else
                    Time_to_85_percent = NaN;
                end
                ExpBDC_fit_line = 1 - TwoExpFit_A_BDC(i) * exp(-TwoExpFit_ka_BDC(i)  * xTIME_lin) - TwoExpFit_B_BDC(i)  * exp(-TwoExpFit_kb_BDC(i)  * xTIME_lin);

            else
                %FRAP_data = vertcat(FRAP_data, Init_norm_smallFRAP);
                FRAP_data_lin = vertcat(FRAP_data_lin, Init_norm_FRAP);
                FRAP_data_log = vertcat(FRAP_data_log,log_FRAP_smooth);
                FRAP_data_BDC = vertcat(FRAP_data_BDC,log_FRAP_smooth_BDC);
                TwoExpFit_A = vertcat(TwoExpFit_A, Exp_fit_A);
                TwoExpFit_B = vertcat(TwoExpFit_B, Exp_fit_B);
                TwoExpFit_ka = vertcat(TwoExpFit_ka, Exp_fit_ka);
                TwoExpFit_kb = vertcat(TwoExpFit_kb, Exp_fit_kb);
                TwoExpFit_A_BDC = vertcat(TwoExpFit_A_BDC, ExpBDC_fit_A);
                TwoExpFit_B_BDC = vertcat(TwoExpFit_B_BDC, ExpBDC_fit_B);
                TwoExpFit_ka_BDC = vertcat(TwoExpFit_ka_BDC, ExpBDC_fit_ka);
                TwoExpFit_kb_BDC = vertcat(TwoExpFit_kb_BDC, ExpBDC_fit_kb);
                Bleach_depth_dist = vertcat(Bleach_depth_dist, Norm_bleach_depth);
                %Percent_area_RC = vertcat(Percent_area_RC, Total_RC_area);
                %Percent_PolII_recruitment = vertcat(Percent_PolII_recruitment, PolII_recruitment);
                if Time_to_50 < 4000
                    Time_to_50_percent = vertcat(Time_to_50_percent, Time_to_50);
                else
                    Time_to_50_percent = vertcat(Time_to_50_percent, NaN);
                end
                if Time_to_75 < 10000
                    Time_to_75_percent = vertcat(Time_to_75_percent, Time_to_75);
                else
                    Time_to_75_percent = vertcat(Time_to_75_percent, NaN);
                end
                if Time_to_85 < 10000
                    Time_to_85_percent = vertcat(Time_to_85_percent, Time_to_85);
                else
                    Time_to_85_percent = vertcat(Time_to_85_percent, NaN);
                end
                ExpBDC_fit_line = horzcat(ExpBDC_fit_line, 1 - TwoExpFit_A_BDC(i) * exp(-TwoExpFit_ka_BDC(i)  * xTIME_lin) - TwoExpFit_B_BDC(i)  * exp(-TwoExpFit_kb_BDC(i)  * xTIME_lin));

            end
            iter = iter + 1;
        
        subplot(5,7,i);
        hold on;
        %plot(time, Init_norm_smallFRAP, 'ko', 'MarkerSize', 3);
        plot(log_spaced_time, log_FRAP_smooth_BDC, 'ro', 'MarkerSize', 3);
        title([num2str(i), ': FRAP for ', Workspaces{i}], 'FontSize',8, 'FontName', 'Helvetica');
        ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
        xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
        legend('small circle', 'Location', 'SouthEast');
        axis([min(time) max(time) 0 1.2]);
        hold off;
        
        end
        
        
    end

iter = 1;
median_ExpBDC_fit_line = median(ExpBDC_fit_line,2);
mean_ExpBDC_fit_line = mean(ExpBDC_fit_line,2);
std_ExpBDC_fit_line = std(ExpBDC_fit_line,1,2);
%median_RC_area = median(Percent_area_RC,2);
%median_PolII_recruitment = median(Percent_PolII_recruitment, 2);
%recovery_array = horzcat(Time_to_50_percent(:),Time_to_75_percent(:),Time_to_85_percent(:));
%RC_recruitment_array = horzcat(Percent_area_RC,Percent_PolII_recruitment);
median_Time_to_50 = median(Time_to_50_percent,2,'omitnan');
median_Time_to_75 = median(Time_to_75_percent,2,'omitnan');
median_Time_to_85 = median(Time_to_85_percent,2,'omitnan');
std_Time_to_50 = std(Time_to_50_percent,1,2,'omitnan');
std_Time_to_75 = std(Time_to_75_percent,1,2,'omitnan');
std_Time_to_85 = std(Time_to_85_percent,1,2,'omitnan');

figure('position',[100 100 450 300]); %[x y width height]
hold on;
for i=1:length(Workspaces)
    plot(linear_spaced_time, ExpBDC_fit_line(:,i), 'k-', 'LineWidth', 1,'Color', [0.35, 0.35, 0.35]);
end
plot(linear_spaced_time, median_ExpBDC_fit_line, 'k-', 'LineWidth', 3,'Color', [0.208, 0.1663, 0.5292]);
title(['Individual fits for ', num2str(length(ToKeep)), ' cells,', ConditionName], 'FontSize',16, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',14, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',14, 'FontName', 'Helvetica');
axis([min(time) max(time) 0 1.2]);
hold off;


%PLOT the average data
meanFRAP_lin = mean(FRAP_data_lin, 1);
stdFRAP_lin = std(FRAP_data_lin);
meanFRAP_log = mean(FRAP_data_log, 1);
stdFRAP_log = std(FRAP_data_log);
meanFRAP_log_BDC = mean(FRAP_data_BDC, 1);
stdFRAP_log_BDC = std(FRAP_data_BDC);

figure('position',[100 100 450 300]); %[x y width height]
hold on;
errorbar(time, meanFRAP_lin, stdFRAP_lin, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 2);
plot(time(FirstBleachFrame:end), smooth(meanFRAP_lin(FirstBleachFrame:end),11), 'k-', 'LineWidth', 3);
title(['FRAP for ', num2str(length(ToKeep)), ' cells, ', ConditionName], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(time) max(time) 0 1.2]);
legend('mean with error', 'smoothed mean', 'Location', 'SouthEast');
legend boxoff;
hold off;

figure('position',[100 100 450 300]); %[x y width height]
hold on;
errorbar(log_time, meanFRAP_log_BDC, stdFRAP_log_BDC, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 2);
plot(time(FirstBleachFrame:end), median_ExpBDC_fit_line, 'k-', 'LineWidth', 3);
title(['FRAP for ', num2str(length(ToKeep)), ' cells, ', ConditionName], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(time) max(time) 0 1.2]);
legend('mean with error', 'smoothed mean', 'Location', 'SouthEast');
legend boxoff;
hold off;

if DoModelFitting == 1
    %Do a simply two-exponential fitting:
    %FRAP(t) = 1 - A*exp(-ka*t) - B*exp(-kb*t)
    
    %Variable for fitting:
    xTime = time(FirstFitFrame:end)+1;
    yFRAP1 = meanFRAP_lin(FirstFitFrame:end);
    yFRAP2_mean_lin = meanFRAP_lin(FirstBleachFrame:end);
    yFRAP2_mean_log = meanFRAP_log(FirstBleachFrame:end);
    yFRAP2_mean_log_BDC = meanFRAP_log_BDC(FirstBleachFrame:end);
    [xTIME_log, yFRAP2_mean_log] = prepareCurveData((log_time(FirstBleachFrame:end)),yFRAP2_mean_log);
    [xTIME_log, yFRAP2_mean_log_BDC] = prepareCurveData((log_time(FirstBleachFrame:end)),yFRAP2_mean_log_BDC);

    f1 = fittype('1 - A*exp(-ka*x) - B*exp(-kb*x)');
    [TwoExp_mean_fit, TwoExp_param] = fit(xTIME_log, yFRAP2_mean_log, f1, 'Lower', [0 0 0 0], 'Upper', [inf inf inf inf], 'StartPoint', [0.5 0.5 0.5 0.025]); 
    TwoExp_CI = confint(TwoExp_mean_fit);

    
    
    f2 = fittype('1 - A*exp(-ka*x) - B*exp(-kb*x)');
    [TwoExp_mean_BDC_fit, TwoExp_param] = fit(xTIME_log, yFRAP2_mean_log_BDC, f2, 'Lower', [0 0 0 0], 'Upper', [inf inf inf inf], 'StartPoint', [0.5 0.5 0.5 0]); 
    TwoExp_CI = confint(TwoExp_mean_BDC_fit);
    
    
    yFit = 1 - TwoExp_mean_fit.A .* exp(-TwoExp_mean_fit.ka .* xTime) - TwoExp_mean_fit.B .* exp(-TwoExp_mean_fit.kb .* xTime);
    yFit_BDC = 1 - TwoExp_mean_BDC_fit.A .* exp(-TwoExp_mean_BDC_fit.ka .* xTime) - TwoExp_mean_BDC_fit.B .* exp(-TwoExp_mean_BDC_fit.kb .* xTime);
    
    Mean_fit_A = TwoExp_mean_fit.A;
    Mean_fit_B = TwoExp_mean_fit.B;
    Mean_fit_ka = TwoExp_mean_fit.ka;
    Mean_fit_kb = TwoExp_mean_BDC_fit.kb;
    Mean_fit_A_BDC = TwoExp_mean_BDC_fit.A;
    Mean_fit_B_BDC = TwoExp_mean_BDC_fit.B;
    Mean_fit_ka_BDC = TwoExp_mean_BDC_fit.ka;
    Mean_fit_kb_BDC = TwoExp_mean_BDC_fit.kb;
   
    Fit2_text(1) = {'2-Exp fit: 1-A*exp(-ka*t)-B*exp(-kb*t)'};
    Fit2_text(2) = {['A = ', num2str(TwoExp_mean_BDC_fit.A)]};
    Fit2_text(3) = {['A (95% CI): [', num2str(TwoExp_CI(1,1)), ';', num2str(TwoExp_CI(2,1)), ']']};
    Fit2_text(4) = {['ka = ', num2str(TwoExp_mean_BDC_fit.ka), ' s^-1 or 1/k = ', num2str(1/TwoExp_mean_BDC_fit.ka), 's']};
    Fit2_text(5) = {['a (95% CI): [', num2str(TwoExp_CI(1,3)), ';', num2str(TwoExp_CI(2,3)), ']']};
    Fit2_text(6) = {['B = ', num2str(TwoExp_mean_BDC_fit.B)]};
    Fit2_text(7) = {['b (95% CI): [', num2str(TwoExp_CI(1,2)), ';', num2str(TwoExp_CI(2,2)), ']']};
    Fit2_text(8) = {['ka = ', num2str(TwoExp_mean_BDC_fit.kb), ' s^-1 or 1/k = ', num2str(1/TwoExp_mean_BDC_fit.kb), 's']};
    Fit2_text(9) = {['a (95% CI): [', num2str(TwoExp_CI(1,4)), ';', num2str(TwoExp_CI(2,4)), ']']};


%%%% TIME TO RECOVERY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = 0:0.1:10000000;
fractional_recovery = 1 - TwoExp_mean_BDC_fit.A .* exp(-TwoExp_mean_BDC_fit.ka .* t) - TwoExp_mean_BDC_fit.B .* exp(-TwoExp_mean_BDC_fit.kb .* t);
[val50,index_50] = min(abs(fractional_recovery - 0.5));
Time_to_50_fitted = index_50*0.1;
[val75,index_75] = min(abs(fractional_recovery - 0.75));
Time_to_75_fitted = index_75 *0.1;
[val85,index_85] = min(abs(fractional_recovery - 0.85));
Time_to_85_fitted = index_85 *0.1;



%%%% PLOT EVERYTHING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
figure('position',[100 100 1400 1000]); %[x y width height]
hold on;
lineProps.width = 2;
lineProps.col ={[0/255, 153/255, 204/255],[237/255, 28/255, 36/255], [0/255, 161/255, 75/255], [102/255, 45/255, 145/255], [157/255, 157/255, 54/255]};
mseb(log_time, meanFRAP_log_BDC, stdFRAP_log_BDC,lineProps,1)
plot(xTime, yFit_BDC, 'k--', 'LineWidth', 2);
title(['FRAP recovery of ', num2str(length(ToKeep)),' cells'], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('Normalized Recovery', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('Time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(xTime) max(xTime) 0 1.2]);
legend('5hpi','2exp fit to bleach','Location', 'NorthEast');
legend boxoff;
hold off;

figure('position',[100 100 1400 1000]); %[x y width height]
hold on;
lineProps.width = 2;
lineProps.col ={[0/255, 153/255, 204/255],[237/255, 28/255, 36/255], [0/255, 161/255, 75/255], [102/255, 45/255, 145/255], [157/255, 157/255, 54/255]};
mseb(log_time, meanFRAP_log, stdFRAP_log,lineProps,2)
plot(xTime, yFit, 'k--', 'LineWidth', 1);
title(['FRAP recovery of ', num2str(length(ToKeep)),' cells'], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('Normalized Recovery', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('Time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(xTime) max(xTime) 0 1.2]);
legend('5hpi','2exp fit to bleach','Location', 'NorthEast');
legend boxoff;
hold off;

figure('position',[100 100 1400 1000]);
hold on;
for i=1:length(Workspaces)
    plot(linear_spaced_time, ExpBDC_fit_line(:,i), 'k-', 'LineWidth', 1,'Color', [0.1801, 0.7177, 0.6424]);
end
lineProps.width = 4;
lineProps.col ={[0.0779, 0.504, 0.8384],[237/255, 28/255, 36/255], [0/255, 161/255, 75/255], [102/255, 45/255, 145/255], [157/255, 157/255, 54/255]};
mseb(log_time, meanFRAP_log_BDC, stdFRAP_log_BDC,lineProps,2)
plot([min(time) max(time)], [0.5 0.5], 'k--', 'LineWidth', 3);
title(['Individual fits for ', num2str(length(ToKeep)), ' cells,', ConditionName], 'FontSize',16, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',14, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',14, 'FontName', 'Helvetica');
axis([min(time) max(time) 0 1.2]);
hold off

end

%%%%%%%%%%%%%%%%%%%%%%%%%% SAVE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if SaveVal == 1;
    clearvars -except ConditionName time log_time meanFRAP_log meanFRAP_log_BDC stdFRAP_log stdFRAP_log_BDC TwoExpFit_A TwoExpFit_A_BDC TwoExpFit_B TwoExpFit_B_BDC TwoExpFit_ka TwoExpFit_ka_BDC TwoExpFit_kb TwoExpFit_kb_BDC n_cells xTime yFit yFit_BDC Percent_PolII_recruitment Percent_area_RC Time_to_50_percent Time_to_75_percent Time_to_85_percent timestamp_array median_Time_to_50 median_Time_to_75 median_Time_to_85 std_Time_to_50 std_Time_to_75 std_Time_to_85 recovery_array RC_recruitment_array Mean_fit_A_BDC Mean_fit_B_BDC Mean_fit_ka_BDC Mean_fit_kb_BDC
    save(['./FRAP_workspaces_v2/', ConditionName, '.mat']);
    MakeMovie = 0;
end

