%%%%%% Manual DNA FISH Segmentation %%%%%%%%%%%%%%%%%
%%%%%%%Copyright (C) 2018 David McSwiggen

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script uses reads in and opens Nikon .nd2 files of z-stack DNA
%%%%%%% or RNA FISH images, calculated the most likely focal plane,
%%%%%%% performs a sum-intensity projection for a defined distance in Z,
%%%%%%% then asks for user input to annotate cells and their individual RCs
%% OPEN STACKS IN MATLAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all

addpath(genpath('/Volumes/MCS64_2/Useful_functions/'));

input_path = '/Volumes/MCS64_2/DM_3_62/IF/';
output_path = input_path;
Experiment_type = 'FISH';

DAPI_channel = 'DAPI';
Orange_channel = 'Halo-RPB1';
Red_channel = 'UL';
NearIR_channel = 'ICP8_IF';

PixelSize = 0.130; %�m per pixel.
Z_step_size = 0.100; %�m per slice
range_to_project = 2; % Total distance to project through in �m
focus_threshold = 6;

if strcmp(Experiment_type,'IF')
    Channel_order = {'Orange','NearIR','DAPI'};
    Contains_RC = {'Orange','NearIR'};
elseif strcmp(Experiment_type, 'FISH')
    Channel_order = {'Red','NearIR','DAPI'};
    Contains_RC = {'NearIR'};
elseif strcmp(Experiment_type, 'DNA')
    Channel_order = {'Orange','Red','DAPI'};
    Contains_RC = {'Orange','Red'};
end


%% Find which files to process
%Find all the ND2 files in a folder
nd2_files=dir([input_path,'*.nd2']);
Filenames = ''; %for saving the actual file name
placeholder = 1;
for iter = 1:length(nd2_files)
    if nd2_files(iter).name(1) == '.'
        continue
    else
        Filenames{placeholder} = nd2_files(iter).name(1:end-4);
        placeholder = placeholder + 1;
    end
    
end
Namefiles = flipud(Filenames');
FilenamesFlip = Namefiles';
InputParameters = whos;


%% Load the data
for iter = 1:length(Filenames)
    if exist([input_path,Filenames{iter},'/',Filenames{iter},'_segmentation.mat'],'file') > 0
        disp([Filenames{iter},' has already been processed.'])
        disp('Skipping this file.')
        disp('-----------------------------------------------------------------')
        continue;
        
    else
        clear('except','InputParameters');
        disp('-----------------------------------------------------------------');
        tic;
        disp(['Processing ', Filenames{iter},'...']);
        
        
        %% read nd2 files:
        ND2_ImgStack= bfopen([input_path, Filenames{iter}, '.nd2']);
        number_of_conditions = length(Channel_order);
        slices_to_project = round(range_to_project/Z_step_size);
        number_of_z_planes = size(ND2_ImgStack{1,1},1)/number_of_conditions;
        ImWidth = size(ND2_ImgStack{1,1}{1,1},2);
        ImHeight = size(ND2_ImgStack{1,1}{1,1},1);
        [all_files, mess,messid] = mkdir(output_path, Filenames{iter});
        
        focus_measure = NaN(number_of_z_planes,number_of_conditions);
        
        if sum(strcmp(Channel_order,'DAPI'))
            string_find = strfind(Channel_order,'DAPI');channel_index = find(not(cellfun('isempty', string_find)));
            string_find = strfind(Contains_RC,'DAPI');
            if sum(not(cellfun('isempty', string_find))) == 1
                Contains_RC_binary(channel_index,1) = 1;
            else
                Contains_RC_binary(channel_index,1) = 0;
            end
            DAPI_channel_z_stack = zeros(ImWidth,ImHeight,number_of_z_planes);
            for slice = 1:number_of_z_planes
                DAPI_channel_z_stack(:,:,slice) = ND2_ImgStack{1,1}{channel_index+(number_of_conditions*(slice-1)),1};
                focus_measure(slice, channel_index) = fmeasure(DAPI_channel_z_stack(:,:,slice), 'GDER');
            end
        end
        
        if sum(strcmp(Channel_order,'Orange'))
            string_find = strfind(Channel_order,'Orange');channel_index = find(not(cellfun('isempty', string_find)));
            Orange_channel_z_stack = zeros(ImWidth,ImHeight,number_of_z_planes);
            string_find = strfind(Contains_RC,'Orange');
            if sum(not(cellfun('isempty', string_find))) == 1
                Contains_RC_binary(channel_index,1) = 1;
            else
                Contains_RC_binary(channel_index,1) = 0;
            end
            for slice = 1:number_of_z_planes
                Orange_channel_z_stack(:,:,slice) = ND2_ImgStack{1,1}{channel_index+(number_of_conditions*(slice-1)),1};
                focus_measure(slice, channel_index) = fmeasure(Orange_channel_z_stack(:,:,slice), 'GDER');
            end
        end
        
        if sum(strcmp(Channel_order,'Red'))
            string_find = strfind(Channel_order,'Red'); channel_index = find(not(cellfun('isempty', string_find)));
            string_find = strfind(Contains_RC,'Red');
            if sum(not(cellfun('isempty', string_find))) == 1
                Contains_RC_binary(channel_index,1) = 1;
            else
                Contains_RC_binary(channel_index,1) = 0;
            end
            Red_channel_z_stack = zeros(ImWidth,ImHeight,number_of_z_planes);
            for slice = 1:number_of_z_planes
                Red_channel_z_stack(:,:,slice) = ND2_ImgStack{1,1}{channel_index+(number_of_conditions*(slice-1)),1};
                focus_measure(slice, channel_index) = fmeasure(Red_channel_z_stack(:,:,slice), 'GDER');
            end
        end
        
        if sum(strcmp(Channel_order,'NearIR'))
            string_find = strfind(Channel_order,'NearIR'); channel_index = find(not(cellfun('isempty', string_find)));
            string_find = strfind(Contains_RC,'NearIR');
            if sum(not(cellfun('isempty', string_find))) == 1
                Contains_RC_binary(channel_index,1) = 1;
            else
                Contains_RC_binary(channel_index,1) = 0;
            end
            NearIR_channel_z_stack = zeros(ImWidth,ImHeight,number_of_z_planes);
            for slice = 1:number_of_z_planes
                NearIR_channel_z_stack(:,:,slice) = ND2_ImgStack{1,1}{channel_index+(number_of_conditions*(slice-1)),1};
                focus_measure(slice, channel_index) = fmeasure(NearIR_channel_z_stack(:,:,slice), 'GDER');
            end
        end
        
        
        
        [val,idx] = max(focus_measure);
        if std(idx) < focus_threshold
            
            median_focal_value = round(median(idx));
            focal_plane = median_focal_value;
            if median_focal_value + floor(slices_to_project/2) > size(focus_measure,1)
                top = size(focus_measure,1);
            else
                top = median_focal_value + floor(slices_to_project/2);
            end
            if median_focal_value - floor(slices_to_project/2) < 1
                bottom = 1;
            else
                bottom = median_focal_value - floor(slices_to_project/2);
            end
            
            if sum(strcmp(Channel_order,'DAPI'))
                string_find = strfind(Channel_order,'DAPI');channel_index = find(not(cellfun('isempty', string_find)));
                DAPI_channel_max = max(DAPI_channel_z_stack(:,:,bottom:top),[],3);
                DAPI_channel_max_tiff = uint16(DAPI_channel_max);
                All_max_projections(:,:,channel_index) = DAPI_channel_max;
                imwrite(uint16(DAPI_channel_max_tiff),[output_path, Filenames{iter},'/',Filenames{iter},'_',DAPI_channel,'_max.tiff']);
            end
            
            if sum(strcmp(Channel_order,'Orange'))
                string_find = strfind(Channel_order,'Orange');channel_index = find(not(cellfun('isempty', string_find)));
                Orange_channel_max = max(Orange_channel_z_stack(:,:,bottom:top),[],3);
                Orange_channel_max_tiff = uint16(Orange_channel_max);
                All_max_projections(:,:,channel_index) = Orange_channel_max;
                imwrite(uint16(Orange_channel_max_tiff),[output_path, Filenames{iter},'/',Filenames{iter},'_',Orange_channel,'_max.tiff']);
            end
            
            if sum(strcmp(Channel_order,'Red'))
                string_find = strfind(Channel_order,'Red');channel_index = find(not(cellfun('isempty', string_find)));
                Red_channel_max = max(Red_channel_z_stack(:,:,bottom:top),[],3);
                Red_channel_max_tiff = uint16(Red_channel_max);
                All_max_projections(:,:,channel_index) = Red_channel_max;
                imwrite(uint16(Red_channel_max_tiff),[output_path, Filenames{iter},'/',Filenames{iter},'_',Red_channel,'_max.tiff']);
            end
            
            if sum(strcmp(Channel_order,'NearIR'))
                string_find = strfind(Channel_order,'NearIR');channel_index = find(not(cellfun('isempty', string_find)));
                NearIR_channel_max = max(NearIR_channel_z_stack(:,:,bottom:top),[],3);
                NearIR_channel_max_tiff = uint16(NearIR_channel_max);
                All_max_projections(:,:,channel_index) = NearIR_channel_max;
                imwrite(uint16(NearIR_channel_max_tiff),[output_path, Filenames{iter},'/',Filenames{iter},'_',NearIR_channel,'_max.tiff']);
            end
            
            temp_img = [];
            for plane = 1:size(All_max_projections,3)
                temp_img = cat(3,temp_img, All_max_projections(:,:,plane) .* double(Contains_RC_binary(plane)));
            end
            
            %% Determine background and subtract
            imshow(sum(temp_img,3),[]);
            uiwait(msgbox('Select background region for subtraction'));
            
            [BkgdROI, X, Y] = roipoly();
            BkgdROI_info(1,1) = {X}; BkgdROI_info(2,1) = {Y};
            
            if sum(strcmp(Channel_order,'DAPI'))
                string_find = strfind(Channel_order,'DAPI');channel_index = find(not(cellfun('isempty', string_find)));
                Temp_img_stack = NaN(ImHeight,ImWidth,(top-bottom+1));
                for slice = bottom:top
                    imgIdx = slice - bottom + 1;
                    BkgdRegion = regionprops(BkgdROI,DAPI_channel_z_stack(:,:,slice),'all');
                    min_bkgd_val = BkgdRegion.MaxIntensity;
                    Temp_img_stack(:,:,imgIdx) = DAPI_channel_z_stack(:,:,slice) - min_bkgd_val;
                end
                DAPI_channel_sum = sum(Temp_img_stack,3);
                L = DAPI_channel_sum > 0;
                DAPI_channel_sum = DAPI_channel_sum .* L;
                All_sum_projections(:,:,channel_index) = DAPI_channel_sum;
                %imwrite(uint16(DAPI_channel_max_tiff),[output_path, Filenames{iter},'/',Filenames{iter},'_',DAPI_channel,'_sum.tiff']);
            end
            
            if sum(strcmp(Channel_order,'Orange'))
                string_find = strfind(Channel_order,'Orange');channel_index = find(not(cellfun('isempty', string_find)));
                Temp_img_stack = NaN(ImHeight,ImWidth,(top-bottom+1));
                for slice = bottom:top
                    imgIdx = slice - bottom + 1;
                    BkgdRegion = regionprops(BkgdROI,Orange_channel_z_stack(:,:,slice),'all');
                    min_bkgd_val = BkgdRegion.MaxIntensity;
                    Temp_img_stack(:,:,imgIdx) = Orange_channel_z_stack(:,:,slice) - min_bkgd_val;
                end
                Orange_channel_sum = sum(Temp_img_stack,3);
                L = Orange_channel_sum > 0;
                Orange_channel_sum = Orange_channel_sum .* L;
                All_sum_projections(:,:,channel_index) = Orange_channel_sum;
                %imwrite(uint16(Orange_channel_max_tiff),[output_path, Filenames{iter},'/',Filenames{iter},'_',Orange_channel,'_sum.tiff']);
            end
            
            if sum(strcmp(Channel_order,'Red'))
                string_find = strfind(Channel_order,'Red');channel_index = find(not(cellfun('isempty', string_find)));
                Temp_img_stack = NaN(ImHeight,ImWidth,(top-bottom+1));
                for slice = bottom:top
                    imgIdx = slice - bottom + 1;
                    BkgdRegion = regionprops(BkgdROI,Red_channel_z_stack(:,:,slice),'all');
                    min_bkgd_val = BkgdRegion.MaxIntensity;
                    Temp_img_stack(:,:,imgIdx) = Red_channel_z_stack(:,:,slice) - min_bkgd_val;
                end
                Red_channel_sum = sum(Temp_img_stack,3);
                L = Red_channel_sum > 0;
                Red_channel_sum = Red_channel_sum .* L;
                All_sum_projections(:,:,channel_index) = Red_channel_sum;
                %imwrite(uint16(Red_channel_max_tiff),[output_path, Filenames{iter},'/',Filenames{iter},'_',Red_channel,'_sum.tiff']);
            end
            
            if sum(strcmp(Channel_order,'NearIR'))
                string_find = strfind(Channel_order,'NearIR');channel_index = find(not(cellfun('isempty', string_find)));
                Temp_img_stack = NaN(ImHeight,ImWidth,(top-bottom+1));
                for slice = bottom:top
                    imgIdx = slice - bottom + 1;
                    BkgdRegion = regionprops(BkgdROI,NearIR_channel_z_stack(:,:,slice),'all');
                    min_bkgd_val = BkgdRegion.MaxIntensity;
                    Temp_img_stack(:,:,imgIdx) = NearIR_channel_z_stack(:,:,slice) - min_bkgd_val;
                end
                NearIR_channel_sum = sum(Temp_img_stack,3);
                L = NearIR_channel_sum > 0;
                NearIR_channel_sum = NearIR_channel_sum .* L;
                All_sum_projections(:,:,channel_index) = NearIR_channel_sum;
                %imwrite(uint16(NearIR_channel_max_tiff),[output_path, Filenames{iter},'/',Filenames{iter},'_',NearIR_channel,'_sum.tiff']);
            end
            
            
            
            
            
            figure('position',[20 100 1200 800]);
            for channel = 1:size(All_max_projections,3)
                subtightplot(2,2,channel,[0.05,0.05])
                imshow(All_sum_projections(:,:,channel),[]);
                title([Channel_order{channel},' max proj.']);
            end
            
            temp_img = [];
            for plane = 1:size(All_max_projections,3)
                temp_img = cat(3,temp_img, All_sum_projections(:,:,plane) .* double(Contains_RC_binary(plane)));
            end
            
            ImgRC = sum(temp_img,3);
            
            
            %% User inputs data
            Cell_individual_masks = zeros(ImHeight,ImWidth,2);
            Cell_masks_all = zeros(ImHeight,ImWidth,2);%Create a binary mask for each frame to determine when a particle detected in frame n is within the nucleus
            
            
            TempImgMasks = zeros(ImHeight,ImWidth,2);
            RC_inside_masks_all = zeros(ImHeight,ImWidth,1);%Create a binary mask for each frame to determine when a particle detected in frame n is within and of the replication compartments
            
            roi_info_nuc = cell(1,2,2); %create an empty cell to deposit X (first column) and Y (second column) information from the generated ROIs, for the begining and end of the aquisition
            roi_RC_temp = cell(1,2,2); %create an empty cell to deposit X (first column) and Y (second column) information from the generated ROIs
            
            uiwait(msgbox('Click OK to proceed to make rough outlines of cells'));
            close all;
            
            %% Segmentation of Nuclei %%%%%%%%
            cell_num = 1;
            while cell_num > 0
                imshow(DAPI_channel_sum,[]);
                title('Outlines superimposed');
                axis image;
                hold on;
                if cell_num > 1
                    for k = 1 : numberOf_cell_Boundaries
                        thisBoundary = Cell_outlines{k};
                        plot(thisBoundary(:,2), thisBoundary(:,1), 'g', 'LineWidth', 2);
                    end
                end
                hold off;
                [Cell_individual_masks(:,:,cell_num), x_coord_cell, y_coord_cell] = roipoly();
                roi_info_cell(cell_num,1,1) = {x_coord_cell};
                roi_info_cell(cell_num,2,1) = {y_coord_cell};
                
                
                button_case = questdlg('Are there more nuclei to segment?');
                switch button_case
                    case 'Yes'
                        cell_num = cell_num + 1;
                        Cell_individual_masks = cat(3,Cell_individual_masks,zeros(ImHeight,ImWidth,1));
                        Cell_masks_all(:,:,1) = sum(Cell_individual_masks,3)>0;
                        Cell_masks_all(:,:,1) = imfill(Cell_masks_all(:,:,1),'holes');
                        Cell_outlines = bwboundaries(Cell_masks_all(:,:,1),8);
                        numberOf_cell_Boundaries = size(Cell_outlines, 1);
                    case 'No'
                        cell_num = 0;
                        Cell_masks_all = sum(Cell_individual_masks,3)>0;
                        Cell_masks_all = imfill(Cell_masks_all,'holes');
                        Cell_outlines = bwboundaries(Cell_masks_all,8);
                        numberOf_cell_Boundaries = size(Cell_outlines, 1);
                    case 'Cancel'
                end
                close;
            end
            
            [DAPI_binary,DAPI_props] = fluorescence_segmentation_static(DAPI_channel_sum,Cell_masks_all,5000,3,0);
            
            Rough_cell_outline = bwboundaries(Cell_masks_all,8);
            DAPI_outline = bwboundaries(DAPI_binary,8);
            numberOf_cell_Boundaries = size(Rough_cell_outline, 1);
            numberOf_nuclear_Boundaries = size(DAPI_outline, 1);
            
            figure('position',[20 100 800 800]);
            imshow(DAPI_channel_max,[]);
            axis image;
            hold on;
            for k = 1 : numberOf_cell_Boundaries
                thisBoundary = Rough_cell_outline{k};
                plot(thisBoundary(:,2), thisBoundary(:,1), 'g', 'LineWidth', 2);
            end
            for L = 1 : numberOf_nuclear_Boundaries
                thisBoundary = DAPI_outline{L};
                plot(thisBoundary(:,2), thisBoundary(:,1), 'r', 'LineWidth', 1);
            end
            hold off;
            
            uiwait(msgbox('Click OK to proceed to outline RCs'));
            close all;
            
            %% Segmentation of Replication Compartments %%%%%%%%%
            roi_RC_all = [];
            
            for nuc = 1:length(DAPI_props)
                
                ImgToMask = imcrop(ImgRC,DAPI_props(nuc).BoundingBox);
                TempImgMasks = zeros(size(ImgToMask,1),size(ImgToMask,2));
                RC_num = 1;
                while RC_num > 0
                    imshow(ImgToMask, [],'InitialMagnification',300);
                    title('Outlines of Replication Compartments for this cell');
                    axis image;
                    if RC_num > 1
                        hold on;
                        for i = 1 : numberOf_RC_Boundaries
                            thisBoundary = RC_outlines{i};
                            plot(thisBoundary(:,2), thisBoundary(:,1), 'r', 'LineWidth', 1);
                        end
                        hold off;
                    end
                    
                    
                    [TempImgMasks(:,:,RC_num), x_coord_RC, y_coord_RC] = roipoly();
                    roi_RC_temp(RC_num,1,1) = {x_coord_RC + DAPI_props(nuc).BoundingBox(1)}; %store x coordinates in the first column of roi_info_RC as a list
                    roi_RC_temp(RC_num,2,1) = {y_coord_RC + DAPI_props(nuc).BoundingBox(2)}; %store y coordinates in the first column of roi_info_RC as a list
                    
                    button_case = questdlg('Are there more Replication Compartments to segment?');
                    switch button_case
                        case 'Yes'
                            TempImgMasks = cat(3,TempImgMasks,zeros(size(ImgToMask,1),size(ImgToMask,2)));
                            RC_num = RC_num + 1;
                            
                            RC_inside_masks_temp = (sum(TempImgMasks,3))>0; %Combines all masks into one mask, should even work if ROIs overlap
                            RC_inside_masks_temp = imfill(RC_inside_masks_temp,'holes'); %There should be no holes, but just in case
                            RC_outlines = bwboundaries(RC_inside_masks_temp,8);
                            numberOf_RC_Boundaries = size(RC_outlines, 1);
                        case 'No'
                            RC_num = 0;
                            RC_inside_masks_temp = (sum(TempImgMasks,3))>0; %Combines all masks into one mask, should even work if ROIs overlap
                            RC_inside_masks_temp = imfill(RC_inside_masks_temp,'holes'); %There should be no holes, but just in case
                            RC_outlines = bwboundaries(RC_inside_masks_temp,8);
                            numberOf_RC_Boundaries = size(RC_outlines, 1);
                        case 'Cancel'
                    end
                    close;
                end
                
                roi_RC_all = vertcat(roi_RC_all,roi_RC_temp);
                temp_mask = zeros(ImHeight,ImWidth,size(roi_RC_all,1));
                for RC = 1:size(roi_RC_all,1)
                    temp_mask(:,:,RC) = poly2mask(roi_RC_all{RC,1},roi_RC_all{RC,2},ImWidth,ImHeight);
                end
                
                RC_all_masks = (sum(temp_mask,3))>0;
                RC_all_masks = imfill(RC_all_masks, 'holes');
                RC_all_outlines = bwboundaries(RC_all_masks,8);
                numberOf_RC_Boundaries = size(RC_all_outlines, 1);
                
                figure('position',[20 100 800 800]);
                imshow(ImgRC, []);
                axis image;
                hold on;
                for i = 1 : numberOf_RC_Boundaries
                    thisBoundary = RC_all_outlines{i};
                    plot(thisBoundary(:,2), thisBoundary(:,1), 'g--', 'LineWidth', 1);
                end
                for j = 1 : numberOf_nuclear_Boundaries
                    thisBoundary = DAPI_outline{j};
                    plot(thisBoundary(:,2), thisBoundary(:,1), 'r', 'LineWidth', 2);
                end
                hold off;
                
                
                
                uiwait(msgbox('Click OK to proceed to the next cell'));
            end
            close all;
            
            %% Save data %%%%%%%%%
             intensities_measure_img = [];
            for plane = 1:size(All_max_projections,3)
                intensities_measure_img(:,:,plane) = All_sum_projections(:,:,plane) .* DAPI_binary;
                Channel_intensity(plane,1:2) = [min(min(intensities_measure_img(:,:,plane))), max(max(intensities_measure_img(:,:,plane))) ]; % [Min, Max] for each color channed for displaying later
            end
            
            Total_nucleus_mask = Cell_masks_all;
            RC_mask = RC_inside_masks_temp;
            save([output_path, Filenames{iter},'/',Filenames{iter},'_segmentation.mat'],...
                'DAPI_binary','RC_all_masks','All_sum_projections', 'Channel_intensity');
            
        end
    end
end