%%%%%% Manual FISH Analysis %%%%%%%%%%%%%%%%%
%%%%%%%Copyright (C) 2018 David McSwiggen

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script uses previously annotated files from Manual FISH
%%%%%%% Segmentation (specifically the '_SumProj_v1.mat' files) to perform
%%%%%%% analysis on all cells in a given experimental condition.

%% OPEN STACKS IN MATLAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all

addpath(genpath('/Volumes/MCS64_2/Useful_functions/'));

input_path = '/Volumes/MCS64_2/DM_3_62/IF/';
output_path = input_path;
Experiment_type = 'IF';

DAPI_channel = 'DAPI';
Orange_channel = 'Halo-RPB1';
Red_channel = 'UL';
NearIR_channel = 'ICP8_IF';

PixelSize = 0.130; %�m per pixel.
Z_step_size = 0.100; %�m per slice
range_to_project = 1; % Total distance to project through in �m
focus_threshold = 6;

if strcmp(Experiment_type,'IF')
    Channel_order = {'Orange','NearIR','DAPI'};
    Contains_RC = {'Orange','NearIR'};
elseif strcmp(Experiment_type, 'FISH')
    Channel_order = {'Red','NearIR','DAPI'};
    Contains_RC = {'NearIR'};
elseif strcmp(Experiment_type, 'DNA')
    Channel_order = {'Orange','Red','DAPI'};
    Contains_RC = {'Orange','Red'};
end


%% Find which files to process
%Find all the ND2 files in a folder
nd2_files=dir([input_path,'*.nd2']);
Filenames = ''; %for saving the actual file name
placeholder = 1;
for iter = 1:length(nd2_files)
    if nd2_files(iter).name(1) == '.'
        continue
    else
        Filenames{placeholder} = nd2_files(iter).name(1:end-4);
        placeholder = placeholder + 1;
    end
    
end
Namefiles = flipud(Filenames');
Filenames = Namefiles';
InputParameters = whos;


%% Load the data
AllRC_rgprps = [];
AllNuc_rgrps = [];
for iter = 1:length(Filenames)
    if ~exist([input_path,Filenames{iter},'/',Filenames{iter},'_SumProj_v1.mat'],'file') > 0
        disp([Filenames{iter},' has not been processed.'])
        disp('Skipping this file.')
        disp('-----------------------------------------------------------------')
        continue;
        
    else
        clear('except','InputParameters');
        disp('-----------------------------------------------------------------');
        tic;
        disp(['Processing ', Filenames{iter},'...']);
        
        
        %% read files:
        load([input_path,Filenames{iter},'/',Filenames{iter},'_SumProj_v1.mat']);
        
        ImWidth = size(All_sum_projections,2);
        ImHeight = size(All_sum_projections,1);
        Orange_channel_sum = All_sum_projections(:,:,1);
        
        
        
        
        
        %% Measure fluorescence intensities %%%%%%%%%%%%%%%%%%%%%%%%%%%
        temp_RC_rgprps = regionprops(RC_all_masks, Orange_channel_sum,'all');
        
        for RC = 1:length(temp_RC_rgprps)
            TempBin(temp_RC_rgprps(RC).PixelIdxList) = 1;
        end
        
        temp_nuc_rgprps = regionprops(DAPI_binary, Orange_channel_sum,'all');
        for nuc = 1:length(temp_nuc_rgprps)
            TempBin = zeros(ImHeight,ImWidth);
            TempBin(temp_nuc_rgprps(nuc).PixelIdxList) = 1;
            TempBinInside = logical(TempBin .* RC_all_masks);
            TempBinOutside = logical(TempBin .* ~RC_all_masks);
            
            outside_rgprps = regionprops(TempBinOutside, Orange_channel_sum,'all');
            temp_nuc_rgprps(nuc).MedianIntensity = median(temp_nuc_rgprps(nuc).PixelValues);
            temp_nuc_rgprps(nuc).TotalIntensity = sum(temp_nuc_rgprps(nuc).PixelValues);
            temp_nuc_rgprps(nuc).OutsideIntensity = sum(outside_rgprps(1).PixelValues);
            temp_nuc_rgprps(nuc).InsideIntensity = temp_nuc_rgprps(nuc).TotalIntensity - temp_nuc_rgprps(nuc).OutsideIntensity;
            temp_nuc_rgprps(nuc).RC_enrichment = temp_nuc_rgprps(nuc).InsideIntensity / temp_nuc_rgprps(nuc).TotalIntensity;
            
            temp_nuc_rgprps(nuc).MedianIntensityOutside = median(outside_rgprps(1).PixelValues);
            temp_nuc_rgprps(nuc).Condition = Filenames{iter}(1:end-4);
            temp_nuc_rgprps(nuc).FOV = Filenames{iter}(end-2:end);
            
            temp_RC_rgprps =  regionprops(TempBinInside, Orange_channel_sum,'all');
            for RC = 1:length(temp_RC_rgprps)
                temp_RC_rgprps(RC).MedianIntensity = median(temp_RC_rgprps(RC).PixelValues);
                temp_RC_rgprps(RC).EnrichmentMedain = temp_RC_rgprps(RC).MedianIntensity / temp_nuc_rgprps(nuc).MedianIntensityOutside;
                temp_RC_rgprps(RC).EnrichmentIntensity = sum(temp_RC_rgprps(RC).PixelValues) /(temp_RC_rgprps(RC).Area * temp_nuc_rgprps(nuc).MedianIntensityOutside);
                temp_RC_rgprps(RC).Condition = Filenames{iter}(1:end-4);
                temp_RC_rgprps(RC).FOV = Filenames{iter}(end-2:end);
            end
            AllRC_rgprps = vertcat(AllRC_rgprps, temp_RC_rgprps);
        end
        
        AllNuc_rgrps = vertcat(AllNuc_rgrps, temp_nuc_rgprps);
        
        
        %% Save data %%%%%%%%%
        save([output_path, Filenames{iter},'/',Filenames{iter},'_SumProj_v2.mat'],...
            'DAPI_binary','RC_all_masks','All_sum_projections','temp_RC_rgprps','temp_nuc_rgprps');
        close all
        
    end
end

save([output_path,'/AllAnnotations_RC_quant_2.mat'],...
    'AllNuc_rgrps','AllRC_rgprps');
