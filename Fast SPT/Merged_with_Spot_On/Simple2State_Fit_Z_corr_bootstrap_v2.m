%Simple2State_Fit.m
%Anders Sejr Hansen, April 2017
%Modified by D McSwiggen, April 2017

% This is for data collected with 7 ms camera exposure time.

clear; clc; clearvars -global; close all;

%In this script we will fit fastSPT data to the simple BOUND-UNBOUND
%2-state model and determine the best-fit parameters
global LocError dT HistVecJumps dZ HistVecJumpsCDF PDF_or_CDF

%Dataset: determine which dataset to load
SampleName = 'Bootstrap_4-6hpi_pooled';
input_path = '/Volumes/LaCie_McS/Imaging/DM_2_70_fastSPT/4-6hpi/';
output_path = '/Users/Davidmcswiggen/Google Drive/Lab_Stuff/Lab_notebook/DM_2_70_fastSPT/2state_bootstrap/';

Number_bootstrap_iterations = 2;
number_to_subsample = 10;
SavePDF = 0; %Set to =1 if you want a PDF saved to a local folder
ModelFit = 2; %Use 1 for PDF-fitting; Use 2 for CDF-fitting
DoSingleCellFit = 0; %Set to 1 if you want to analyse all single cells individually.
HSV_infected = 1;

iterations = 1; %Manually input the desired number of fitting iterations:
LocError = 0.045; %Manually input the localization error in um: 35 nm = 0.035 um.
jump_length_minimum_dist = 0.1;

%UPDATE July 28, 2017 - v2
% Incorporated random resampling into analysis. Code will randomly
% subsample data out of entire data set and perform model fitting

%%%% A NOTE ON MODEL FITTING %%%%
%Use ModelFit = 1 if you want to do fitting of the jump length histogram
%using PDF fitting.
%Use ModelFit = 2 if you want to do fitting of the jump length histogram
%using CDF fitting.
%When fitting to a CDF you can use much smaller bin sizes and thus occur
%smaller binning artefacts.
PDF_or_CDF = ModelFit;

%2-State model: give ranges for parameters:
%D_Free = [0.15 25];
D_Free = [0.1 5];
D_Bound = [0.0001 0.05];
Frac_Bound = [0 1];
list_of_model_parameters = {'D_Free', 'D_Bound', 'Frac_Bound'};

%Data and fitting parameters
%MaxJump = 1.25; BinWidth = 0.010; %For PDF fitting and plotting
%MaxJump = 1.65; BinWidth = 0.010; %For PDF fitting and plotting
MaxJump = 2.05; BinWidth = 0.010; %For PDF fitting and plotting
TimePoints = 7; %TimePoints: how many jump lengths to use for the fitting: 3 timepoints, yields 2 jumps
UseAllTraj = 0; %Use =1 if you want to use all of a trajectory. This biases towards bound molecules, but useful for troubleshooting
JumpsToConsider = 4; % If UseAllTraj =0, then use no more than 3 jumps.
HistVecJumps = 0:BinWidth:MaxJump; %jump lengths in micrometers
HistVecJumpsCDF = 0:0.001:MaxJump; %jump lengths in micrometers
GapsAllowed = 1; %This is the number of missing frames that are allowed in a single trajectory
TimeGap = 7.477; %Time between frames in milliseconds;
%TimeGap = 1000/102; %Time between frames in milliseconds;
dT = TimeGap/1000; %Time in seconds
dZ = 0.700; %The axial illumination slice: measured to be roughly 700 nm


%% Read in all workspaces
nd2_files=dir([input_path,'*.nd2']);
all_workspaces = ''; %for saving the actual file name

keep_file = 1;
for file = 1:length(nd2_files)
    if nd2_files(file).name(1) == '.'
        continue
    else
        all_workspaces{keep_file} = [input_path,nd2_files(file).name(1:end-4),'/',nd2_files(file).name(1:end-4),'_Tracked_and_masked.mat'];
        keep_file = keep_file + 1;
    end
end
if HSV_infected == 1
    Compartment_to_test_start = 2;
    Compartment_to_test_end = 3;
else
    Compartment_to_test_start = 1;
    Compartment_to_test_end = 1;
end

%%
Compartment_jump_stats_bootstrap = NaN(4,4,Number_bootstrap_iterations);

for bootstrap_iteration = 1:Number_bootstrap_iterations
    if Number_bootstrap_iterations == 1
        workspaces = all_workspaces;
    else
        workspaces = datasample(all_workspaces,number_to_subsample,2,'Replace',false);
    end
    Include = ones(1,length(workspaces));
    
    
    
    for compartment = Compartment_to_test_start:Compartment_to_test_end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%% Analyze data from all cells %%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %%% LOAD IN THE DATA AND CONVERT TO HISTOGRAMS %%%
        tic; disp(['loading in the data for iteration ', num2str(bootstrap_iteration),'...']);
        AllData = []; %for storing data
        TotalFrames = 0; %for counting how many frames
        TotalLocs = 0; %for counting the total number of localizations
        
        %Check whether the data is pooled or not:
        %INPUT IS NOT A STRUCTURE:
        %LOAD EACH CELL FROM A SINGLE REPLICATE
        %Load data
        for i=1:length(workspaces)
            %Only load a dataset if it is to be included
            if Include(i) == 1
                load(workspaces{i});
                if compartment == 1
                    trackedPar_to_analyze = trackedPar_nuclear;
                elseif compartment == 2
                    trackedPar_to_analyze = trackedPar_insideRC;
                elseif compartment == 3
                    trackedPar_to_analyze = trackedPar_outside;
                end
                
                if ~isempty(fieldnames(trackedPar_to_analyze));
                    AllData = [AllData trackedPar_to_analyze];
                    
                    for n=1:length(trackedPar_to_analyze)
                        
                        TotalLocs = TotalLocs + length(trackedPar_to_analyze(1,n).Frame);
                        
                    end
                    %Find total frames using a slight ad-hoc way: find the last frame with
                    %a localization and round it. This is not an elegant solution, but it
                    %works for your particle density:
                    TempLastFrame = max(trackedPar_to_analyze(1,end).Frame);
                    TotalFrames = TotalFrames + 100*round(TempLastFrame/100);
                end
            else
                continue
            end
        end
        toc;
        
        
        
        
        %Compile histograms for each jump lengths
        tic; disp('compiling histogram of jump lengths...');
        Min3Traj = 0; %for counting number of min3 trajectories;
        TotalJumps = 0; %for counting the total number of jumps
        TrajLengthHist = zeros(1,length(AllData));
        %Calculate a histogram of translocation lengths vs. frames
        TransFrames = TimePoints+GapsAllowed*(TimePoints-1); TransLengths = struct;
        for i=1:TransFrames
            TransLengths(1,i).Step = []; %each iteration is a different number of timepoints
        end
        if UseAllTraj == 1 %Use all of the trajectory
            for i=1:length(AllData)
                % save length of the trajectory
                TrajLengthHist(1,i) = max(AllData(i).Frame) - min(AllData(i).Frame) + 1;
                CurrTrajLength = size(AllData(i).xy,1);
                %save lengths
                if CurrTrajLength >= 3
                    Min3Traj = Min3Traj + 1;
                end
                %Now loop through the trajectory. Keep in mind that there are missing
                %timepoints in the trajectory, so some gaps may be for multiple
                %timepoints.
                %Figure out what the max jump to consider is:
                HowManyFrames = min(TimePoints-1, CurrTrajLength);
                if CurrTrajLength > 1
                    TotalJumps = TotalJumps + CurrTrajLength - 1; %for counting all the jumps
                    for n=1:HowManyFrames
                        for k=1:CurrTrajLength-n
                            %Find the current XY coordinate and frames between
                            %timepoints
                            CurrXY_points = vertcat(AllData(i).xy(k,:), AllData(i).xy(k+n,:));
                            CurrFrameJump = AllData(i).Frame(k+n) - AllData(i).Frame(k);
                            %Calculate the distance between the pair of points
                            TransLengths(1,CurrFrameJump).Step = horzcat(TransLengths(1,CurrFrameJump).Step, pdist(CurrXY_points));
                        end
                    end
                end
            end
        elseif UseAllTraj == 0 %Use only the first JumpsToConsider timepoints
            for i=1:length(AllData)
                % save length of the trajectory
                TrajLengthHist(1,i) = max(AllData(i).Frame) - min(AllData(i).Frame) + 1;
                CurrTrajLength = size(AllData(i).xy,1);
                if CurrTrajLength >= 3
                    Min3Traj = Min3Traj + 1;
                end
                %Loop through the trajectory. If it is a short trajectory, you need to
                %make sure that you do not overshoot. So first figure out how many
                %jumps you can consider.
                %Figure out what the max jump to consider is:
                HowManyFrames = min([TimePoints-1, CurrTrajLength]);
                if CurrTrajLength > 1
                    TotalJumps = TotalJumps + CurrTrajLength - 1; %for counting all the jumps
                    for n=1:HowManyFrames
                        FrameToStop = min([CurrTrajLength, n+JumpsToConsider]);
                        for k=1:FrameToStop-n
                            %Find the current XY coordinate and frames between
                            %timepoints
                            CurrXY_points = vertcat(AllData(i).xy(k,:), AllData(i).xy(k+n,:));
                            CurrFrameJump = AllData(i).Frame(k+n) - AllData(i).Frame(k);
                            %Calculate the distance between the pair of points
                            TransLengths(1,CurrFrameJump).Step = horzcat(TransLengths(1,CurrFrameJump).Step, pdist(CurrXY_points));
                        end
                    end
                end
            end
        end
        %CALCULATE THE PDF HISTOGRAMS
        JumpProb = zeros(TimePoints-1, length(HistVecJumps));
        JumpProbFine = zeros(TimePoints-1, length(HistVecJumpsCDF));
        for i=1:size(JumpProb,1)
            JumpProb(i,:) = histc(TransLengths(1,i).Step, HistVecJumps)/length(TransLengths(1,i).Step);
            JumpProbFine(i,:) = histc(TransLengths(1,i).Step, HistVecJumpsCDF)/length(TransLengths(1,i).Step);
        end
        
        %CALCULATE THE CDF HISTOGRAMS:
        JumpProbCDF = zeros(TimePoints-1, length(HistVecJumpsCDF));
        for i=1:size(JumpProbCDF,1)
            for j=2:size(JumpProbCDF,2)
                JumpProbCDF(i,j) = sum(JumpProbFine(i,1:j));
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%Identify compartment switching %%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if compartment == 2
            Inside_jumps = [];
            Outside_jumps = [];
            Entering_jumps = [];
            Leaving_jumps = [];
            for struct_entry = 1:length(AllData) %Grab each trajectory from the concatenated "AllData" trajectories
                TrajLength = size(AllData(struct_entry).Frame,1);
                if TrajLength > 1 %Check if it has more than one localization
                    currentLoc = AllData(struct_entry).Frame(1);
                    currentXY = AllData(struct_entry).xy(1,:);
                    current_compartment = AllData(struct_entry).compartment(1);
                    fraction_inside = sum(AllData(struct_entry).compartment) / length(AllData(struct_entry).compartment);
                    for jump = 1:(TrajLength - 1); %For each jump from localization n to n + 1, get the distance and the type of jump (Inside, Outside, Entering, Exiting)
                        nextLoc = AllData(struct_entry).Frame(jump + 1);
                        nextXY = AllData(struct_entry).xy(jump + 1,:);
                        next_compartment = AllData(struct_entry).compartment(jump + 1);
                        if nextLoc == currentLoc + 1
                            jumpLength = pdist([currentXY;nextXY]);
                            if jumpLength > jump_length_minimum_dist
                                if and(current_compartment,next_compartment) %both localizations inside
                                    Inside_jumps = vertcat(Inside_jumps, jumpLength);
                                elseif and(~current_compartment,next_compartment) %going from outside to inside
                                    Entering_jumps = vertcat(Entering_jumps, jumpLength);
                                elseif and(current_compartment,~next_compartment) %going from inside to outside
                                    Leaving_jumps = vertcat(Leaving_jumps, jumpLength);
                                elseif and(~current_compartment,~next_compartment)
                                    Outside_jumps = vertcat(Outside_jumps, jumpLength);
                                end
                            else
                            end
                        end
                        currentLoc = AllData(struct_entry).Frame(jump + 1);
                        currentXY = AllData(struct_entry).xy(jump + 1,:);
                        current_compartment = AllData(struct_entry).compartment(jump + 1);
                    end
                end
            end
            Compartment_jump_stats_bootstrap(1,1:4,bootstrap_iteration) = [length(Inside_jumps), mean(Inside_jumps), median(Inside_jumps), std(Inside_jumps,1,1)];
            Compartment_jump_stats_bootstrap(2,1:4,bootstrap_iteration) = [length(Outside_jumps), mean(Outside_jumps), median(Outside_jumps), std(Outside_jumps,1,1)];
            Compartment_jump_stats_bootstrap(3,1:4,bootstrap_iteration) = [length(Entering_jumps), mean(Entering_jumps), median(Entering_jumps), std(Entering_jumps,1,1)];
            Compartment_jump_stats_bootstrap(4,1:4,bootstrap_iteration) = [length(Leaving_jumps), mean(Leaving_jumps), median(Leaving_jumps), std(Leaving_jumps,1,1)];
            EnterExit_ratio(bootstrap_iteration) = Compartment_jump_stats_bootstrap(3,1,bootstrap_iteration) / Compartment_jump_stats_bootstrap(4,1,bootstrap_iteration);
        end
        toc;
        
        
        
        
        if ModelFit > 0
            
            %Lower and Upper parameter bounds
            LB = [D_Free(1,1) D_Bound(1,1) Frac_Bound(1,1)];
            UB = [D_Free(1,2) D_Bound(1,2) Frac_Bound(1,2)];
            diff = UB - LB; %difference: used for initial parameters guess
            best_ssq2 = 5e10; %initial error
            
            
            %Need to ensure that the x-input is the same size as y-output
            if ModelFit == 1
                ModelHistVecJumps = zeros(size(JumpProb,1), length(HistVecJumps));
                for i = 1:size(JumpProb,1)
                    ModelHistVecJumps(i,:) = HistVecJumps;
                end
            elseif ModelFit == 2
                ModelHistVecJumps = zeros(size(JumpProb,1), length(HistVecJumpsCDF));
                for i = 1:size(JumpProb,1)
                    ModelHistVecJumps(i,:) = HistVecJumpsCDF;
                end
            end
            
            
            %%%%%%%%%%%%%%% NON-LINEAR LEAST SQUARED FITTING PROCEDURE %%%%%%%%%%%%
            
            %Options for the non-linear least squares parameter optimisation
            options = optimset('MaxIter',1000,'MaxFunEvals', 5000, 'TolFun',1e-8,'TolX',1e-8,'Display','on');
            
            %Guess a random set of parameters
            parameter_guess =rand(1,length(LB)).*diff+LB;
            
            %Do you want to fit the data by fitting to the histogram of jump
            %lengths (ModelFit == 1; PDF_or_CDF = 1) or to the CDF of jump
            %lengths (ModelFit == 2; PDF_or_CDF = 2)?
            
            if ModelFit == 1
                [values, ssq2,res] = lsqcurvefit('SS_2State_model_Z_corr_v4', parameter_guess, ModelHistVecJumps, JumpProb, LB, UB, options);
            elseif ModelFit == 2
                [values, ssq2,res] = lsqcurvefit('SS_2State_model_Z_corr_v4', parameter_guess, ModelHistVecJumps, JumpProbCDF, LB, UB, options);
            end
            
            %See if the current fit is an improvement:
            if ssq2 < best_ssq2
                best_vals = values;
                best_ssq2 = ssq2;
                %OUTPUT THE NEW BEST VALUES TO THE SCREEN
                disp(['Improved error is ', num2str(ssq2)]);
                for k = 1:length(best_vals)
                    disp([char(list_of_model_parameters{k}), ' = ', num2str(best_vals(k))]);
                end
                toc;
            end
            
            
            
            
            
            disp('============================================================')
            Fraction_bound = (round(best_vals(3)*1000)/1000);
            D_bound = (round(best_vals(2)*1000)/1000);
            D_free = (round(best_vals(1)*1000)/1000);
            
            
            if compartment == 1
                Tracking_stats_bootstrap(bootstrap_iteration,:) = [Fraction_bound, D_bound, D_free];
            elseif compartment == 2
                Tracking_stats_inside_bootstrap = [Fraction_bound, D_bound, D_free];
            elseif compartment == 3
                Tracking_stats_outside_bootstrap = [Fraction_bound, D_bound, D_free];
                Tracking_stats_bootstrap(bootstrap_iteration,:) = horzcat(Tracking_stats_outside_bootstrap,Tracking_stats_inside_bootstrap);
            end
        end
    end
end

Tracking_stats_bootstrap_mean = mean(Tracking_stats_bootstrap,1);
Tracking_stats_bootstrap_stdev = std(Tracking_stats_bootstrap,1,1);


%% Plot pooled data
workspaces = all_workspaces;
Include = ones(1,length(all_workspaces));
for compartment = Compartment_to_test_start:2%Compartment_to_test_end
    %%% LOAD IN THE DATA AND CONVERT TO HISTOGRAMS %%%
    tic; disp('loading in the data...');
    AllData = []; %for storing data
    TotalFrames = 0; %for counting how many frames
    TotalLocs = 0; %for counting the total number of localizations
    
    %INPUT IS NOT A STRUCTURE:
    %LOAD EACH CELL FROM A SINGLE REPLICATE
    %Load data
    for i=1:length(workspaces)
        %Only load a dataset if it is to be included
        if Include(i) == 1
            load(workspaces{i});
            if compartment == 1
                trackedPar_to_analyze = trackedPar_nuclear;
            elseif compartment == 2
                trackedPar_to_analyze = trackedPar_insideRC;
            elseif compartment == 3
                trackedPar_to_analyze = trackedPar_outside;
            end
            
            if ~isempty(fieldnames(trackedPar_to_analyze));
                AllData = [AllData trackedPar_to_analyze];
                
                for n=1:length(trackedPar_to_analyze)
                    
                    TotalLocs = TotalLocs + length(trackedPar_to_analyze(1,n).Frame);
                    
                end
                %Find total frames using a slight ad-hoc way: find the last frame with
                %a localization and round it. This is not an elegant solution, but it
                %works for your particle density:
                TempLastFrame = max(trackedPar_to_analyze(1,end).Frame);
                TotalFrames = TotalFrames + 100*round(TempLastFrame/100);
            end
        else
            continue
        end
    end
    
    toc;
    
    
    
    
    
    
    
    %Compile histograms for each jump lengths
    tic; disp('compiling histogram of jump lengths...');
    Min3Traj = 0; %for counting number of min3 trajectories;
    TotalJumps = 0; %for counting the total number of jumps
    TrajLengthHist = zeros(1,length(AllData));
    %Calculate a histogram of translocation lengths vs. frames
    TransFrames = TimePoints+GapsAllowed*(TimePoints-1); TransLengths = struct;
    for i=1:TransFrames
        TransLengths(1,i).Step = []; %each iteration is a different number of timepoints
    end
    if UseAllTraj == 1 %Use all of the trajectory
        for i=1:length(AllData)
            % save length of the trajectory
            TrajLengthHist(1,i) = max(AllData(i).Frame) - min(AllData(i).Frame) + 1;
            CurrTrajLength = size(AllData(i).xy,1);
            %save lengths
            if CurrTrajLength >= 3
                Min3Traj = Min3Traj + 1;
            end
            %Now loop through the trajectory. Keep in mind that there are missing
            %timepoints in the trajectory, so some gaps may be for multiple
            %timepoints.
            %Figure out what the max jump to consider is:
            HowManyFrames = min(TimePoints-1, CurrTrajLength);
            if CurrTrajLength > 1
                TotalJumps = TotalJumps + CurrTrajLength - 1; %for counting all the jumps
                for n=1:HowManyFrames
                    for k=1:CurrTrajLength-n
                        %Find the current XY coordinate and frames between
                        %timepoints
                        CurrXY_points = vertcat(AllData(i).xy(k,:), AllData(i).xy(k+n,:));
                        CurrFrameJump = AllData(i).Frame(k+n) - AllData(i).Frame(k);
                        %Calculate the distance between the pair of points
                        TransLengths(1,CurrFrameJump).Step = horzcat(TransLengths(1,CurrFrameJump).Step, pdist(CurrXY_points));
                    end
                end
            end
        end
    elseif UseAllTraj == 0 %Use only the first JumpsToConsider timepoints
        for i=1:length(AllData)
            % save length of the trajectory
            TrajLengthHist(1,i) = max(AllData(i).Frame) - min(AllData(i).Frame) + 1;
            CurrTrajLength = size(AllData(i).xy,1);
            if CurrTrajLength >= 3
                Min3Traj = Min3Traj + 1;
            end
            %Loop through the trajectory. If it is a short trajectory, you need to
            %make sure that you do not overshoot. So first figure out how many
            %jumps you can consider.
            %Figure out what the max jump to consider is:
            HowManyFrames = min([TimePoints-1, CurrTrajLength]);
            if CurrTrajLength > 1
                TotalJumps = TotalJumps + CurrTrajLength - 1; %for counting all the jumps
                for n=1:HowManyFrames
                    FrameToStop = min([CurrTrajLength, n+JumpsToConsider]);
                    for k=1:FrameToStop-n
                        %Find the current XY coordinate and frames between
                        %timepoints
                        CurrXY_points = vertcat(AllData(i).xy(k,:), AllData(i).xy(k+n,:));
                        CurrFrameJump = AllData(i).Frame(k+n) - AllData(i).Frame(k);
                        %Calculate the distance between the pair of points
                        TransLengths(1,CurrFrameJump).Step = horzcat(TransLengths(1,CurrFrameJump).Step, pdist(CurrXY_points));
                    end
                end
            end
        end
    end
    %CALCULATE THE PDF HISTOGRAMS
    JumpProb = zeros(TimePoints-1, length(HistVecJumps));
    JumpProbFine = zeros(TimePoints-1, length(HistVecJumpsCDF));
    for i=1:size(JumpProb,1)
        JumpProb(i,:) = histc(TransLengths(1,i).Step, HistVecJumps)/length(TransLengths(1,i).Step);
        JumpProbFine(i,:) = histc(TransLengths(1,i).Step, HistVecJumpsCDF)/length(TransLengths(1,i).Step);
    end
    
    %CALCULATE THE CDF HISTOGRAMS:
    JumpProbCDF = zeros(TimePoints-1, length(HistVecJumpsCDF));
    for i=1:size(JumpProbCDF,1)
        for j=2:size(JumpProbCDF,2)
            JumpProbCDF(i,j) = sum(JumpProbFine(i,1:j));
        end
    end
    toc;
    
    %PLOT THE HISTOGRAM OF TRANSLOCATIONS
    figure('position',[200 200 300 300]); %[x y width height]
    histogram_spacer = 0.10;
    hold on;
    colour = jet;
    for i=size(JumpProb,1):-1:1
        new_level = (i-1)*histogram_spacer;%*y_max;
        colour_element = colour(round(i/size(JumpProb,1)*size(colour,1)),:);
        plot(HistVecJumps, new_level*ones(1,length(HistVecJumps)), 'k-', 'LineWidth', 1);
        for j=2:size(JumpProb,2)
            x1 = HistVecJumps(1,j-1); x2 = HistVecJumps(1,j);
            y1 = new_level; y2 = JumpProb(i,j-1)+new_level;
            patch([x1 x1 x2 x2], [y1 y2 y2 y1],colour_element);
        end
        text(0.6*max(HistVecJumps), new_level+0.5*histogram_spacer, ['\Delta\tau: ', num2str(TimeGap*i), ' ms'], 'HorizontalAlignment','left', 'FontSize',9, 'FontName', 'Helvetica');
    end
    axis([0 max(HistVecJumps) 0 1.05*(max(JumpProb(end,:))+(size(JumpProb,1)-1)*histogram_spacer)]);
    title(['Jump Probabilities for ', num2str(length(AllData)), ' trajectories'], 'FontSize',10, 'FontName', 'Helvetica');
    set(gca,'YColor','w')
    ylabel('Probability', 'FontSize',10, 'FontName', 'Helvetica', 'Color', 'k');
    xlabel('jump length \mu m', 'FontSize',10, 'FontName', 'Helvetica');
    set(gca,'YTickLabel',''); set(gca, 'YTick', []);
    hold off;
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%Identify compartment switching %%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if compartment == 2
        Inside_jumps = [];
        Outside_jumps = [];
        Entering_jumps = [];
        Leaving_jumps = [];
        TrajCounter = 1;
        for struct_entry = 1:length(AllData) %Grab each trajectory from the concatenated "AllData" trajectories
            TrajLength = size(AllData(struct_entry).Frame,1);
            if TrajLength > 1 %Check if it has more than one localization
                currentLoc = AllData(struct_entry).Frame(1);
                currentXY = AllData(struct_entry).xy(1,:);
                current_compartment = AllData(struct_entry).compartment(1);
                fraction_inside_all(TrajCounter) = sum(AllData(struct_entry).compartment) / length(AllData(struct_entry).compartment); %Find what fraction of localizations for a given trajectory are inside vs outside
                %Determine the number of times a particle crosses the
                %boundary
                if fraction_inside_all(TrajCounter) == 1
                    Number_of_crossings(TrajCounter) = 0;
                    AllData(struct_entry).Number_of_crossings = 0;
                else
                    if TrajLength > 2
                        [val,peaks] = findpeaks(AllData(struct_entry).compartment);
                        [val,valley] = findpeaks(AllData(struct_entry).compartment * -1);
                        if length(peaks) ~= length(valley)
                            Number_of_crossings(TrajCounter) = 2 * max([length(peaks),length(valley)]);
                            AllData(struct_entry).Number_of_crossings = 2 *max([length(peaks),length(valley)]);
                        elseif length(peaks) == 0
                            Number_of_crossings(TrajCounter) = 1;
                            AllData(struct_entry).Number_of_crossings = 1;
                        else
                            Number_of_crossings(TrajCounter) = 2 *  max([length(peaks),length(valley)]) + 1;
                            AllData(struct_entry).Number_of_crossings = 2 * max([length(peaks),length(valley)]) + 1;
                        end
                    else
                        Number_of_crossings(TrajCounter) = 1;
                        AllData(struct_entry).Number_of_crossings = 1;
                    end
                    
                end
                
                for jump = 1:(TrajLength - 1); %For each jump from localization n to n + 1, get the distance and the type of jump (Inside, Outside, Entering, Exiting)
                    nextLoc = AllData(struct_entry).Frame(jump + 1);
                    nextXY = AllData(struct_entry).xy(jump + 1,:);
                    next_compartment = AllData(struct_entry).compartment(jump + 1);
                    if nextLoc == currentLoc + 1
                        jumpLength = pdist([currentXY;nextXY]);
                        if jumpLength > jump_length_minimum_dist
                            if and(current_compartment,next_compartment) %both localizations inside
                                Inside_jumps = vertcat(Inside_jumps, jumpLength);
                            elseif and(~current_compartment,next_compartment) %going from outside to inside
                                Entering_jumps = vertcat(Entering_jumps, jumpLength);
                            elseif and(current_compartment,~next_compartment) %going from inside to outside
                                Leaving_jumps = vertcat(Leaving_jumps, jumpLength);
                            elseif and(~current_compartment,~next_compartment)
                                Outside_jumps = vertcat(Outside_jumps, jumpLength);
                            end
                        else
                        end
                    end
                    currentLoc = AllData(struct_entry).Frame(jump + 1);
                    currentXY = AllData(struct_entry).xy(jump + 1,:);
                    current_compartment = AllData(struct_entry).compartment(jump + 1);
                end
                TrajCounter = TrajCounter + 1;
            end
        end
        %Compute general statistics of the new distance distributions:
        %       Size    Mean    Median StDev
        % Ins
        % Out
        % Ent
        % Ext
        
        Compartment_jump_stats_all(1,1:4) = [length(Inside_jumps), mean(Inside_jumps), median(Inside_jumps), std(Inside_jumps,1,1)];
        Compartment_jump_stats_all(2,1:4) = [length(Outside_jumps), mean(Outside_jumps), median(Outside_jumps), std(Outside_jumps,1,1)];
        Compartment_jump_stats_all(3,1:4) = [length(Entering_jumps), mean(Entering_jumps), median(Entering_jumps), std(Entering_jumps,1,1)];
        Compartment_jump_stats_all(4,1:4) = [length(Leaving_jumps), mean(Leaving_jumps), median(Leaving_jumps), std(Leaving_jumps,1,1)];
        
        if ~isempty(Inside_jumps)
            figure('position',[100 100 1200 800])
            s1 = subplot(4,5,1:4);
            InsHist = histogram(Inside_jumps,[jump_length_minimum_dist:0.03:1.1*round(max(Inside_jumps))],'Normalization','PDF');
            yMaxVal = ylim;
            hold on
            line([Compartment_jump_stats_all(1,3),Compartment_jump_stats_all(1,3)],yMaxVal,'Color','k','LineStyle','--','LineWidth',2);
            InsHist.FaceColor = [166/256,206/256, 227/256];
            hold off
            title('Jump lengths within Replication Compartments');
            ylabel('Probability','FontSize',12, 'FontName', 'Helvetica');
            if ~isempty(Outside_jumps)
                s2 = subplot(4,5,6:9);
                OutHist =histogram(Outside_jumps,[jump_length_minimum_dist:0.03:1.1*round(max(Inside_jumps))],'Normalization','PDF');
                yMaxVal = ylim;
                hold on
                line([Compartment_jump_stats_all(2,3),Compartment_jump_stats_all(2,3)],yMaxVal,'Color','k','LineStyle','--','LineWidth',2);
                hold off
                OutHist.FaceColor = [31/256,120/256, 180/256];
                title('Jump lengths outside Replication Compartments');
                ylabel('Probability','FontSize',12, 'FontName', 'Helvetica');
            end
            if ~isempty(Entering_jumps)
                s3 = subplot(4,5,11:14);
                EntHist = histogram(Entering_jumps,[jump_length_minimum_dist:0.03:1.1*round(max(Inside_jumps))],'Normalization','PDF');
                yMaxVal = ylim;
                hold on
                line([Compartment_jump_stats_all(3,3),Compartment_jump_stats_all(3,3)],yMaxVal,'Color','k','LineStyle','--','LineWidth',2);
                hold off
                EntHist.FaceColor = [178/256,223/256, 138/256];
                title('Jump lengths entering Replication Compartments');
                ylabel('Probability','FontSize',12, 'FontName', 'Helvetica');
            end
            if ~isempty(Leaving_jumps)
                s4 = subplot(4,5,16:19);
                ExtHist = histogram(Leaving_jumps,[jump_length_minimum_dist:0.03:1.1*round(max(Inside_jumps))],'Normalization','PDF');
                yMaxVal = ylim;
                hold on
                line([Compartment_jump_stats_all(4,3),Compartment_jump_stats_all(4,3)],yMaxVal,'Color','k','LineStyle','--','LineWidth',2);
                hold off
                ExtHist.FaceColor = [51/256,160/256, 44/256];
                title('Jump lengths leaving Replication Compartments');
                ylabel('Probability','FontSize',12, 'FontName', 'Helvetica');
                xlabel('Jump Length (�M)','FontSize',12, 'FontName', 'Helvetica');
            end
        end
        s5 = subplot(4,5,[5,10]);
        RatioHist = histogram(EnterExit_ratio,'Normalization','PDF');
        yMaxVal = ylim;
        hold on
        RatioHist.FaceColor = [50/256,50/256, 50/256];
        hold off
        title('Entering/Exiting ratio');
        ylabel('Probability','FontSize',12, 'FontName', 'Helvetica');
        
        s6 = subplot(4,5,[15,20]);
        RatioHist = histogram(Number_of_crossings,[0:1:max(Number_of_crossings)],'Normalization','PDF');
        hold on
        RatioHist.FaceColor = [50/256,50/256, 50/256];
        hold off
        title('Crossing frequency');
        ylabel('Probability','FontSize',12, 'FontName', 'Helvetica');
        
    end
    
    if ModelFit > 0
        
        %Lower and Upper parameter bounds
        LB = [D_Free(1,1) D_Bound(1,1) Frac_Bound(1,1)];
        UB = [D_Free(1,2) D_Bound(1,2) Frac_Bound(1,2)];
        diff = UB - LB; %difference: used for initial parameters guess
        best_ssq2 = 5e10; %initial error
        
        
        %Need to ensure that the x-input is the same size as y-output
        if ModelFit == 1
            ModelHistVecJumps = zeros(size(JumpProb,1), length(HistVecJumps));
            for i = 1:size(JumpProb,1)
                ModelHistVecJumps(i,:) = HistVecJumps;
            end
        elseif ModelFit == 2
            ModelHistVecJumps = zeros(size(JumpProb,1), length(HistVecJumpsCDF));
            for i = 1:size(JumpProb,1)
                ModelHistVecJumps(i,:) = HistVecJumpsCDF;
            end
        end
        
        
        %%%%%%%%%%%%%%% NON-LINEAR LEAST SQUARED FITTING PROCEDURE %%%%%%%%%%%%
        
        %Options for the non-linear least squares parameter optimisation
        options = optimset('MaxIter',1000,'MaxFunEvals', 5000, 'TolFun',1e-8,'TolX',1e-8,'Display','on');
        
        for i=1:iterations
            tic;
            %Guess a random set of parameters
            parameter_guess =rand(1,length(LB)).*diff+LB;
            
            %Do you want to fit the data by fitting to the histogram of jump
            %lengths (ModelFit == 1; PDF_or_CDF = 1) or to the CDF of jump
            %lengths (ModelFit == 2; PDF_or_CDF = 2)?
            
            if ModelFit == 1
                [values, ssq2,res] = lsqcurvefit('SS_2State_model_Z_corr_v4', parameter_guess, ModelHistVecJumps, JumpProb, LB, UB, options);
            elseif ModelFit == 2
                [values, ssq2,res] = lsqcurvefit('SS_2State_model_Z_corr_v4', parameter_guess, ModelHistVecJumps, JumpProbCDF, LB, UB, options);
            end
            
            %See if the current fit is an improvement:
            if ssq2 < best_ssq2
                best_vals = values;
                best_ssq2 = ssq2;
                %OUTPUT THE NEW BEST VALUES TO THE SCREEN
                disp('==================================================');
                disp(['Improved fit on iteration ', num2str(i)]);
                disp(['Improved error is ', num2str(ssq2)]);
                for k = 1:length(best_vals)
                    disp([char(list_of_model_parameters{k}), ' = ', num2str(best_vals(k))]);
                end
                disp('==================================================');
            else
                disp(['Iteration ', num2str(i), ' did not yield an improved fit']);
            end
            toc;
        end
        
        
        % PLOT THE SURVIVAL PROBABILITY OF THE DYE
        %Bin into a full histogram:
        HistVec = 1:1:max(TrajLengthHist);
        TrajLengthProb = histc(TrajLengthHist, HistVec)./length(TrajLengthHist);
        TrajLengthCDF = zeros(1,length(TrajLengthProb));
        for i=2:length(TrajLengthProb)
            TrajLengthCDF(1,i) = sum(TrajLengthProb(1:i));
        end
        SurvivalProb = 1-TrajLengthCDF;
        figure('position',[800 100 300 275]); %[x y width height]
        hold on;
        plot(HistVec, SurvivalProb, 'ko', 'MarkerSize', 6, 'MarkerFaceColor', 'r');
        axis([1 51 0.001 1.01]);
        title(['1-CDF of trajectory lengths; mean = ', num2str(mean(TrajLengthHist)), ' frames'], 'FontSize',9, 'FontName', 'Helvetica');
        ylabel('1-CDF', 'FontSize',8, 'FontName', 'Helvetica');
        xlabel('number of frames', 'FontSize',8, 'FontName', 'Helvetica');
        set(gca,'yscale','log');
        hold off;
        
        %PLOT a FIT OF THE CONTINUOUS MODEL TO THE DATA
        %r = min(HistVecJumps):0.001:1.05*max(HistVecJumps);
        r = HistVecJumpsCDF;
        y = SS_2State_model_PLOT_Z_corr_v4( best_vals, JumpProb, r );
        norm_y = zeros(size(y,1), size(y,2));
        CDF_y = zeros(size(y,1), size(y,2));
        %Normalize y as a PDF
        for i=1:size(y,1)
            norm_y(i,:) = y(i,:)./sum(y(i,:));
        end
        %scale y for plotting next to histograms
        scaled_y = (length(r)/length(HistVecJumps)).*norm_y;
        
        %Calculate a CDF version of y
        for i=1:size(CDF_y,1)
            for j=2:size(CDF_y,2)
                CDF_y(i,j) = sum(norm_y(i,1:j));
            end
        end
        
        %PLOT CDFs of TRANSLOCATIONS AND OF FIT
        figure('position',[100 100 1200 800]); %[x y width height]
        colour = jet;
        for i=1:min([12 size(JumpProbCDF,1)])
            colour_element = colour(round(i/size(JumpProbCDF,1)*size(colour,1)),:);
            subplot(3,4,i);
            hold on;
            plot(HistVecJumpsCDF, JumpProbCDF(i,:), '-', 'LineWidth', 2, 'Color', colour_element);
            plot(HistVecJumpsCDF, CDF_y(i,:), 'k-', 'LineWidth', 1);
            
            axis([min(HistVecJumpsCDF) max(HistVecJumpsCDF) 0 1.05]);
            title(['CDF for \Delta\tau: ', num2str(TimeGap*i), ' ms'], 'FontSize',10, 'FontName', 'Helvetica');
            ylabel('jump length CDF', 'FontSize',10, 'FontName', 'Helvetica');
            xlabel('jump length (um)', 'FontSize',10, 'FontName', 'Helvetica');
            legend('raw data', 'Model fit', 'Location', 'SouthEast');
            legend boxoff
            
            hold off;
        end
        
        %PLOT SINGLE PLOT WITH ALL OF THE CDFs:
        figure('position',[600 200 300 250]); %[x y width height]
        hold on;
        colour = jet;
        LegendNames = {''};
        for i=1:min([12 size(JumpProbCDF,1)])
            colour_element = colour(round(i/size(JumpProbCDF,1)*size(colour,1)),:);
            plot(HistVecJumpsCDF, JumpProbCDF(i,:), '-', 'LineWidth', 2, 'Color', colour_element);
            h = plot(HistVecJumpsCDF, CDF_y(i,:), 'k-', 'LineWidth', 1);
            if i < size(JumpProbCDF,1)
                %only include black line in legend once
                set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
            end
            LegendNames{i} = ['\Delta\tau: ', num2str(TimeGap*i), ' ms'];
            if i == size(JumpProbCDF,1)
                LegendNames{i+1} = 'model fit';
            end
            
        end
        axis([min(HistVecJumpsCDF) max(HistVecJumpsCDF) 0 1.05]);
        title(['CDF model fit'], 'FontSize',10, 'FontName', 'Helvetica');
        ylabel('jump length CDF', 'FontSize',10, 'FontName', 'Helvetica');
        xlabel('jump length (um)', 'FontSize',10, 'FontName', 'Helvetica');
        legend(LegendNames, 'Location', 'SouthEast');
        %set(gca,'yscale','log')
        legend boxoff
        hold off;
        
        %PLOT THE HISTOGRAM OF TRANSLOCATIONS
        format short
        figure('position',[200 200 300 400]); %[x y width height]
        histogram_spacer = 0.055;
        hold on;
        colour = jet;
        for i=size(JumpProb,1):-1:1
            new_level = (i-1)*histogram_spacer;%*y_max;
            colour_element = colour(round(i/size(JumpProb,1)*size(colour,1)),:);
            plot(HistVecJumps, new_level*ones(1,length(HistVecJumps)), 'k-', 'LineWidth', 1);
            for j=2:size(JumpProb,2)
                x1 = HistVecJumps(1,j-1); x2 = HistVecJumps(1,j);
                y1 = new_level; y2 = JumpProb(i,j-1)+new_level;
                patch([x1 x1 x2 x2], [y1 y2 y2 y1],colour_element);
            end
            plot(r, scaled_y(i,:)+new_level, 'k-', 'LineWidth', 2);
            text(0.6*max(HistVecJumps), new_level+0.5*histogram_spacer, ['\Delta\tau: ', num2str(TimeGap*i), ' ms'], 'HorizontalAlignment','left', 'FontSize',9, 'FontName', 'Helvetica');
        end
        %axis([0 max(HistVecJumps) 0 1.05*(max(JumpProb(end,:))+(size(JumpProb,1)-1)*histogram_spacer)]);
        axis([0 1.05 0 1.05*(max(JumpProb(end,:))+(size(JumpProb,1)-1)*histogram_spacer)]);
        title({SampleName; ['2-state model fit to raw data; Fit Type = ' num2str(ModelFit)]; ['Dfree = ', num2str(round(best_vals(1)*1000)/1000), '; Dbound = ', num2str(round(best_vals(2)*1000)/1000), '; FracBound = ', num2str(round(best_vals(3)*1000)/1000)]; ['Total trajectories: ', num2str(length(AllData)), '; => Length 3 trajectories: ', num2str(Min3Traj)]; ['Total Locs = ', num2str(TotalLocs), '; Locs/frame = ', num2str(round(TotalLocs/TotalFrames*1000)/1000), ';  Total jumps: ', num2str(TotalJumps), ';']}, 'FontSize',9, 'FontName', 'Helvetica');
        set(gca,'YColor','w')
        ylabel('Probability', 'FontSize',10, 'FontName', 'Helvetica', 'Color', 'k');
        xlabel('jump length \mu m', 'FontSize',10, 'FontName', 'Helvetica');
        set(gca,'YTickLabel',''); set(gca, 'YTick', []);
        hold off;
        
        
        Fraction_bound = (round(best_vals(3)*1000)/1000);
        D_bound = (round(best_vals(2)*1000)/1000);
        D_free = (round(best_vals(1)*1000)/1000);
        
        
        if compartment == 1
            Tracking_stats = [Fraction_bound, D_bound, D_free];
            Tracking_stats_all = vertcat(Tracking_stats,Tracking_stats_bootstrap_mean,Tracking_stats_bootstrap_stdev);
        elseif compartment == 2
            Tracking_stats_inside = [Fraction_bound, D_bound, D_free];
        elseif compartment == 3
            Tracking_stats_outside = [Fraction_bound, D_bound, D_free];
            Tracking_stats = horzcat(Tracking_stats_outside,Tracking_stats_inside);
            Tracking_stats_all = vertcat(Tracking_stats,Tracking_stats_bootstrap_mean,Tracking_stats_bootstrap_stdev);
        end
        
        
        if SavePDF == 1
            
            if compartment == 1
                %print([output_path, SampleName, 'All_traj_FitType',num2str(ModelFit), '.pdf'], '-dpdf');
                print([output_path, SampleName, 'All_traj_FitType',num2str(ModelFit), '.eps'], '-depsc');
                dlmwrite([output_path, SampleName, '_bootstrapped_fits.txt'], Tracking_stats_all, '\t');
            elseif compartment == 2
                %print([output_path, SampleName, '_Inside_RC_FitType',num2str(ModelFit), '.pdf'], '-dpdf');
                print([output_path, SampleName, '_Inside_RC_FitType',num2str(ModelFit), '.eps'], '-depsc');
            elseif compartment == 3
                %print([output_path, SampleName, '_Outside_RC_FitType',num2str(ModelFit), '.pdf'], '-dpdf');
                %print([output_path, SampleName, '_Outside_RC_FitType',num2str(ModelFit), '.eps'], '-depsc');
                dlmwrite([output_path, SampleName, '_bootstrap_summary.txt'], Tracking_stats_all, '\t');
                dlmwrite([output_path, SampleName, '_all_iterations.txt'], Tracking_stats_bootstrap, '\t');
            end
        end
    end
end
