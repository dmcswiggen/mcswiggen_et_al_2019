%SpotOn_RegionMask_BootstrapPlus
%Spot On by Anders Sejr Hansen and Maxime Woringer, July 2017
%Implementation by David McSwiggen, 2018

%Batch_Process_Annotated_Directory
%%%%%%%Copyright (C) 2018 David McSwiggen

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script uses files generated in the "Batch_Process_Annotated_Directory.m"
%%%%%%% Matlab script, and performs multiple analytical tests on these
%%%%%%% trajectories. Primarily, it uses the Spot On core code available
%%%%%%% here: https://gitlab.com/tjian-darzacq-lab/spot-on-matlab and
%%%%%%% incorporates other measurements includeing Angular Displacement and
%%%%%%% MSD analysis. For all of these steps, the code will perform
%%%%%%% bootstrap subsampling to subsample the data from N cells randomly
%%%%%%% for M iterations to give the variability of the data.

%%%%%%% For more information on Spot On, or setting parameters to fit SPT
%%%%%%% data, please refer to the original Spot On documentation. To
%%%%%%% address specific questions, please email dmcswiggen@berkeley.edu or
%%%%%%% direct them to the corresponding authors of the McSwiggen, et. al.
%%%%%%% manuscript.

%% Input Parameters

addpath(genpath('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT'));

%clear; clc; clearvars -global; close all;

%In this script we will fit fastSPT data to the simple BOUND-UNBOUND
%2-state model and determine the best-fit parameters
global LocError dT HistVecJumps dZ HistVecJumpsCDF ModelFit FitLocError Z_corr_a Z_corr_b JumpsPerdT UseWeights

%Dataset: determine which dataset to load
SampleNameAll = {'LacI_inf_all','LacI_inf_scr_all','LacI_uninf_all','LacI_inf_d2','LacI_inf_scr_d2','LacI_inf_d3','LacI_inf_scr_d3','LacI_uninf_d3',...
    'TetR_inf_all','TetR_inf_scr_all','TetR_uninf_all','TetR_inf_d2','TetR_inf_scr_d2','TetR_uninf_d2',...
    'EWS_inf_d1','EWS_inf_scr_d1','EWS_uninf_d1',...
    'FUS_inf_d1','FUS_inf_scr_d1','FUS_uninf_d1',...
    'FUS_inf_d1','FUS_inf_scr_d1','FUS_uninf_d1',...
    'NLS_uninf_scr_d1'};

% SampleNameAll = {'TetR_inf_d2','TetR_inf_scr_d2','TetR_uninf_d2',...
%     'EWS_inf_d1','EWS_inf_scr_d1','EWS_uninf_d1',...
%     'FUS_inf_d1','FUS_inf_scr_d1','FUS_uninf_d1',...
%     'FUS_inf_d1','FUS_inf_scr_d1','FUS_uninf_d1',...
%     'NLS_uninf_scr_d1'};

SampleAnnotationToUse = [1,2,1,1,2,1,2,1,...
                         1,2,1,1,2,1,1,2,1,...
                         2];
                     
input_path_all = {'/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Infected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Infected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Uninfected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Infected_D2/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Infected_D2/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Infected_D3/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Infected_D3/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Uninfected_D3/',...
    '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Infected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Infected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Uninfected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Infected_D2/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Infected_D2/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Uninfected_D2/',...
    '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-EWS/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-EWS/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-EWS/Uninfected/',...
    '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-FUS/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-FUS/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-FUS/Uninfected/',...
    '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-Taf15/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-Taf15/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-Taf15/Uninfected/',...
    '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-NLS'};

% input_path_all = {'/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Infected_D3/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Infected_D3/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-LacI/Uninfected_D3/',...
%     '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Infected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Infected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Uninfected_D1/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Infected_D2/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Infected_D2/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/Uninfected_D2/',...
%     '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-EWS/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-EWS/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-EWS/Uninfected/',...
%     '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-FUS/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-FUS/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-FUS/Uninfected/',...
%     '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-Taf15/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-Taf15/Infected/','/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-Taf15/Uninfected/',...
%     '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-NLS'};


output_path = '/Users/Davidmcswiggen/Google Drive/Lab_Stuff/Lab_notebook/DM_3_87_SPT/HMM_analysis/';
HSV_infected = 1;
AngleAnalysis = 1;
MSD_Analysis = 0;
SavePDF = 1; %Set to 1 if you want a PDF saved to the output path

Number_bootstrap_iterations = 1;
number_to_subsample = 1;
ModelFit = 1; %Use 1 for PDF-fitting; Use 2 for CDF-fitting
DoSingleCellFit = 0; %Set to 1 if you want to analyse all single cells individually.
TimeGap = 7.48; %Time between frames in milliseconds;
iterations = 2; %Manually input the desired number of fitting iterations:
jump_length_minimum_dist = 0.15;
LocError = 0.045; % If FitLocError=0, LocError in units of micrometers will be used.
FitLocError = 0; % If FitLocError=1, the localization error will fitted from the data

% For Asymmetry calculations
nBins = 24; %so 30 degrees per bin
rads = 0:2*pi/nBins:2*pi;
%Classify the states
BoundState = 1;
FreeState = 2;
minFree = 2;
minAngleNumber = 100;
MovingThreshold = 0:0.1:0.5; %a range of translocations
MinJumpThres = 3; %Element 3 in MovingThreshold vector, i.e. 200 nm;
MinMinJumpThres = 0.150; %This threshold is the minimum for both jumps making up the angle
MaxJumpAngle = 0.900; % Maximum jump length to consider. Very long jumps are likely to be result of tracking errors.
MinTrajLength = 3;
JackKnife_fraction = 0.5;
JackKnife_iterations = Number_bootstrap_iterations*10;
MinNumAngles = 3;
MaxAsymAnglesFrac = 0.5;
OffSet_thres = 1.26;
MSD_timepoints = 8;%For MSD plot
Constrain_MSD_Dfree = 0; %1 if you want to use the Dfree from the two-state model to constrain D, 0 if you want it as a free parameter

%%%% A NOTE ON MODEL FITTING %%%%
%Use ModelFit = 1 if you want to do fitting of the jump length histogram
%using PDF fitting.
%Use ModelFit = 2 if you want to do fitting of the jump length histogram
%using CDF fitting.
%When fitting to a CDF you can use much smaller bin sizes and thus occur
%smaller binning artefacts.
PDF_or_CDF = ModelFit;

%Data and fitting parameters
MaxJump = 2.05; BinWidth = 0.010; %For PDF fitting and plotting
TimePoints = 7; %TimePoints: how many jump lengths to use for the fitting: 3 timepoints, yields 2 jumps
UseAllTraj = 0; %Use =1 if you want to use all of a trajectory. This biases towards bound molecules, but useful for troubleshooting
JumpsToConsider = 4; % If UseAllTraj =0, then use no more than 3 jumps.
NumberOfStates = 2; % If NumberOfStates=2, a 2-state model will be used; If NumberOfStates=3, a 3-state model will be used
FitIterations = 1; % Input the desired number of fitting iterations (random initial parameter guess for each)
FitLocErrorRange = [0.010 0.075]; % min/max for model-fitted localization error in micrometers
UseWeights = 1; % If UseWeights=0, all TimePoints are given equal weights. If UseWeights=1, TimePoints are weighted according to how much data there is. E.g. 1dT will be weighted more than 5dT.
D_Free_2State = [0.05 25]; % min/max Diffusion constant for Free state in 2-state model (units um^2/s)
D_Bound_2State = [0.0001 0.03]; % min/max Diffusion constant for Bound state in 2-state model (units um^2/s)
D_Free1_3State = [0.5 25]; % min/max Diffusion constant #1 for Free state in 3-state model (units um^2/s)
D_Free2_3State = [0.5 25]; % min/max Diffusion constant #2 for Free state in 3-state model (units um^2/s)
D_Bound_3State = [0.0001 0.08]; % min/max Diffusion constant for Bound state in 3-state model (units um^2/s)
MaxJumpPlotPDF = 1.05; % the cut-off for displaying the displacement histograms plots
MaxJumpPlotCDF = 3.05; % the cut-off for displaying the displacement CDF plots
HistVecJumps = 0:BinWidth:MaxJump; %jump lengths in micrometers
HistVecJumpsCDF = 0:0.001:MaxJump; %jump lengths in micrometers
GapsAllowed = 1; %This is the number of missing frames that are allowed in a single trajectory
dT = TimeGap/1000; %Time in seconds
dZ = 0.700; %The axial illumination slice: measured to be roughly 700 nm
[Z_corr_a, Z_corr_b] = MatchZ_corr_coeff(dT, dZ, GapsAllowed);
screen_size_vector = get(0,'ScreenSize');



%% make sure the plots do not exceed the screen size:
plot_width = min([1200 screen_size_vector(3)]);
plot_height = min([800 screen_size_vector(4)]);
figure1 = figure; colour = parula; close;

%% Process all directories
for dataset = 1:length(SampleNameAll)
    input_path = input_path_all{dataset};
    SampleName = SampleNameAll{dataset};
    %% Read in all workspaces
    nd2_files=dir([input_path,'*.nd2']);
    Workspaces = ''; %for saving the actual file name
    IterOne = 1;
    for file = 1:length(nd2_files)
        if nd2_files(file).name(1) == '.'
            continue
        else
            if SampleAnnotationToUse(dataset) == 2
                if exist([input_path,nd2_files(file).name(1:end-4),'/',nd2_files(file).name(1:end-4),'_Tracked_scrambled_and_masked.mat'])
                    Workspaces{IterOne} = [input_path,nd2_files(file).name(1:end-4),'/',nd2_files(file).name(1:end-4),'_Tracked_scrambled_and_masked.mat'];
                    IterOne = IterOne + 1;
                elseif exist([input_path,nd2_files(file).name(1:end-4),'/',nd2_files(file).name(1:end-4),'_Tracked_and_masked.mat'])
                    Workspaces{IterOne} = [input_path,nd2_files(file).name(1:end-4),'/',nd2_files(file).name(1:end-4),'_Tracked_and_masked.mat'];
                    IterOne = IterOne + 1;
                end
            elseif SampleAnnotationToUse(dataset) == 1
                if exist([input_path,nd2_files(file).name(1:end-4),'/',nd2_files(file).name(1:end-4),'_Tracked_and_masked.mat'])
                    Workspaces{IterOne} = [input_path,nd2_files(file).name(1:end-4),'/',nd2_files(file).name(1:end-4),'_Tracked_and_masked.mat'];
                    IterOne = IterOne + 1;
                end
            end
        end
    end
    if HSV_infected == 1
        Compartment_to_test_start = 2;
        Compartment_to_test_end = 3;
    else
        Compartment_to_test_start = 1;
        Compartment_to_test_end = 1;
    end
    
    %% Sample through individual iterations
    if Number_bootstrap_iterations > 0
        Compartment_jump_stats_bootstrap = NaN(4,4,Number_bootstrap_iterations);
        insideHist = NaN(200,Number_bootstrap_iterations);
        outsideHist = NaN(200,Number_bootstrap_iterations);
        EnteringHist = NaN(200,Number_bootstrap_iterations);
        LeavingHist = NaN(200,Number_bootstrap_iterations);
        files_used = cell(Number_bootstrap_iterations,number_to_subsample + 1);
        for bootstrap_iteration = 1:Number_bootstrap_iterations
            loopStart = tic;
            disp(['Beginning iteration ',num2str(bootstrap_iteration),' of ',num2str(Number_bootstrap_iterations)]);
            if Number_bootstrap_iterations == 1
                workspaces = Workspaces;
            else
                workspaces = datasample(Workspaces,number_to_subsample,2,'Replace',false);
            end
            Include = ones(1,length(workspaces));
            
            
            
            for compartment = Compartment_to_test_start:Compartment_to_test_end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%% Analyze data from all cells %%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                %%% LOAD IN THE DATA AND CONVERT TO HISTOGRAMS %%%
                tic; disp('Loading in the data for randomly selected files...');
                AllData = []; %for storing data
                TotalFrames = 0; %for counting how many frames
                TotalLocs = 0; %for counting the total number of localizations
                
                %Check whether the data is pooled or not:
                %INPUT IS NOT A STRUCTURE:
                %LOAD EACH CELL FROM A SINGLE REPLICATE
                %Load data
                for dataset=1:length(workspaces)
                    %Only load a dataset if it is to be included
                    if Include(dataset) == 1
                        load(workspaces{dataset});
                        if compartment == 1
                            trackedPar_to_analyze = trackedPar_nuclear;
                            compartment_string = 'All_trajectories';
                        elseif compartment == 2
                            trackedPar_to_analyze = trackedPar_insideRC;
                            compartment_string = 'Inside_RC';
                        elseif compartment == 3
                            trackedPar_to_analyze = trackedPar_outside;
                            compartment_string = 'Outside_RC';
                        end
                        
                        if ~isempty(fieldnames(trackedPar_to_analyze))
                            AllData = [AllData trackedPar_to_analyze];
                            
                            for n=1:length(trackedPar_to_analyze)
                                TotalLocs = TotalLocs + length(trackedPar_to_analyze(1,n).Frame);  
                            end
                            %Find total frames using a slight ad-hoc way: find the last frame with
                            %a localization and round it. This is not an elegant solution, but it
                            %works for your particle density:
                            TempLastFrame = max(trackedPar_to_analyze(1,end).Frame);
                            TotalFrames = TotalFrames + 100*round(TempLastFrame/100);
                        end
                    else
                        continue
                    end
                end
                if isempty(AllData)
                    error('The data failed to properly load. Please check whether the parameters that you set ar correct');
                end
                toc;
                
                
                
                
                %Compile histograms for each jump lengths
                tic; disp('compiling histogram of jump lengths...');
                Min3Traj = 0; %for counting number of min3 trajectories;
                TotalJumps = 0; %for counting the total number of jumps
                TrajLengthHist = zeros(1,length(AllData));
                %Calculate a histogram of translocation lengths vs. frames
                TransFrames = TimePoints+GapsAllowed*(TimePoints-1); TransLengths = struct;
                for Frame=1:TransFrames
                    TransLengths(1,Frame).Step = []; %each iteration is a different number of timepoints
                end
                JumpsPerdT = zeros(TransFrames,1); % for counting how many jumps per dT
                
                if UseAllTraj == 1 %Use all of the trajectory
                    for i=1:length(AllData)
                        % save length of the trajectory
                        TrajLengthHist(1,i) = max(AllData(i).Frame) - min(AllData(i).Frame) + 1;
                        CurrTrajLength = size(AllData(i).xy,1);
                        %save lengths
                        if CurrTrajLength >= 3
                            Min3Traj = Min3Traj + 1;
                        end
                        %Now loop through the trajectory. Keep in mind that there are missing
                        %timepoints in the trajectory, so some gaps may be for multiple
                        %timepoints.
                        %Figure out what the max jump to consider is:
                        HowManyFrames = min(TimePoints-1, CurrTrajLength);
                        if CurrTrajLength > 1
                            TotalJumps = TotalJumps + CurrTrajLength - 1; %for counting all the jumps
                            for n=1:HowManyFrames
                                for k=1:CurrTrajLength-n
                                    %Find the current XY coordinate and frames between
                                    %timepoints
                                    CurrXY_points = vertcat(AllData(i).xy(k,:), AllData(i).xy(k+n,:));
                                    CurrFrameJump = AllData(i).Frame(k+n) - AllData(i).Frame(k);
                                    %Calculate the distance between the pair of points
                                    TransLengths(1,CurrFrameJump).Step = horzcat(TransLengths(1,CurrFrameJump).Step, pdist(CurrXY_points));
                                    %Count the number of jumps for a given delta T
                                    JumpsPerdT(CurrFrameJump) =  JumpsPerdT(CurrFrameJump) + 1;
                                end
                            end
                        end
                    end
                elseif UseAllTraj == 0 %Use only the first JumpsToConsider timepoints
                    for i=1:length(AllData)
                        % save length of the trajectory
                        TrajLengthHist(1,i) = max(AllData(i).Frame) - min(AllData(i).Frame) + 1;
                        CurrTrajLength = size(AllData(i).xy,1);
                        if CurrTrajLength >= 3
                            Min3Traj = Min3Traj + 1;
                        end
                        %Loop through the trajectory. If it is a short trajectory, you need to
                        %make sure that you do not overshoot. So first figure out how many
                        %jumps you can consider.
                        %Figure out what the max jump to consider is:
                        HowManyFrames = min([TimePoints-1, CurrTrajLength]);
                        if CurrTrajLength > 1
                            TotalJumps = TotalJumps + CurrTrajLength - 1; %for counting all the jumps
                            for n=1:HowManyFrames
                                FrameToStop = min([CurrTrajLength, n+JumpsToConsider]);
                                for k=1:FrameToStop-n
                                    %Find the current XY coordinate and frames between
                                    %timepoints
                                    CurrXY_points = vertcat(AllData(i).xy(k,:), AllData(i).xy(k+n,:));
                                    CurrFrameJump = AllData(i).Frame(k+n) - AllData(i).Frame(k);
                                    %Calculate the distance between the pair of points
                                    TransLengths(1,CurrFrameJump).Step = horzcat(TransLengths(1,CurrFrameJump).Step, pdist(CurrXY_points));
                                    %Count the number of jumps for a given delta T
                                    JumpsPerdT(CurrFrameJump) =  JumpsPerdT(CurrFrameJump) + 1;
                                end
                            end
                        end
                    end
                end
                
                if ~isempty(TransLengths(1,TimePoints-1).Step)
                    %CALCULATE THE PDF HISTOGRAMS
                    JumpProb = zeros(TimePoints-1, length(HistVecJumps));
                    JumpProbFine = zeros(TimePoints-1, length(HistVecJumpsCDF));
                    for i=1:size(JumpProb,1)
                        JumpProb(i,:) = histc(TransLengths(1,i).Step, HistVecJumps)/length(TransLengths(1,i).Step);
                        JumpProbFine(i,:) = histc(TransLengths(1,i).Step, HistVecJumpsCDF)/length(TransLengths(1,i).Step);
                    end
                    
                    %CALCULATE THE CDF HISTOGRAMS:
                    JumpProbCDF = zeros(TimePoints-1, length(HistVecJumpsCDF));
                    for i=1:size(JumpProbCDF,1)
                        for j=2:size(JumpProbCDF,2)
                            JumpProbCDF(i,j) = sum(JumpProbFine(i,1:j));
                        end
                    end
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    %%%%%%%%%%%%%%%%%Identify compartment switching %%%%%%%%%%%%%%%%%%%%%%%
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    if compartment == 2
                        Inside_jumps = [];
                        Outside_jumps = [];
                        Entering_jumps = [];
                        Leaving_jumps = [];
                        for struct_entry = 1:length(AllData) %Grab each trajectory from the concatenated "AllData" trajectories
                            TrajLength = size(AllData(struct_entry).Frame,1);
                            if TrajLength > 1 %Check if it has more than one localization
                                currentLoc = AllData(struct_entry).Frame(1);
                                currentXY = AllData(struct_entry).xy(1,:);
                                current_compartment = AllData(struct_entry).compartment(1);
                                fraction_inside = sum(AllData(struct_entry).compartment) / length(AllData(struct_entry).compartment);
                                for jump = 1:(TrajLength - 1) %For each jump from localization n to n + 1, get the distance and the type of jump (Inside, Outside, Entering, Exiting)
                                    nextLoc = AllData(struct_entry).Frame(jump + 1);
                                    nextXY = AllData(struct_entry).xy(jump + 1,:);
                                    next_compartment = AllData(struct_entry).compartment(jump + 1);
                                    if nextLoc == currentLoc + 1
                                        jumpLength = pdist([currentXY;nextXY]);
                                        if jumpLength > jump_length_minimum_dist
                                            if and(current_compartment,next_compartment) %both localizations inside
                                                Inside_jumps = vertcat(Inside_jumps, jumpLength);
                                            elseif and(~current_compartment,next_compartment) %going from outside to inside
                                                Entering_jumps = vertcat(Entering_jumps, jumpLength);
                                            elseif and(current_compartment,~next_compartment) %going from inside to outside
                                                Leaving_jumps = vertcat(Leaving_jumps, jumpLength);
                                            elseif and(~current_compartment,~next_compartment)
                                                Outside_jumps = vertcat(Outside_jumps, jumpLength);
                                            end
                                        else
                                        end
                                    end
                                    currentLoc = AllData(struct_entry).Frame(jump + 1);
                                    currentXY = AllData(struct_entry).xy(jump + 1,:);
                                    current_compartment = AllData(struct_entry).compartment(jump + 1);
                                end
                            end
                        end
                        if ~isempty(Inside_jumps)
                            Compartment_jump_stats_bootstrap(1,1:4,bootstrap_iteration) = [length(Inside_jumps), mean(Inside_jumps), median(Inside_jumps), std(Inside_jumps,1,1)];
                            insideHist(:,bootstrap_iteration) = histcounts(Inside_jumps,[0:0.005:1],'Normalization','CDF');
                        else
                            Compartment_jump_stats_bootstrap(1,1:4,bootstrap_iteration) = NaN;
                        end
                        
                        if ~isempty(Outside_jumps)
                            Compartment_jump_stats_bootstrap(2,1:4,bootstrap_iteration) = [length(Outside_jumps), mean(Outside_jumps), median(Outside_jumps), std(Outside_jumps,1,1)];
                            outsideHist(:,bootstrap_iteration) = histcounts(Outside_jumps,[0:0.005:1],'Normalization','CDF');
                        else
                            Compartment_jump_stats_bootstrap(2,1:4,bootstrap_iteration) = NaN;
                        end
                        
                        if ~isempty(Entering_jumps)
                            Compartment_jump_stats_bootstrap(3,1:4,bootstrap_iteration) = [length(Entering_jumps), mean(Entering_jumps), median(Entering_jumps), std(Entering_jumps,1,1)];
                            EnteringHist(:,bootstrap_iteration) = histcounts(Entering_jumps,[0:0.005:1],'Normalization','CDF');
                        else
                            Compartment_jump_stats_bootstrap(3,1:4,bootstrap_iteration) = NaN;
                        end
                        
                        if ~isempty(Leaving_jumps)
                            Compartment_jump_stats_bootstrap(4,1:4,bootstrap_iteration) = [length(Leaving_jumps), mean(Leaving_jumps), median(Leaving_jumps), std(Leaving_jumps,1,1)];
                            LeavingHist(:,bootstrap_iteration) = histcounts(Leaving_jumps,[0:0.005:1],'Normalization','CDF');
                            if ~isempty(Entering_jumps)
                                EnterExit_ratio(bootstrap_iteration) = Compartment_jump_stats_bootstrap(3,1,bootstrap_iteration) / Compartment_jump_stats_bootstrap(4,1,bootstrap_iteration);
                            else
                                EnterExit_ratio(bootstrap_iteration) = NaN;
                            end
                        else
                            Compartment_jump_stats_bootstrap(4,1:4,bootstrap_iteration) = NaN;
                        end
                        
  
                        
                    end
                    toc;
                    
                    if ModelFit > 0
                        disp('performing model fitting of displacement histograms...'); tic;
                        [model_params, residuals, list_of_model_parameters] = ModelFitting_main(JumpProb, JumpProbCDF, NumberOfStates, FitLocErrorRange, FitIterations, D_Free_2State, D_Bound_2State, D_Free1_3State, D_Free2_3State, D_Bound_3State);
                        [model_PDF, model_CDF] = GenerateModelFitforPlot(model_params, JumpProb, JumpProbCDF, NumberOfStates);
                        toc;
                        Fraction_bound = model_params(3); D_bound = model_params(2); D_free = model_params(1);
                        
                        
                        if FitLocError == 1
                            Localization_Error = model_params(4);
                            if compartment == 1
                                Tracking_stats_bootstrap(bootstrap_iteration,:) = [Fraction_bound, D_bound, D_free, Localization_Error];
                            elseif compartment == 2
                                Tracking_stats_inside_bootstrap = [Fraction_bound, D_bound, D_free, Localization_Error];
                            elseif compartment == 3
                                Tracking_stats_outside_bootstrap = [Fraction_bound, D_bound, D_free, Localization_Error];
                                Tracking_stats_bootstrap(bootstrap_iteration,:) = horzcat(Tracking_stats_outside_bootstrap,Tracking_stats_inside_bootstrap);
                            end
                        else
                            if compartment == 1
                                Tracking_stats_bootstrap(bootstrap_iteration,:) = [Fraction_bound, D_bound, D_free];
                            elseif compartment == 2
                                Tracking_stats_inside_bootstrap = [Fraction_bound, D_bound, D_free];
                                files_used{bootstrap_iteration,1} = Tracking_stats_inside_bootstrap(1,1);
                            elseif compartment == 3
                                Tracking_stats_outside_bootstrap = [Fraction_bound, D_bound, D_free];
                                Tracking_stats_bootstrap(bootstrap_iteration,:) = horzcat(Tracking_stats_outside_bootstrap,Tracking_stats_inside_bootstrap);
                            end
                        end
                    end
                else
                    disp('Too few trajectories to perform model fitting. Omitting this iteration...')
                    if FitLocError == 1
                        Localization_Error = model_params(4);
                        if compartment == 1
                            Tracking_stats_bootstrap(bootstrap_iteration,:) = NaN(1,4);
                        elseif compartment == 2
                            Tracking_stats_inside_bootstrap = NaN(1,4);
                        elseif compartment == 3
                            Tracking_stats_outside_bootstrap = NaN(1,4);
                            Tracking_stats_bootstrap(bootstrap_iteration,:) = horzcat(Tracking_stats_outside_bootstrap,Tracking_stats_inside_bootstrap);
                        end
                    else
                        if compartment == 1
                            Tracking_stats_bootstrap(bootstrap_iteration,:) = NaN(1,3);
                        elseif compartment == 2
                            Tracking_stats_inside_bootstrap = NaN(1,3);
                        elseif compartment == 3
                            Tracking_stats_outside_bootstrap = NaN(1,3);
                            Tracking_stats_bootstrap(bootstrap_iteration,:) = horzcat(Tracking_stats_outside_bootstrap,Tracking_stats_inside_bootstrap);
                        end
                    end
                    
                end
            end
            for line = 1:length(workspaces)
                files_used{bootstrap_iteration,line+1} = workspaces{line}(length(input_path)+1:length(input_path)+30);
            end
            disp(['Iteration ', num2str(bootstrap_iteration),' finished in ', num2str(toc(loopStart)), ' seconds']);
        end
        
        Tracking_stats_bootstrap_mean = mean(Tracking_stats_bootstrap,1);
        Tracking_stats_bootstrap_stdev = std(Tracking_stats_bootstrap,1,1);
        Compartment_Jumps_Prism = [mean(insideHist,2,'omitnan'),std(insideHist,1,2,'omitnan'),...
                                   mean(outsideHist,2,'omitnan'),std(outsideHist,1,2,'omitnan'),...
                                   mean(EnteringHist,2,'omitnan'),std(EnteringHist,1,2,'omitnan'),...
                                   mean(LeavingHist,2,'omitnan'),std(LeavingHist,1,2,'omitnan')];
    else
        Tracking_stats_bootstrap_mean = [];
        Tracking_stats_bootstrap_stdev = [];
    end

    %% Analyze pooled data
    workspaces = Workspaces;
    Include = ones(1,length(Workspaces));
    [s,mess,messid] = mkdir(output_path,SampleName);
    for compartment = Compartment_to_test_start:Compartment_to_test_end
        %%% LOAD IN THE DATA AND CONVERT TO HISTOGRAMS %%%
        tic; disp('Loading in the data for all files...');
        AllData = []; %for storing data
        TotalFrames = 0; %for counting how many frames
        TotalLocs = 0; %for counting the total number of localizations
        %INPUT IS NOT A STRUCTURE:
        %LOAD EACH CELL FROM A SINGLE REPLICATE
        %Load data
        for i=1:length(workspaces)
            %Only load a dataset if it is to be included
            if Include(i) == 1
                load(workspaces{i});
                if compartment == 1
                    trackedPar_to_analyze = trackedPar_nuclear;
                    compartment_string = 'All_trajectories';
                elseif compartment == 2
                    trackedPar_to_analyze = trackedPar_insideRC;
                    compartment_string = 'Inside_RC';
                elseif compartment == 3
                    trackedPar_to_analyze = trackedPar_outside;
                    compartment_string = 'Outside_RC';
                end
                
                if ~isempty(fieldnames(trackedPar_to_analyze))
                    AllData = [AllData trackedPar_to_analyze];
                    for n=1:length(trackedPar_to_analyze)
                        TotalLocs = TotalLocs + length(trackedPar_to_analyze(1,n).Frame);
                    end
                    %Find total frames using a slight ad-hoc way: find the last frame with
                    %a localization and round it. This is not an elegant solution, but it
                    %works for your particle density:
                    TempLastFrame = max(trackedPar_to_analyze(1,end).Frame);
                    TotalFrames = TotalFrames + 100*round(TempLastFrame/100);
                end
            else
                continue
            end
        end
        
        toc;
        %Compile histograms for each jump lengths
        tic; disp('Compiling histogram of jump lengths...');
        Min3Traj = 0; %for counting number of min3 trajectories;
        TotalJumps = 0; %for counting the total number of jumps
        TrajLengthHist = zeros(1,length(AllData));
        %Calculate a histogram of translocation lengths vs. frames
        TransFrames = TimePoints+GapsAllowed*(TimePoints-1); TransLengths = struct;
        for i=1:TransFrames
            TransLengths(1,i).Step = []; %each iteration is a different number of timepoints
        end
        JumpsPerdT = zeros(TransFrames,1); % for counting how many jumps per dT
        if UseAllTraj == 1 %Use all of the trajectory
            for i=1:length(AllData)
                % save length of the trajectory
                TrajLengthHist(1,i) = max(AllData(i).Frame) - min(AllData(i).Frame) + 1;
                CurrTrajLength = size(AllData(i).xy,1);
                %save lengths
                if CurrTrajLength >= 3
                    Min3Traj = Min3Traj + 1;
                end
                %Now loop through the trajectory. Keep in mind that there are missing
                %timepoints in the trajectory, so some gaps may be for multiple
                %timepoints.
                %Figure out what the max jump to consider is:
                HowManyFrames = min(TimePoints-1, CurrTrajLength);
                if CurrTrajLength > 1
                    TotalJumps = TotalJumps + CurrTrajLength - 1; %for counting all the jumps
                    for n=1:HowManyFrames
                        for k=1:CurrTrajLength-n
                            %Find the current XY coordinate and frames between
                            %timepoints
                            CurrXY_points = vertcat(AllData(i).xy(k,:), AllData(i).xy(k+n,:));
                            CurrFrameJump = AllData(i).Frame(k+n) - AllData(i).Frame(k);
                            %Calculate the distance between the pair of points
                            TransLengths(1,CurrFrameJump).Step = horzcat(TransLengths(1,CurrFrameJump).Step, pdist(CurrXY_points));
                            %Count the number of jumps for a given delta T
                            JumpsPerdT(CurrFrameJump) =  JumpsPerdT(CurrFrameJump) + 1;
                        end
                    end
                end
            end
        elseif UseAllTraj == 0 %Use only the first JumpsToConsider timepoints
            for i=1:length(AllData)
                % save length of the trajectory
                TrajLengthHist(1,i) = max(AllData(i).Frame) - min(AllData(i).Frame) + 1;
                CurrTrajLength = size(AllData(i).xy,1);
                if CurrTrajLength >= 3
                    Min3Traj = Min3Traj + 1;
                end
                %Loop through the trajectory. If it is a short trajectory, you need to
                %make sure that you do not overshoot. So first figure out how many
                %jumps you can consider.
                %Figure out what the max jump to consider is:
                HowManyFrames = min([TimePoints-1, CurrTrajLength]);
                if CurrTrajLength > 1
                    TotalJumps = TotalJumps + CurrTrajLength - 1; %for counting all the jumps
                    for n=1:HowManyFrames
                        FrameToStop = min([CurrTrajLength, n+JumpsToConsider]);
                        for k=1:FrameToStop-n
                            %Find the current XY coordinate and frames between
                            %timepoints
                            CurrXY_points = vertcat(AllData(i).xy(k,:), AllData(i).xy(k+n,:));
                            CurrFrameJump = AllData(i).Frame(k+n) - AllData(i).Frame(k);
                            %Calculate the distance between the pair of points
                            TransLengths(1,CurrFrameJump).Step = horzcat(TransLengths(1,CurrFrameJump).Step, pdist(CurrXY_points));
                            %Count the number of jumps for a given delta T
                            JumpsPerdT(CurrFrameJump) =  JumpsPerdT(CurrFrameJump) + 1;
                        end
                    end
                end
            end
        end
        %CALCULATE THE PDF HISTOGRAMS
        JumpProb = zeros(TimePoints-1, length(HistVecJumps));
        JumpProbFine = zeros(TimePoints-1, length(HistVecJumpsCDF));
        for i=1:size(JumpProb,1)
            JumpProb(i,:) = histc(TransLengths(1,i).Step, HistVecJumps)/length(TransLengths(1,i).Step);
            JumpProbFine(i,:) = histc(TransLengths(1,i).Step, HistVecJumpsCDF)/length(TransLengths(1,i).Step);
        end
        
        %CALCULATE THE CDF HISTOGRAMS:
        JumpProbCDF = zeros(TimePoints-1, length(HistVecJumpsCDF));
        for i=1:size(JumpProbCDF,1)
            for j=2:size(JumpProbCDF,2)
                JumpProbCDF(i,j) = sum(JumpProbFine(i,1:j));
            end
        end
        toc;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%Identify compartment switching %%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if compartment == 2
            Inside_jumps = [];
            Outside_jumps = [];
            Entering_jumps = [];
            Leaving_jumps = [];
            for struct_entry = 1:length(AllData) %Grab each trajectory from the concatenated "AllData" trajectories
                TrajLength = size(AllData(struct_entry).Frame,1);
                if TrajLength > 1 %Check if it has more than one localization
                    currentLoc = AllData(struct_entry).Frame(1);
                    currentXY = AllData(struct_entry).xy(1,:);
                    current_compartment = AllData(struct_entry).compartment(1);
                    fraction_inside = sum(AllData(struct_entry).compartment) / length(AllData(struct_entry).compartment);
                    for jump = 1:(TrajLength - 1) %For each jump from localization n to n + 1, get the distance and the type of jump (Inside, Outside, Entering, Exiting)
                        nextLoc = AllData(struct_entry).Frame(jump + 1);
                        nextXY = AllData(struct_entry).xy(jump + 1,:);
                        next_compartment = AllData(struct_entry).compartment(jump + 1);
                        if nextLoc == currentLoc + 1
                            jumpLength = pdist([currentXY;nextXY]);
                            if jumpLength > jump_length_minimum_dist
                                if and(current_compartment,next_compartment) %both localizations inside
                                    Inside_jumps = vertcat(Inside_jumps, jumpLength);
                                elseif and(~current_compartment,next_compartment) %going from outside to inside
                                    Entering_jumps = vertcat(Entering_jumps, jumpLength);
                                elseif and(current_compartment,~next_compartment) %going from inside to outside
                                    Leaving_jumps = vertcat(Leaving_jumps, jumpLength);
                                elseif and(~current_compartment,~next_compartment)
                                    Outside_jumps = vertcat(Outside_jumps, jumpLength);
                                end
                            else
                            end
                        end
                        currentLoc = AllData(struct_entry).Frame(jump + 1);
                        currentXY = AllData(struct_entry).xy(jump + 1,:);
                        current_compartment = AllData(struct_entry).compartment(jump + 1);
                    end
                end
            end
            Compartment_stats = struct;
            Compartment_stats.Entering = Entering_jumps;
            Compartment_stats.Exiting = Leaving_jumps;
            Compartment_stats.all_state_chance = vertcat(Entering_jumps,Leaving_jumps);
            save([output_path, SampleName,'/',SampleName, '_compartment_switching.mat'],'Compartment_stats');
        end
        
        
        if ModelFit > 0
            disp('performing model fitting of displacement histograms...'); tic;
            [model_params, residuals, list_of_model_parameters] = ModelFitting_main(JumpProb, JumpProbCDF, NumberOfStates, FitLocErrorRange, FitIterations, D_Free_2State, D_Bound_2State, D_Free1_3State, D_Free2_3State, D_Bound_3State);
            toc;
            disp('proceeding to plotting the output of the model fitting...');
            [model_PDF, model_CDF] = GenerateModelFitforPlot(model_params, JumpProb, JumpProbCDF, NumberOfStates);
            % Generate a plot title with all the relevant info
            toc;
            disp('============================================================')
            Fraction_bound = model_params(3);
            D_bound = model_params(2);
            D_free = model_params(1);
            
            if FitLocError == 1
                Localization_Error = model_params(4);
                if compartment == 1
                    Tracking_stats_all = [Fraction_bound, D_bound, D_free, Localization_Error];
                elseif compartment == 2
                    Tracking_stats_inside = [Fraction_bound, D_bound, D_free, Localization_Error];
                elseif compartment == 3
                    Tracking_stats_outside = [Fraction_bound, D_bound, D_free, Localization_Error];
                    Tracking_stats_all = horzcat(Tracking_stats_outside,Tracking_stats_inside);
                end
            else
                if compartment == 1
                    Tracking_stats_all = [Fraction_bound, D_bound, D_free];
                    Tracking_stats_all(2,:) = mean(Tracking_stats_bootstrap);
                    Tracking_stats_all(3,:) = std(Tracking_stats_bootstrap);
                    For_prism_jumps = [HistVecJumps',JumpProb(1,:)',model_PDF(1,:)'];
                elseif compartment == 2
                    Tracking_stats_inside = [Fraction_bound, D_bound, D_free];
                    For_prism_jumps = [HistVecJumps',JumpProb(1,:)',model_PDF(1,:)'];
                elseif compartment == 3
                    Tracking_stats_outside = [Fraction_bound, D_bound, D_free];
                    Tracking_stats_all(1,:) = horzcat(Tracking_stats_outside,Tracking_stats_inside);
                    Tracking_stats_all(2,:) = mean(Tracking_stats_bootstrap);
                    Tracking_stats_all(3,:) = std(Tracking_stats_bootstrap);
                    For_prism_jumps = [For_prism_jumps, HistVecJumps',JumpProb(1,:)',model_PDF(1,:)'];
                end
            end
            
        end
        
        for i = 1:length(AllData)
            traj_length(i) = length(AllData(i).compartment);
        end
        longest_traj = max(traj_length);
        traj_counts = histcounts(traj_length,[1:1:longest_traj+1],'Normalization','cumcount');
        traj_CDF = histcounts(traj_length,[1:1:longest_traj+1],'Normalization','cdf');
        traj_inverse_CDF = 1-traj_CDF;
        CDF_min_val = traj_inverse_CDF(longest_traj - 1);
        
        
        %% Perform Angle Analysis on pooled trajectories
        if AngleAnalysis == 1
            %In the AllData structured array you allow gaps in the trajectories
            %So need to re-slice each trajectory to get rid of gaps.
            %Make a cell array of trajectories
            CellTracks = cell(1);
            CellTrackViterbiClass = cell(1);
            input_struct = struct;
            input_struct(1).nBins = nBins;
            input_struct(1).rads = rads;
            input_struct(1).BoundState = BoundState;
            input_struct(1).FreeState = FreeState;
            input_struct(1).MaxJump = MaxJumpAngle;
            input_struct(1).minAngleNumber = minAngleNumber; % below this number, don't do calculations
            input_struct(1).minFree = minFree;
            input_struct(1).MovingThreshold = MovingThreshold;
            input_struct(1).MinMinJumpThres = MinMinJumpThres;
            input_struct(1).MinNumAngles = MinNumAngles;
            input_struct(1).MaxAsymAnglesFrac = MaxAsymAnglesFrac;
            input_struct(1).OffSet_thres = OffSet_thres;
            input_struct(1).JackKnife_fraction = JackKnife_fraction;
            input_struct(1).JackKnife_iterations = JackKnife_iterations;
            
            iter = 1;
            for i=1:size(AllData,2)
                if compartment == 2
                    if size(AllData(i).xy,1) >= MinTrajLength
                        %Now slice up trajectory to get rid of gaps. For
                        %trajectories annotated as "inside", remove jumps that
                        %include translocations outside of the RC.
                        Compartment_annotation = AllData(i).compartment;
                        Idx_out = [];
                        for k = 1:length(Compartment_annotation) -1
                            curr_compartment = Compartment_annotation(k);
                            next_compartment = Compartment_annotation(k+1);
                            if and(~curr_compartment,~next_compartment)
                                Idx_out = vertcat(Idx_out, k);
                            end
                        end
                        currFrame = AllData(i).Frame;
                        currTrack = AllData(i).xy;
                        currFrame_outside_removed = currFrame;
                        currFrame_outside_removed(Idx_out) = [];
                        
                        %check for gaps - expect a difference of 1 between frames if there
                        %are no gaps, so search for difference of 2:
                        Idx = find(diff(currFrame_outside_removed)>=2);
                        %If there were no gaps, just save:
                        if isempty(Idx)
                            CellTracks{iter} = currTrack;
                            iter = iter + 1;
                        else
                            %Modify Idx:
                            Idx_slice = [0 Idx' length(currFrame)];
                            %So there are gaps:
                            %Slice up trajectories if they satisfy MinTrajLength
                            for j=2:length(Idx_slice)
                                if size(currTrack(Idx_slice(j-1)+1:Idx_slice(j),:),1) >= MinTrajLength
                                    CellTracks{iter} = currTrack(Idx_slice(j-1)+1:Idx_slice(j),:);
                                    iter = iter + 1;
                                end
                            end
                        end
                    end
                else
                    if size(AllData(i).xy,1) >= MinTrajLength
                        %Now slice up trajectory to get rid of gaps
                        currFrame = AllData(i).Frame;
                        currTrack = AllData(i).xy;
                        %check for gaps - expect a difference of 1 between frames if there
                        %are no gaps, so search for difference of 2:
                        Idx = find(diff(currFrame)==2);
                        %If there were no gaps, just save:
                        if isempty(Idx)
                            CellTracks{iter} = currTrack;
                            iter = iter + 1;
                        else
                            %Modify Idx:
                            Idx_slice = [0 Idx length(currFrame)];
                            %So there are gaps:
                            %Slice up trajectories if they satisfy MinTrajLength
                            for j=2:length(Idx_slice)
                                if size(currTrack(Idx_slice(j-1)+1:Idx_slice(j),:),1) >= MinTrajLength
                                    CellTracks{iter} = currTrack(Idx_slice(j-1)+1:Idx_slice(j),:);
                                    iter = iter + 1;
                                end
                            end
                        end
                    end
                end
            end
            
            CuratedCellTracks = cell(1);
            NewCounter = 1;
            for i = 1:length(CellTracks)
                if length(CellTracks{i}) > MinTrajLength
                    CuratedCellTracks{NewCounter} = CellTracks{i};
                    NewCounter = NewCounter + 1;
                end
            end
            
%             CellTracks = CuratedCellTracks;
            
            save('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT/HMM_class_container/Traj_to_classify.mat', 'CellTracks');
            save('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT/HMM_class_container/Finished_classification.mat','CellTrackViterbiClass');% In case there was an error, so that old data is not ported over.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%% Run vbSPT on the data %%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            R=VB3_HMManalysis('vbSPT_RunInputFile.m');
            
            %Now distill out the key things:
            CellTrackViterbiClass = R.Wbest.est2.viterbi;
            
            BoundPopAllPointsHMM = 0;
            FreePopAllPointsHMM = 0;
            BoundPopFirstHMM = 0;
            FreePopFirstHMM = 0;
            for i = 1:length(CellTrackViterbiClass)
                if CellTrackViterbiClass{i}(1) == 1
                    BoundPopFirstHMM = BoundPopFirstHMM + 1;
                elseif CellTrackViterbiClass{i}(1) == 2
                    FreePopFirstHMM = FreePopFirstHMM + 1;
                end
                for j = length(CellTrackViterbiClass{i})
                    if CellTrackViterbiClass{i}(j) == 1
                        BoundPopAllPointsHMM = BoundPopAllPointsHMM + 1;
                    elseif CellTrackViterbiClass{i}(j) == 2
                        FreePopAllPointsHMM = FreePopAllPointsHMM + 1;
                    end
                end
            end
            
            HMM_Class_Totals = [BoundPopFirstHMM, FreePopFirstHMM, BoundPopAllPointsHMM, FreePopAllPointsHMM];
            dlmwrite([output_path, SampleName,'/',SampleName,'_',compartment_string,'_HMM_ClassTotals.txt'],HMM_Class_Totals,'\t');
            
            %Display the output:
            disp('====================================================');
            VB3_getResult('vbSPT_RunInputFile.m');
            toc;
            save('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT/HMM_class_container/Finished_classification.mat', 'CellTracks', 'CellTrackViterbiClass');
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%% SEARCH AND SLICE THE DATA %%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            disp('Slice up the data and calculate the angles...'); tic;
            input_struct(1).CellTracks = CellTracks;
            input_struct(1).CellTrackViterbiClass = CellTrackViterbiClass;
            
            output_struct = angleFWHM_Amp_HMM_analyzer_v3(input_struct );
            toc;
            
            % key output from the calculations
            normPDF = output_struct(1).normPDF;
            mean_Amp = output_struct(1).mean_Amp;
            mean_FWHM = output_struct(1).mean_FWHM;
            std_Amp = std(output_struct(1).jack_Amp);
            std_FWHM = std(output_struct(1).jack_FWHM);
            Alla = output_struct(1).Alla;
            AllVals = output_struct(1).AllVals;
            AC = output_struct(1).AC;
            std_AC = std(output_struct(1).jack_AC);
            
            
            % x-vector for displaying the angles
            x_index_vector = [1;];
            for i=2:length(normPDF)
                x_index_vector = [x_index_vector, i, i];
            end
            x_index_vector = [x_index_vector, length(normPDF)+1];
            y_index_vector = [];
            for i=1:length(normPDF)
                y_index_vector = [y_index_vector, i, i];
            end
            x_theta = 0:1/length(normPDF):1;
            
            %%%%%%%%%%%%%%%% ANALYSIS AT MULTIPLE LENGTH SCALES %%%%%%%%%%%%%%%%%%%
            % at multiple length scales
            AmpAtClosestMean = output_struct(1).AmpAtClosestMean;
            FWHMAtClosestMean = output_struct(1).FWHMAtClosestMean;
            ACAtClosestMean = output_struct(1).ACAtClosestMean;
            AmpAtClosestMin = output_struct(1).AmpAtClosestMin;
            FWHMAtClosestMin = output_struct(1).FWHMAtClosestMin;
            ACAtClosestMin = output_struct(1).ACAtClosestMin;
            jack_AmpAtClosestMean = output_struct(1).jack_AmpAtClosestMean;
            jack_FWHMAtClosestMean = output_struct(1).jack_FWHMAtClosestMean;
            jack_ACAtClosestMean = output_struct(1).jack_ACAtClosestMean;
            jack_AmpAtClosestMin = output_struct(1).jack_AmpAtClosestMin;
            jack_FWHMAtClosestMin = output_struct(1).jack_FWHMAtClosestMin;
            jack_ACAtClosestMin = output_struct(1).jack_ACAtClosestMin;
        end
        
        %% MSD ANALYSIS
        if MSD_Analysis == 1
            tic;
            disp('Slicing up trajectories for MSD analysis...')
            %In the AllData structured array you allow gaps in the trajectories
            %So need to re-slice each trajectory to get rid of gaps.
            %Make a cell array of trajectories
            CellTracks = cell(1);
            CellTrackViterbiClass = cell(1);
            input_struct = struct;
            input_struct(1).nBins = nBins;
            input_struct(1).rads = rads;
            input_struct(1).BoundState = BoundState;
            input_struct(1).FreeState = FreeState;
            input_struct(1).MaxJump = MaxJumpAngle;
            input_struct(1).minAngleNumber = minAngleNumber; % below this number, don't do calculations
            input_struct(1).minFree = minFree;
            input_struct(1).MovingThreshold = MovingThreshold;
            input_struct(1).MinMinJumpThres = MinMinJumpThres;
            input_struct(1).MinNumAngles = MinNumAngles;
            input_struct(1).MaxAsymAnglesFrac = MaxAsymAnglesFrac;
            input_struct(1).OffSet_thres = OffSet_thres;
            input_struct(1).JackKnife_fraction = JackKnife_fraction;
            input_struct(1).JackKnife_iterations = JackKnife_iterations;
            
            iter = 1;
            CellTracks = cell(1);
            iter = 1;
            for i=1:size(AllData,2)
                if compartment == 2
                    Compartment_annotation = AllData(i).compartment;
                    if size(AllData(i).xy,1) >= MinTrajLength
                        Compartment_annotation = AllData(i).compartment;
                        if sum(Compartment_annotation) / length(Compartment_annotation) >= 0.5
                            %Now slice up trajectory to get rid of gaps
                            currFrame = AllData(i).Frame;
                            currTrack = AllData(i).xy;
                            %check for gaps - expect a difference of 1 between frames if there
                            %are no gaps, so search for difference of 2:
                            Idx = find(diff(currFrame)==2);
                            %If there were no gaps, just save:
                            if isempty(Idx)
                                CellTracks{iter} = currTrack;
                                iter = iter + 1;
                            else
                                %Modify Idx:
                                Idx_slice = [0 Idx length(currFrame)];
                                %So there are gaps:
                                %Slice up trajectories if they satisfy MinTrajLength
                                for j=2:length(Idx_slice)
                                    if size(currTrack(Idx_slice(j-1)+1:Idx_slice(j),:),1) >= MinTrajLength
                                        CellTracks{iter} = currTrack(Idx_slice(j-1)+1:Idx_slice(j),:);
                                        iter = iter + 1;
                                    end
                                end
                            end
                        end
                    end
                else
                    if size(AllData(i).xy,1) >= MinTrajLength
                        %Now slice up trajectory to get rid of gaps
                        currFrame = AllData(i).Frame;
                        currTrack = AllData(i).xy;
                        %check for gaps - expect a difference of 1 between frames if there
                        %are no gaps, so search for difference of 2:
                        Idx = find(diff(currFrame)==2);
                        %If there were no gaps, just save:
                        if isempty(Idx)
                            CellTracks{iter} = currTrack;
                            iter = iter + 1;
                        else
                            %Modify Idx:
                            Idx_slice = [0 Idx length(currFrame)];
                            %So there are gaps:
                            %Slice up trajectories if they satisfy MinTrajLength
                            for j=2:length(Idx_slice)
                                if size(currTrack(Idx_slice(j-1)+1:Idx_slice(j),:),1) >= MinTrajLength
                                    CellTracks{iter} = currTrack(Idx_slice(j-1)+1:Idx_slice(j),:);
                                    iter = iter + 1;
                                end
                            end
                        end
                    end
                end
            end
            save('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT/HMM_class_container/Traj_to_classify.mat', 'CellTracks');
            save('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT/HMM_class_container/Finished_classification.mat','CellTrackViterbiClass');% In case there was an error, so that old data is not ported over.
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%% Run vbSPT on the data %%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            R=VB3_HMManalysis('vbSPT_RunInputFile.m');
            
            %Now distill out the key things:
            CellTrackViterbiClass = R.Wbest.est2.viterbi;
            
            %Display the output:
            disp('====================================================');
            VB3_getResult('vbSPT_RunInputFile.m');
            toc;
            save('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT/HMM_class_container/Finished_classification.mat', 'CellTracks', 'CellTrackViterbiClass');
            
            disp(['Calculating the MSD for ',SampleName,'...']);
            MSD_time = zeros(1,MSD_timepoints);
            for TimeIter = 1:length(MSD_time)
                MSD_time(1,TimeIter) = TimeIter * dT;
            end
            
            %%% CALCULATE THE MSD FOR ALL DATA USING THE FUNCTION MSD_HMM_ANALYZER %%%
            MSD = MSD_HMM_analyzer(CellTracks, CellTrackViterbiClass, MSD_time, FreeState);
            toc;
            
            %%%%%%%%%%%%%% JACK-KNIFE RE-SAMPLING TO ESTIMATE ERROR %%%%%%%%%%%%%%%
            
            tic;
            disp(['Performing bootstrapping...']);
            MSD_JackKnife = zeros(JackKnife_iterations, MSD_timepoints);
            Num_samples = floor(length(CellTracks) * JackKnife_fraction);
            MSD_bootstrap_fits = NaN(JackKnife_iterations,3);
            fit_weights = ones(MSD_timepoints,1);
            for i = 2:MSD_timepoints
                %fit_weights(i,1) = (traj_counts(longest_traj) - traj_counts(i-1))/traj_counts(longest_traj);
                fit_weights(i,1) = (i-1)^(-1);
            end
            % sub-sample the dataset
            for JackIter = 1:JackKnife_iterations
                percent_finished = (JackIter/JackKnife_iterations)*100;
                [subsampled_CellTracks, indices] = datasample(CellTracks, Num_samples, 'Replace', false);
                subsampled_CellTrackViterbiClass = CellTrackViterbiClass(indices);
                % now calculate the subsample MSD:
                MSD_JackKnife(JackIter,:) = MSD_HMM_analyzer(subsampled_CellTracks, subsampled_CellTrackViterbiClass, MSD_time, FreeState);
                if mod(percent_finished,5) == 0
                    disp([num2str(percent_finished),' percent processed...']);
                end
            end
            toc;
            
            %%%%%%%%% FITTING AND PLOTTING %%%%%%%%%
            disp('fit a Power Law to the MSD and perform plotting'); tic;
            % Fit a Power Law to the MSD:
            if Constrain_MSD_Dfree == 1
                if Number_bootstrap_iterations >= 2
                    if compartment == 2
                        d_free_fit = Tracking_stats_all(1,6);
                        d_free_fit_upper = Tracking_stats_all(1,6) + 1.96*Tracking_stats_all(3,6);
                        d_free_fit_lower = Tracking_stats_all(1,6) - 1.96*Tracking_stats_all(3,6);
                    else
                        d_free_fit = Tracking_stats_all(1,3);
                        d_free_fit_upper = Tracking_stats_all(1,3) + 1.96*Tracking_stats_all(3,3);
                        d_free_fit_lower = Tracking_stats_all(1,3) - 1.96*Tracking_stats_all(3,3);
                    end
                    f = fittype('4*D*x^a + 4*b^2');
                    [MSD_fit, MSD_param] = fit(MSD_time', MSD', f, 'Weights',fit_weights, 'Lower', [0 0 0.005], 'Upper', [4 2 LocError], 'StartPoint', [d_free_fit 1 0.035]);
                    MSD_fit_params = [MSD_fit.D, MSD_fit.a, MSD_fit.b];
                    
                else
                    d_free_fit = D_free;
                    f = fittype('4*D*x^a + 4*b^2');
                    [MSD_fit, MSD_param] = fit(MSD_time', MSD', f, 'Weights',fit_weights, 'Lower', [0 0.005], 'Upper', [2 LocError], 'StartPoint', [1 0.035]);
                    MSD_fit_params = [MSD_fit.D, MSD_fit.a, MSD_fit.b];
                end
            else
                d_free_fit = D_free;
                f = fittype('4*D*x^a + 4*b^2');
                [MSD_fit, MSD_param] = fit(MSD_time', MSD', f, 'Weights',fit_weights, 'Lower', [0 0 0.005], 'Upper', [10 2 LocError], 'StartPoint', [d_free_fit 1 0.035]);
                MSD_fit_params = [MSD_fit.D, MSD_fit.a, MSD_fit.b];
            end
            MSD_fit_CI =  confint(MSD_fit);
            %     MSD_fit_bootstrap_mean = mean(MSD_bootstrap_fits,1,'Omitnan');
            %     MSD_fit_bootstrap_std = std(MSD_bootstrap_fits,1,1,'Omitnan');
            %     MSD_fit_CI = [MSD_fit_bootstrap_mean + MSD_fit_bootstrap_std.*1.96;MSD_fit_bootstrap_mean - MSD_fit_bootstrap_std.*1.96];
            
        end
        %% PLOT EVERYTHING
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%% PLOT TRACKING STATS %%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        SPT_text(1) = {['Tracking statistics for trajectories ',compartment_string]};
        SPT_text(2) = {['Mean trajectory length = ',num2str(mean(traj_length)),' frames']};
        SPT_text(3) = {['Median trajectory length = ',num2str(median(traj_length)),' frames']};
        SPT_text(4) = {['Number of trajectories longer than 3 = ',num2str(traj_counts(longest_traj) - traj_counts(3)),' trajectories']};
        SPT_text(5) = {['Number of trajectories longer than 10 = ',num2str(traj_counts(longest_traj) - traj_counts(10)),' trajectories']};
        SPT_text(6) = {['Longest trajectory = ',num2str(longest_traj),' frames']};
        
        figure2 = figure('position',[300 300 plot_width plot_height]);
        subplot(1,2,1)
        traj_length_PDF = histogram(traj_length,[0:1:longest_traj + 1],'Normalization','pdf');
        traj_length_PDF.Normalization = 'pdf';
        traj_length_PDF.FaceColor = [52/255,48/255,134/255];
        traj_length_PDF.FaceAlpha = 0.5;
        xlabel('Trajectory length (frames)', 'FontSize',12, 'FontName', 'Optima');
        ylabel('Probability', 'FontSize',12, 'FontName', 'Optima');
        text(4, 0.5, SPT_text,'HorizontalAlignment','Left', 'FontSize',8, 'FontName', 'Helvetica');
        ax = gca;
        ax.XLim = [1 longest_traj];
        
        subplot(1,2,2)
        plot(1:longest_traj,traj_inverse_CDF,'Color',[52/255,48/255,134/255],'LineWidth',2)
        ax = gca;
        ax.YScale = 'log';
        ax.XLim = [1 longest_traj]; ax.YLim = [CDF_min_val 1];
        xlabel('Trajectory length (frames)', 'FontSize',12, 'FontName', 'Optima');
        ylabel('1 - CDF', 'FontSize',12, 'FontName', 'Optima');
        
        if SavePDF == 1
            set(figure2,'Units','Inches');
            pos = get(figure2,'Position');
            set(figure2,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
            print(figure2,[output_path, SampleName,'/',SampleName, '_tracking_stats_',compartment_string,'.pdf'],'-dpdf','-r0');
            print(figure2,[output_path, SampleName,'/',SampleName, '_tracking_stats_',compartment_string,'.eps'],'-depsc');
            close
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%% PLOT ALL OF THE RESULTS FROM SPOT-ON %%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Plot residuals for the relevant fit for fraction bound analysis:
        if ModelFit > 0
            figure3 = figure('position',[300 300 plot_width plot_height]); %[x y width height]
            % find max_y for plot
            max_y = max([0.1 max(max(abs(residuals)))]); min_y = -max_y;
            for i=1:min([9 size(residuals,1)])
                colour_element = colour(round(i/size(residuals,1)*size(colour,1)),:);
                subplot(3,6,(2*i-1):(2*i));
                hold on;
                if ModelFit == 1
                    plot(HistVecJumps, residuals(i,:), '-', 'Color', colour_element, 'LineWidth', 2);
                    max_x = MaxJumpPlotPDF;
                elseif ModelFit == 2
                    plot(HistVecJumpsCDF, residuals(i,:), '-', 'Color', colour_element, 'LineWidth', 2);
                    max_x = MaxJumpPlotCDF;
                end
                plot([0 max_x], [0 0], 'k--', 'LineWidth', 2);
                
                axis([0 max_x min_y max_y]);
                if ModelFit == 1
                    title({SampleName; ['PDF residuals for \Delta\tau: ', num2str(TimeGap*i), ' ms']}, 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k');
                elseif ModelFit == 2
                    title({SampleName; ['CDF residuals for \Delta\tau: ', num2str(TimeGap*i), ' ms']}, 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k');
                end
                ylabel('residuals', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
                xlabel('displacements (\mu m)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
                hold off;
            end
            
            if SavePDF == 1
                set(figure3,'Units','Inches');
                pos = get(figure3,'Position');
                set(figure3,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
                print(figure3,[output_path, SampleName,'/',SampleName, '_CDF_residuals_',compartment_string,'.pdf'],'-dpdf','-r0');
                print(figure3,[output_path, SampleName,'/',SampleName, '_CDF_residuals_',compartment_string,'.eps'],'-depsc');
                close
            end
            
            %PLOT CDFs of DISPLACEMENTS AND OF FIT
            figure4 = figure('position',[100 100 plot_width plot_height]); %[x y width height]
            for i=1:min([12 size(JumpProbCDF,1)])
                colour_element = colour(round(i/size(JumpProbCDF,1)*size(colour,1)),:);
                subplot(3,4,i);
                hold on;
                plot(HistVecJumpsCDF, JumpProbCDF(i,:), '-', 'LineWidth', 2, 'Color', colour_element);
                plot(HistVecJumpsCDF, model_CDF(i,:), 'k-', 'LineWidth', 1);
                
                axis([0 MaxJumpPlotCDF 0 1.05]);
                title({SampleName; ['CDF for \Delta\tau: ', num2str(TimeGap*i), ' ms']}, 'FontSize',8, 'FontName', 'Helvetica', 'Color', 'k');
                ylabel('displacement CDF', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
                xlabel('displacements (\mu m)', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
                legend('raw data', 'Model fit', 'Location', 'SouthEast');
                legend boxoff
                hold off;
            end
            if SavePDF == 1
                set(figure4,'Units','Inches');
                pos = get(figure4,'Position');
                set(figure4,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
                print(figure4,[output_path, SampleName,'/',SampleName, '_CDF_fit_',compartment_string,'.pdf'],'-dpdf','-r0')
                print(figure4,[output_path, SampleName,'/',SampleName, '_CDF_fit_',compartment_string,'.pdf'],'-depsc')
                close
            end
            
            %PLOT THE HISTOGRAM OF TRANSLOCATIONS
            figure5 = figure('position',[200 200 300 400]); %[x y width height]
            histogram_spacer = 0.055;
            hold on;
            for i=size(JumpProb,1):-1:1
                new_level = (i-1)*histogram_spacer;
                colour_element = colour(round(i/size(JumpProb,1)*size(colour,1)),:);
                plot(HistVecJumps, new_level*ones(1,length(HistVecJumps)), 'k-', 'LineWidth', 1);
                for j=2:size(JumpProb,2)
                    x1 = HistVecJumps(1,j-1); x2 = HistVecJumps(1,j);
                    y1 = new_level; y2 = JumpProb(i,j-1)+new_level;
                    patch([x1 x1 x2 x2], [y1 y2 y2 y1],colour_element);
                end
                plot(HistVecJumps, model_PDF(i,:)+new_level, 'k-', 'LineWidth', 2);
                text(0.6*MaxJumpPlotPDF, new_level+0.5*histogram_spacer, ['\Delta\tau: ', num2str(TimeGap*i), ' ms'], 'HorizontalAlignment','left', 'FontSize',9, 'FontName', 'Helvetica');
            end
            axis([0 MaxJumpPlotPDF 0 1.05*(max(JumpProb(end,:))+(size(JumpProb,1)-1)*histogram_spacer)]);
            if compartment == 1
                title('Translocations of all trajectories', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
            elseif compartment == 2
                title('Translocations inside Replication Compartments', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
            elseif compartment == 3
                title('Translocations outside of Replication Compartments', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
            end
            set(gca,'YColor','w')
            ylabel('Probability', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
            xlabel('jump length \mu m', 'FontSize',9, 'FontName', 'Helvetica', 'Color', 'k');
            set(gca,'YTickLabel',''); set(gca, 'YTick', []);
            hold off;
            if SavePDF == 1
                set(figure5,'Units','Inches');
                pos = get(figure5,'Position');
                set(figure5,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
                print(figure5,[output_path, SampleName,'/',SampleName, '_PDF_fit_',compartment_string,'.pdf'],'-dpdf','-r0')
                print(figure5,[output_path, SampleName,'/',SampleName, '_PDF_fit_',compartment_string,'.eps'],'-depsc')
                close
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%% PLOT ALL OF THE RESULTS FROM vbSPT %%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if AngleAnalysis == 1;
                figure6 = figure('position',[100 100 700 300]); %[x y width height]
                subplot(1,2,1);
                %Max limit:
                MaxLim = max([0.04 1.01*max(AllVals)]);
                h = polar(0,MaxLim);
                delete(h);
                set(gca, 'Nextplot','add')
                %# draw patches instead of lines: polar(t,r)
                [x,y] = pol2cart(Alla,AllVals);
                h = patch(reshape(x,4,[]), reshape(y,4,[]), [237/255, 28/255, 36/255]);
                title({['Angle histogram; AC = ', num2str(AC), ' +/- ', num2str(std_AC)]; ['fold(180/0) = ', num2str(output_struct.f_180_0), ' +/- ', num2str(std(output_struct.jack_f_180_0))]}, 'FontSize', 8, 'FontName', 'Helvetica');
                xlabel('angle (degrees)', 'FontSize',9, 'FontName', 'Helvetica');
                
                subplot(1,2,2);
                hold on;
                plot(x_theta(x_index_vector), normPDF(y_index_vector), '-', 'LineWidth', 2, 'Color', 'r');
                title(['Amp = ', num2str(mean_Amp), ' +/- ', num2str(std_Amp), '; FWHM = ', num2str(mean_FWHM), ' +/- ', num2str(std_FWHM), ' degrees'], 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('\theta / 2\pi', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('probability', 'FontSize',9, 'FontName', 'Helvetica');
                legend('angle PDF', 'Location', 'NorthEast');
                legend boxoff
                axis([0 1 0 max([1.15*max(normPDF) 0.1])]);
                hold off;
                
                set(figure6,'Units','Inches');
                pos = get(figure6,'Position');
                set(figure6,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
                if SavePDF == 1
                    print(figure6,[output_path, SampleName,'/',SampleName, '_Angle_AC_pooled_',compartment_string,'.pdf'], '-dpdf');
                    print(figure6,[output_path, SampleName,'/',SampleName, '_Angle_AC_pooled_',compartment_string,'.eps'], '-depsc');
                    close
                end
                
                
                figure7 = figure('position',[100 100 700 1200]); %[x y width height]
                
                % add difference between elements to moving threshold for plot
                diff_to_add = .5*(MovingThreshold(2)-MovingThreshold(1));
                
                subplot(4,2,1); % PLOT closest-mean Amp vs. length
                hold on;
                errorbar(1000*(MovingThreshold+diff_to_add), AmpAtClosestMean, std(jack_AmpAtClosestMean), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', [237/255, 28/255, 36/255]);
                title('Amp vs. Mean jump', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('average displacement (nm)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('amplitude', 'FontSize',9, 'FontName', 'Helvetica');
                %axis([0 1.05*1000*max(MovingThreshold+diff_to_add) 0 max([1.15*max(AmpAtClosestMean) 0.3])]);
                axis([0 1020*max(MovingThreshold) 0 0.75]);
                hold off;
                
                subplot(4,2,2); % PLOT closest-min Amp vs. length
                hold on;
                errorbar(1000*(MovingThreshold+diff_to_add), AmpAtClosestMin, std(jack_AmpAtClosestMin), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', [237/255, 28/255, 36/255]);
                title('Amp vs. Min jump', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('min displacement (nm)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('amplitude', 'FontSize',9, 'FontName', 'Helvetica');
                %axis([0 1.05*1000*max(MovingThreshold+diff_to_add) 0 max([1.15*max(AmpAtClosestMin) 0.3])]);
                axis([0 1020*max(MovingThreshold) 0 0.75]);
                hold off;
                
                subplot(4,2,3); % PLOT closest-mean FWHM vs. length
                hold on;
                errorbar(1000*(MovingThreshold+diff_to_add), FWHMAtClosestMean, std(jack_FWHMAtClosestMean), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', [0/255, 153/255, 204/255] );
                title('FWHM vs. Mean jump', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('average displacement (nm)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('FWHM', 'FontSize',9, 'FontName', 'Helvetica');
                %axis([0 1.05*1000*max(MovingThreshold+diff_to_add) 0 max([1.15*max(FWHMAtClosestMean) 30])]);
                axis([0 1020*max(MovingThreshold) 0 max([1.15*max(FWHMAtClosestMean) 30])]);
                hold off;
                
                subplot(4,2,4); % PLOT closest-min FWHM vs. length
                hold on;
                errorbar(1000*(MovingThreshold+diff_to_add), FWHMAtClosestMin, std(jack_FWHMAtClosestMin), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', [0/255, 153/255, 204/255]);
                title('FWHM vs. Min jump', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('min displacement (nm)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('FWHM', 'FontSize',9, 'FontName', 'Helvetica');
                %axis([0 1.05*1000*max(MovingThreshold+diff_to_add) 0 max([1.15*max(FWHMAtClosestMin) 30])]);
                axis([0 1020*max(MovingThreshold) 0 max([1.15*max(FWHMAtClosestMin) 30])]);
                hold off;
                
                subplot(4,2,5); % PLOT closest-mean AC vs. length
                AC_max = max([1.05*max(abs(ACAtClosestMean)) 1.05*max(abs(ACAtClosestMin)) 1]);
                hold on;
                errorbar(1000*(MovingThreshold+diff_to_add), ACAtClosestMean, std(jack_ACAtClosestMean), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', [237/255, 28/255, 36/255]);
                title('AC vs. Mean jump', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('average displacement (nm)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('AC', 'FontSize',9, 'FontName', 'Helvetica');
                %axis([0 1.05*1000*max(MovingThreshold+diff_to_add) -AC_max 1.05]);
                axis([0 1020*max(MovingThreshold) -2.2 1.05]);
                hold off;
                
                subplot(4,2,6); % PLOT closest-min AC vs. length
                hold on;
                errorbar(1000*(MovingThreshold+diff_to_add), ACAtClosestMin, std(jack_ACAtClosestMin), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', [237/255, 28/255, 36/255]);
                title('AC vs. Min jump', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('min displacement (nm)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('AC', 'FontSize',9, 'FontName', 'Helvetica');
                %axis([0 1.05*1000*max(MovingThreshold+diff_to_add) -AC_max 1.05]);
                axis([0 1020*max(MovingThreshold) -2.2 1.05]);
                hold off;
                
                subplot(4,2,7); % PLOT closest-mean f(180/0) vs. length
                hold on;
                errorbar(1000*(MovingThreshold+diff_to_add), output_struct.f_180_0_AtClosestMean, std(output_struct.jack_f_180_0_AtClosestMean), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', [237/255, 28/255, 36/255]);
                title('f(180/0) vs. Mean jump', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('average displacement (nm)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('f(180/0)', 'FontSize',9, 'FontName', 'Helvetica');
                %axis([0 1.05*1000*max(MovingThreshold+diff_to_add) -AC_max 1.05]);
                axis([0 1020*max(MovingThreshold) 0.8 3.1]);
                hold off;
                
                subplot(4,2,8); % PLOT closest-mean f(180/0) vs. length
                hold on;
                errorbar(1000*(MovingThreshold+diff_to_add), output_struct.f_180_0_AtClosestMin, std(output_struct.jack_f_180_0_AtClosestMin), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', [237/255, 28/255, 36/255]);
                title('f(180/0) vs. Min jump', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('min displacement (nm)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('f(180/0)', 'FontSize',9, 'FontName', 'Helvetica');
                %axis([0 1.05*1000*max(MovingThreshold+diff_to_add) -AC_max 1.05]);
                axis([0 1020*max(MovingThreshold) 0.8 3.1]);
                hold off;
                
                set(figure7,'Units','Inches');
                pos = get(figure7,'Position');
                set(figure7,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
                if SavePDF == 1
                    print(figure7,[output_path, SampleName,'/',SampleName, '_Angle_AC_by_jump_',compartment_string,'.pdf'], '-dpdf');
                    print(figure7,[output_path, SampleName,'/',SampleName, '_Angle_AC_by_jump_',compartment_string,'.eps'], '-depsc');
                    close
                end
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%% PLOT MDS RESULTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if MSD_Analysis == 1
                %%% PLOT THE MSD
                xTime = 0:0.005:1.2*max(MSD_time);
                %yMSD = 4 .* MSD_fit_bootstrap_mean(1) .* xTime.^MSD_fit_bootstrap_mean(2) + 4 * MSD_fit_bootstrap_mean(3)^2;
                yMSD = 4 .* MSD_fit_params(1) .* xTime.^MSD_fit_params(2) + 4 * MSD_fit_params(3)^2;
                %text
                Fit1_text(1) = {'Power Law fit: MSD= 4*D*t^a + 4*LocError^2'};
                Fit1_text(2) = {['D = ', num2str(MSD_fit_params(1)), ' um^2/s']};
                Fit1_text(3) = {['D (95% CI): [', num2str(MSD_fit_CI(1,1)), ';', num2str(MSD_fit_CI(2,1)), ']']};
                Fit1_text(4) = {['alpha = ', num2str(MSD_fit_params(2))]};
                Fit1_text(5) = {['alpha (95% CI): [', num2str(MSD_fit_CI(1,2)), ';', num2str(MSD_fit_CI(2,2)), ']']};
                Fit1_text(6) = {['LocError = ', num2str(MSD_fit_params(3)), ' um']};
                Fit1_text(7) = {['LocError (95% CI): [', num2str(MSD_fit_CI(1,3)), ';', num2str(MSD_fit_CI(2,3)), ']']};
                
                figure8 = figure('position',[200 200 350 300]); %[x y width height]
                % LINEAR SCALE
                xMin = 0; xMax = 1.05*max(MSD_time); yMin = 0; yMax = 1.1*max(MSD);
                hold on;
                errorbar(MSD_time, MSD, std(MSD_JackKnife), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', 'r');
                plot(xTime, yMSD, 'k-', 'LineWidth', 2);
                text(0.02*xMax, 0.85*yMax, Fit1_text,'HorizontalAlignment','Left', 'FontSize',8, 'FontName', 'Helvetica');
                title([SampleName,' MSD Analysis'], 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('lag time (s)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('time-averaged MSD (um^2)', 'FontSize',9, 'FontName', 'Helvetica');
                axis([xMin xMax yMin yMax]);
                hold off;
                
                set(figure8,'Units','Inches');
                pos = get(figure8,'Position');
                set(figure8,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
                
                if SavePDF == 1
                    print(figure8,[output_path, SampleName,'/',SampleName, '_MSD_linear_',compartment_string,'.pdf'], '-dpdf');
                    print(figure8,[output_path, SampleName,'/',SampleName, '_MSD_linear_',compartment_string,'.eps'], '-depsc');
                    close
                end
                
                % LOG-LOG SCALE
                figure9 = figure('position',[200 200 350 300]); %[x y width height]
                xMin = 0.95*dT; xMax = 1.25*max(MSD_time); yMin = 0.95*min(MSD); yMax = 1.25*max(MSD);
                hold on;
                errorbar(MSD_time, MSD, std(MSD_JackKnife), 'ko', 'MarkerSize', 8, 'MarkerFaceColor', 'r');
                plot(xTime, yMSD, 'k-', 'LineWidth', 2);
                %text(0.42*xMax, 0.75*yMax, Fit1_text,'HorizontalAlignment','Left', 'FontSize',8, 'FontName', 'Helvetica');
                title('log-log plot', 'FontSize',9, 'FontName', 'Helvetica');
                xlabel('lag time (s)', 'FontSize',9, 'FontName', 'Helvetica');
                ylabel('time-averaged MSD (um^2)', 'FontSize',9, 'FontName', 'Helvetica');
                set(gca,'xscale','log');
                set(gca,'yscale','log');
                axis([xMin xMax yMin yMax]);
                hold off;
                
                set(figure9,'Units','Inches');
                pos = get(figure9,'Position');
                set(figure9,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
                if SavePDF == 1
                    print(figure9,[output_path, SampleName,'/',SampleName, '_MSD_log_',compartment_string,'.pdf'], '-dpdf');
                    print(figure9,[output_path, SampleName,'/',SampleName, '_MSD_log_',compartment_string,'.eps'], '-depsc');
                    close
                end
                
                toc;
                
            end
            
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%% SAVE EVERYTHING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if SavePDF == 1
                if compartment == 1
                    dlmwrite([output_path, SampleName,'/',SampleName, '_bootstrapped_fits.txt'], Tracking_stats_all, '\t');
                    dlmwrite([output_path, SampleName,'/',SampleName, '_all_iterations.txt'], Tracking_stats_bootstrap, '\t');
                    %dlmwrite([output_path, SampleName,'/',SampleName, '_compartmentSwitch.txt'],Compartment_Jumps_Prism,'\t');
                    
                elseif compartment == 3
                    dlmwrite([output_path, SampleName,'/', SampleName, '_bootstrap_summary.txt'], Tracking_stats_all, '\t');
                    dlmwrite([output_path, SampleName,'/',SampleName, '_all_iterations.txt'], Tracking_stats_bootstrap, '\t');
                   % dlmwrite([output_path, SampleName,'/',SampleName, '_compartmentSwitch.txt'],Compartment_Jumps_Prism,'\t');
                    
                end
            end
        end
        close all
        disp(['Finished processing ',SampleName,', ',compartment_string]);
        disp('===========================================================');
        disp('===========================================================');
        disp('===========================================================');   
    end
end
