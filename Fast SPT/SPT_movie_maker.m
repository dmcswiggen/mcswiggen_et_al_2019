clear all
addpath(genpath('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT')); warning('off','all');

Filename = 'Halo-RPB1_5hpi_JF549-100nM_PAJF646-50nM_7msec_1msec-strobe_013';
Filepath_to_image = ['/Volumes/LaCie_McS/Imaging/DM_2_70_fastSPT/4-6hpi/Day_1/',Filename,'.nd2'];
Filepath_to_anotations = ['/Volumes/LaCie_McS/Imaging/DM_2_70_fastSPT/4-6hpi/Day_1/',Filename,'/'];
WriteMovieTo = '/Users/Davidmcswiggen/Desktop/test/test';
MovieLength = 1000;
FirstFrame = 50;

Number_SPT_frames = 20000;
Number_still_frames = 100; %Number of still frames on each side, assumes they're the same (i.e. 100 before, 100 after).
HSV1_Infected = 1;

Inside_color_name = 'Greens';
Outside_color_name = 'Reds';
Transition_color_name = 'Greens';

% InsideRC_color = [256, 256, 256]; %RGB vector
% OutsideRC_color = [68, 68, 151]; %RGB vector
InsideRC_color = [127, 201, 127]; %RGB vector
OutsideRC_color = [151,151,151]; %RGB vector
binaryColorMap = colormap([256,256,256;OutsideRC_color; InsideRC_color]./256);
%binaryColorMap = colormap([linspace(InsideRC_color(1),OutsideRC_color(1),256)',linspace(InsideRC_color(2),OutsideRC_color(2),256)',linspace(InsideRC_color(3),OutsideRC_color(3),256)']./256);
close();

screen_size_vector = get(0,'ScreenSize');
% make sure the plots do not exceed the screen size:
plot_width = min([1200 screen_size_vector(3)]);
plot_height = min([800 screen_size_vector(4)]);



[imgs_3d_double,~] = FastND2Reader(Filepath_to_image,1,1,1000);
ImHeight = size(imgs_3d_double,1);
ImWidth = size(imgs_3d_double,2);
SPT_stack = imgs_3d_double(:,:,Number_still_frames:Number_still_frames +Number_SPT_frames-1);
clear imgs_3d_double

load([Filepath_to_anotations,'Nucleus_mask_drift_corrected.mat']);
load([Filepath_to_anotations,'RC_masks_drift_corrected.mat']);
load([Filepath_to_anotations,Filename,'_Tracked_and_masked.mat']);

colormap_outside = cbrewer2(Outside_color_name,length(trackedPar_nuclear)*2,'cubic');
colormap_inside = cbrewer2(Inside_color_name,length(trackedPar_nuclear)*2,'cubic');
%%
% Find good LUT
for traj = 1:length(trackedPar_nuclear)
    if max(trackedPar_nuclear(traj).compartment)
        trackedPar_nuclear(traj).colorVector = datasample(colormap_inside(round(0.5*length(trackedPar_nuclear)):round(1.5*length(trackedPar_nuclear)),:),1,1);
    else
        trackedPar_nuclear(traj).colorVector = datasample(colormap_outside(round(0.5*length(trackedPar_nuclear)):round(1.5*length(trackedPar_nuclear)),:),1,1);
    end
end

for MovieFrame = FirstFrame:(MovieLength+FirstFrame)
    %find points to plot
    toPlot = [];
    for i = 1:length(trackedPar_nuclear)
        for j = 1:length(trackedPar_nuclear(i).Frame)
            if trackedPar_nuclear(i).Frame(j) == MovieFrame
                toPlot = vertcat(toPlot,[i,j]);
            end
        end
    end
    
    Nuc_outlines = bwboundaries(final_nuclear_stack(:,:,MovieFrame));
    numberOf_nuc_Boundaries_before = size(Nuc_outlines, 1);
    RC_outlines = bwboundaries(RC_mask_binary_stack(:,:,MovieFrame));
    numberOf_RC_Boundaries_before = size(RC_outlines, 1);
    
    
    figure1 = figure;
    imshow(SPT_stack(:,:,MovieFrame),[250 2500],'InitialMagnification',600);
    axis image
    hold on
    for k = 1 : numberOf_nuc_Boundaries_before
        thisBoundary = Nuc_outlines{k};
        plot(thisBoundary(:,2), thisBoundary(:,1),'Color',[0.8 0.8 0.8],'LineStyle','-', 'LineWidth', 2);
    end
    for k = 1 : numberOf_RC_Boundaries_before
        thisBoundary = RC_outlines{k};
        plot(thisBoundary(:,2), thisBoundary(:,1), 'Color',InsideRC_color./256,'LineStyle','--', 'LineWidth', 2);
    end
    
    for i = 1:size(toPlot,1)
        colorVectInside = datasample(colormap_inside(length(trackedPar_nuclear):2*length(trackedPar_nuclear),:),1,1);
        colorVectOutside = datasample(colormap_outside(length(trackedPar_nuclear):2*length(trackedPar_nuclear),:),1,1);
        
        if trackedPar_nuclear(toPlot(i,1)).compartment(toPlot(i,2))
            scatter(trackedPar_nuclear(toPlot(i,1)).xy(toPlot(i,2),1)./0.160+1,trackedPar_nuclear(toPlot(i,1)).xy(toPlot(i,2),2)./0.160+1,...
                80,'MarkerFaceColor',trackedPar_nuclear(toPlot(i,1)).colorVector,'MarkerFaceAlpha',0.1,'MarkerEdgeColor',trackedPar_nuclear(toPlot(i,1)).colorVector);
        else
            scatter(trackedPar_nuclear(toPlot(i,1)).xy(toPlot(i,2),1)./0.160+1,trackedPar_nuclear(toPlot(i,1)).xy(toPlot(i,2),2)./0.160+1,...
                80,'MarkerFaceColor',trackedPar_nuclear(toPlot(i,1)).colorVector,'MarkerFaceAlpha',0.1,'MarkerEdgeColor',trackedPar_nuclear(toPlot(i,1)).colorVector);
        end
        
        if max(trackedPar_nuclear(toPlot(i,1)).compartment) > 0
            plot(trackedPar_nuclear(toPlot(i,1)).xy(1:toPlot(i,2),1)./0.160+1,trackedPar_nuclear(toPlot(i,1)).xy(1:toPlot(i,2),2)./0.160+1,...
                'Color',trackedPar_nuclear(toPlot(i,1)).colorVector,'LineWidth',2);
        else
            plot(trackedPar_nuclear(toPlot(i,1)).xy(1:toPlot(i,2),1)./0.160+1,trackedPar_nuclear(toPlot(i,1)).xy(1:toPlot(i,2),2)./0.160+1,...
                'Color',trackedPar_nuclear(toPlot(i,1)).colorVector,'LineWidth',2);
        end
    end
    hold off
    print(figure1,[WriteMovieTo,'/Frame_',num2str(MovieFrame),'.tif'],'-dtiff','-r0');
    close;
end

