%%%%%%FAST TRACKING DATA ANNOTATE %%%%%%%%%%%%%%%%%
%%%%%%%Copyright (C) 2018 David McSwiggen

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script uses reads in and opens Nikon .nd2 files from
%%%%%%% experiments as in McSwiggen, et. al. 2018. Specifically, movies
%%%%%%% with "Before" and "After" images as reference for SPT movies, to
%%%%%%% manually annotate with ROIs.

%%%%%%% For more information, or to address specific questions, please
%%%%%%% email dmcswiggen@berkeley.edu or direct them to the corresponding
%%%%%%% authors of the McSwiggen, et. al. manuscript.


%% OPEN STACKS IN MATLAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
close all

addpath(genpath('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT'));

input_path = '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-FUS/';
output_path = '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-FUS/';
HSV1_Infected = 1;


%% Find which files to process
%Find all the ND2 files in a folder
nd2_files=dir([input_path,'*.nd2']);
Filenames = ''; %for saving the actual file name
placeholder = 1;
for iter = 1:length(nd2_files)
    if nd2_files(iter).name(1) == '.'
        continue
    else
        Filenames{placeholder} = nd2_files(iter).name(1:end-4);
        placeholder = placeholder + 1;
    end
    
end
InputParameters = whos;

%% Load the data
for iter = 1:length(Filenames)
    if exist([input_path,Filenames{iter},'/roi_metadata.mat'],'file') > 0
        disp('-----------------------------------------------------------------')
        disp([Filenames{iter},' has already been processed.'])
        disp('Skipping this file.')
        
        disp('-----------------------------------------------------------------')
        continue;
        
    else
        if and(exist([input_path,Filenames{iter},'/widefield_before_tracking.tif'],'file'),...
                exist([input_path,Filenames{iter},'/widefield_after_tracking.tif'],'file'))
            clear('except','InputParameters');
            disp('-----------------------------------------------------------------');
            tic;
            
            [s,mess,messid] = mkdir(output_path,Filenames{iter});
            
            
            %Separate into widefield and single-molecule stacks
            Widefield_before_tracking = imread([input_path,Filenames{iter},'/widefield_before_tracking.tif']);
            Widefield_after_tracking = imread([input_path,Filenames{iter},'/widefield_after_tracking.tif']);
            ImHeight = size(Widefield_before_tracking,1);
            ImWidth = size(Widefield_before_tracking,2);
            
            toc;
            disp('-----------------------------------------------------------------');
            disp('User input needed for image segmentation');
            disp('-----------------------------------------------------------------');
            % Segment Nucleus and Replicaton Compartments
            figure('units','normalized','outerposition',[0 0 1 1]);
            subplot (1,2,1);
            imshow(Widefield_before_tracking,[]);
            
            subplot (1,2,2);
            imshow(Widefield_after_tracking,[]);
            
            uiwait(msgbox('Click OK to proceed to outline Nuclei'));
            close;
            
            %%%%% Initial Segmentation of Nuclei %%%%%%%%
            
            nuc_num = 1;
            Nuc_individual_masks = [];
            while nuc_num > 0
                imshow(Widefield_before_tracking,[],'InitialMagnification',600);
                title('Nucleus outlines superimposed');
                axis image;
                hold on;
                if nuc_num > 1
                    for k = 1 : numberOf_nuc_Boundaries
                        thisBoundary = Nuc_outlines{k};
                        plot(thisBoundary(:,2), thisBoundary(:,1), 'r--', 'LineWidth', 2);
                    end
                end
                hold off;
                [Nuc_individual_masks(:,:,nuc_num), x_coord_cell, y_coord_cell] = roipoly();
                roi_info_nuc(nuc_num,1,1) = {x_coord_cell};
                roi_info_nuc(nuc_num,2,1) = {y_coord_cell};
                
                
                button_case = questdlg('Are there more nuclei to segment?');
                switch button_case
                    case 'Yes'
                        nuc_num = nuc_num + 1;
                        Nuc_individual_masks = cat(3,Nuc_individual_masks,zeros(ImHeight,ImWidth,1));
                        Nuc_masks_all(:,:,1) = sum(Nuc_individual_masks,3)>0;
                        Nuc_masks_all(:,:,1) = imfill(Nuc_masks_all(:,:,1),'holes');
                        Nuc_outlines = bwboundaries(Nuc_masks_all(:,:,1),8);
                        numberOf_nuc_Boundaries = size(Nuc_outlines, 1);
                    case 'No'
                        nuc_num = 0;
                        Nuc_masks_all = sum(Nuc_individual_masks,3)>0;
                        Nuc_masks_all = imfill(Nuc_masks_all,'holes');
                        Nuc_outlines = bwboundaries(Nuc_masks_all,8);
                        numberOf_nuc_Boundaries = size(Nuc_outlines, 1);
                    case 'Cancel'
                end
                close;
            end
            
            Nuc_number = size(roi_info_nuc,1);
            hold on;
            imshow(Widefield_after_tracking,[],'InitialMagnification',600);
            impixelinfo;
            axis image;
            hold off;
            for nuc = 1:Nuc_number
                new_nuc_roi = impoly(gca,[cell2mat(roi_info_nuc(nuc,1,1)),cell2mat(roi_info_nuc(nuc,2,1))]);
                %retrieve ROI information stored in roi_info_nuc from the previous entry and read it out as a list into impoly
                message = ['Move ROI for Nucleus #', num2str(nuc), ' of ', num2str(Nuc_number)];
                message = sprintf(message);
                uiwait(msgbox(message));
                Nuc_individual_masks(:,:,nuc) = createMask(new_nuc_roi);
                new_roi_positions = getPosition(new_nuc_roi);
                roi_info_nuc(nuc,1,2) = {new_roi_positions(:,1)};%Deposit new x coordinates into roi_info_nuc
                roi_info_nuc(nuc,2,2) = {new_roi_positions(:,2)};%Deposit new y coordinates into roi_info_nuc
                if nuc == Nuc_number
                    close;
                end
                Nuc_masks_all(:,:,2) = sum(Nuc_individual_masks,3)>0;
                Nuc_masks_all(:,:,2) = imfill(Nuc_masks_all(:,:,2),'holes');
            end
            
            Nuc_outlines_before = bwboundaries(Nuc_masks_all(:,:,1),8);
            numberOf_nuc_Boundaries_before = size(Nuc_outlines_before, 1);
            Nuc_outlines_after = bwboundaries(Nuc_masks_all(:,:,2),8);
            numberOf_nuc_Boundaries_after = size(Nuc_outlines_after, 1);
            
            figure('units','normalized','outerposition',[0 0 1 1]);
            subplot (1,2,1);
            imshow(Widefield_before_tracking,[],'InitialMagnification',300);
            axis image;
            hold on;
            for k = 1 : numberOf_nuc_Boundaries_before
                thisBoundary = Nuc_outlines_before{k};
                plot(thisBoundary(:,2), thisBoundary(:,1), 'r--', 'LineWidth', 2);
            end
            hold off;
            
            subplot (1,2,2);
            imshow(Widefield_after_tracking,[],'InitialMagnification',300);
            axis image;
            hold on;
            for k = 1 : numberOf_nuc_Boundaries_after
                thisBoundary = Nuc_outlines_after{k};
                plot(thisBoundary(:,2), thisBoundary(:,1), 'r--', 'LineWidth', 2);
            end
            hold off;
            
            
            
            if HSV1_Infected == 1
                
                uiwait(msgbox('Click OK to proceed to outline Replication Compartments'));
                close all;
                %%%%% Initial Segmentation of Replication Compartments %%%%%%%%%
                RC_num = 1;
                RC_individual_masks = [];
                while RC_num > 0
                    imshow(Widefield_before_tracking,[],'InitialMagnification',600);
                    title('RC outlines superimposed');
                    axis image;
                    hold on;
                    if RC_num > 1
                        for k = 1 : numberOf_RC_Boundaries
                            thisBoundary = RC_outlines{k};
                            plot(thisBoundary(:,2), thisBoundary(:,1), 'b-', 'LineWidth', 1);
                        end
                    end
                    hold off;
                    [RC_individual_masks(:,:,RC_num), x_coord_cell, y_coord_cell] = roipoly();
                    roi_info_RC(RC_num,1,1) = {x_coord_cell};
                    roi_info_RC(RC_num,2,1) = {y_coord_cell};
                    
                    
                    button_case = questdlg('Are there more RCs to segment?');
                    switch button_case
                        case 'Yes'
                            RC_num = RC_num + 1;
                            RC_individual_masks = cat(3,RC_individual_masks,zeros(ImHeight,ImWidth,1));
                            RC_inside_masks_all(:,:,1) = sum(RC_individual_masks,3)>0;
                            RC_inside_masks_all(:,:,1) = imfill(RC_inside_masks_all(:,:,1),'holes');
                            RC_outlines = bwboundaries(RC_inside_masks_all(:,:,1),8);
                            numberOf_RC_Boundaries = size(RC_outlines, 1);
                        case 'No'
                            RC_num = 0;
                            RC_inside_masks_all = sum(RC_individual_masks,3)>0;
                            RC_inside_masks_all = imfill(RC_inside_masks_all,'holes');
                            RC_outlines = bwboundaries(RC_inside_masks_all,8);
                            numberOf_RC_Boundaries = size(RC_outlines, 1);
                        case 'Cancel'
                    end
                    close;
                end
                
                RC_number = size(roi_info_RC,1);
                
                hold on;
                imshow(Widefield_after_tracking,[],'InitialMagnification',600);
                impixelinfo;
                axis image;
                hold off;
                for RC = 1:RC_number
                    new_RC_roi = impoly(gca,[cell2mat(roi_info_RC(RC,1,1)),cell2mat(roi_info_RC(RC,2,1))]); %retrieve ROI information stored in roi_info_nuc from the previous entry and read it out as a list into impoly
                    message = ['Move ROI for RC# ', num2str(RC), ' of ', num2str(RC_number)];
                    message = sprintf(message);
                    uiwait(msgbox(message));
                    RC_individual_masks(:,:,RC) = createMask(new_RC_roi);
                    new_roi_positions = getPosition(new_RC_roi);
                    roi_info_RC(RC,1,2) = {new_roi_positions(:,1)};%Deposit new x coordinates into roi_info_nuc
                    roi_info_RC(RC,2,2) = {new_roi_positions(:,2)};%Deposit new y coordinates into roi_info_nuc
                end
                %create one flat mask incorporating all possible RC masks into one boolean.
                %Save this to the RC_masks_all, which is what will determine whether a track is counted or not
                RC_inside_masks_all(:,:,2) = sum(RC_individual_masks,3)>0;
                RC_inside_masks_all(:,:,2) = imfill(RC_inside_masks_all(:,:,2),'holes');
                
                RC_outlines_before = bwboundaries(RC_inside_masks_all(:,:,1),8);
                numberOf_RC_Boundaries_before = size(RC_outlines_before, 1);
                RC_outlines_after = bwboundaries(RC_inside_masks_all(:,:,2),8);
                numberOf_RC_Boundaries_after = size(RC_outlines_after, 1);
                close;
                
                figure('units','normalized','outerposition',[0 0 1 1]);
                subplot (1,2,1);
                imshow(Widefield_before_tracking,[],'InitialMagnification',300);
                axis image;
                hold on;
                for k = 1 : numberOf_nuc_Boundaries_before
                    thisBoundary = Nuc_outlines_before{k};
                    plot(thisBoundary(:,2), thisBoundary(:,1), 'r--', 'LineWidth', 2);
                end
                for i = 1 : numberOf_RC_Boundaries_before
                    thisBoundary = RC_outlines_before{i};
                    plot(thisBoundary(:,2), thisBoundary(:,1), 'b-', 'LineWidth', 1);
                end
                hold off;
                
                subplot (1,2,2);
                imshow(Widefield_after_tracking,[],'InitialMagnification',300);
                axis image;
                hold on;
                for k = 1 : numberOf_nuc_Boundaries_after
                    thisBoundary = Nuc_outlines_after{k};
                    plot(thisBoundary(:,2), thisBoundary(:,1), 'r--', 'LineWidth', 2);
                end
                for i = 1 : numberOf_RC_Boundaries_after
                    thisBoundary = RC_outlines_after{i};
                    plot(thisBoundary(:,2), thisBoundary(:,1), 'b-', 'LineWidth', 1);
                end
                hold off;
            end
            
            disp('-----------------------------------------------------------------');
            disp('Saving masked data');
            tic;
            if HSV1_Infected == 1
                save([output_path,Filenames{iter},'/roi_metadata.mat'],'roi_info_nuc','roi_info_RC', '-v7.3');
            else
                save([output_path,Filenames{iter},'/roi_metadata.mat'],'roi_info_nuc', '-v7.3');
            end
            clear roi_info_nuc roi_info_RC RC_outlines Nuc_outlines RC_inside_masks_all Nuc_masks_all
            toc;
            disp('-----------------------------------------------------------------');
            uiwait(msgbox('Click OK to proceed to the next file...'));
            
            close all;
        end
        
    end
end

