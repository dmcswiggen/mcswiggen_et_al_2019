function [CompleteFlag] = DriftCorrectScramble(FilepathIn,FilePathToLib, FilepathOut)
disp(['Processing file ', FilepathIn]);
if and(exist([FilepathIn,'/roi_metadata.mat'],'file'),exist([FilepathIn,'/SPT_raw_images_double.mat'],'file'))
    if exist(FilePathToLib,'file')
        load(FilePathToLib);
        load([FilepathIn,'/roi_metadata.mat']);
        if size(roi_info_nuc,1) == 1
            load([FilepathIn,'/SPT_raw_images_double.mat']);
            
            %Prepare Library Statistics
            number_RC_data = [];
            percent_RC_area = [];
            for nuc = 1:length(Per_nucleus_struct)
                number_RC_data = vertcat(number_RC_data, Per_nucleus_struct(nuc).number);
                percent_RC_area = vertcat(percent_RC_area, round(Per_nucleus_struct(nuc).frac_RC_area*100));
            end
            nuc_RC_PDF = (histc(number_RC_data,[1:1:max(number_RC_data)]))';
            RC_area_PDF = (histc(percent_RC_area,[1:1:max(percent_RC_area)]))';
            
            ImHeight = size(SPT_stack,1);
            ImWidth = size(SPT_stack,2);
            number_of_frames = size(SPT_stack,3);
            number_of_nuclei = size(roi_info_nuc,1);
            if exist('roi_info_RC','var')
                First_nuclear_mask = poly2mask(roi_info_nuc{1,1,1},roi_info_nuc{1,2,1},ImHeight,ImWidth);
                temp_RC_mask = zeros(ImHeight,ImWidth,size(roi_info_RC,2));
                for roi = 1:size(roi_info_RC,1)
                    temp_RC_mask(:,:,roi) = poly2mask(roi_info_RC{roi,1,2},roi_info_RC{roi,2,2},ImHeight,ImWidth);
                end
                temp_RC_mask = sum(temp_RC_mask,3);
                temp_RC_mask = temp_RC_mask > 0;
                temp_nuclear_mask = and(First_nuclear_mask,~temp_RC_mask);
            else
                First_nuclear_mask = poly2mask(roi_info_nuc{1,1,1},roi_info_nuc{1,2,1},ImHeight,ImWidth);
                temp_nuclear_mask = First_nuclear_mask;
            end
            %% Randomly place RCs inside of the first nucleus frame
            roi_size_ok = 0;
            roi_number_ok = 0;
            failed_attempt = 1;
            break_number  = 1;
            roi_area_record = [];
            while roi_number_ok == 0
                number_of_RC = gendist(nuc_RC_PDF,1,1);
                percent_of_RC_area = gendist(RC_area_PDF,1,1);
                lower_bound = (percent_of_RC_area * 0.80)/100;
                upper_bound = (percent_of_RC_area * 1.20)/100;
                while roi_size_ok == 0
                    roi_list = datasample(all_RC_struct,number_of_RC,'Replace',false);
                    % Check to make sure that RCs are "realistic" - not too much of
                    % nucleus is taken up by RC
                    ImCenter = [round(ImHeight/2),round(ImWidth/2)];
                    total_roi_area = 0;
                    total_nuc_area = sum(sum(First_nuclear_mask));
                    for roi = 1:length(roi_list)
                        BW = poly2mask(roi_list(roi).RC_boundaries_centered(:,2)+ImCenter(2),roi_list(roi).RC_boundaries_centered(:,1)+ImCenter(1),ImHeight,ImWidth);
                        roi_area = sum(sum(BW));
                        total_roi_area = total_roi_area + roi_area;
                    end
                    if total_roi_area < (total_nuc_area * upper_bound)
                        if total_roi_area > (total_nuc_area * lower_bound)
                            roi_size_ok = 1;
                        else
                            failed_attempt = failed_attempt + 1;
                            if failed_attempt > 1000
                                failed_attempt = 1;
                                break_number  = break_number + 1;
                                break
                            end
                            roi_area_record = vertcat(roi_area_record,total_roi_area);
                        end
                    else
                        failed_attempt = failed_attempt + 1;
                        if failed_attempt > 1000
                            failed_attempt = 1;
                            break_number  = break_number + 1;
                            break
                        end
                        roi_area_record = vertcat(roi_area_record,total_roi_area);
                    end
                end
                if roi_size_ok == 1
                    roi_number_ok = 1;
                end
            end
            
            roi_iter = 1;
            failed_attempt = 1;
            roi_info_RC = cell(number_of_RC,2,1);
            
            while roi_iter < number_of_RC + 1
                Nuclear_coordinates = regionprops(temp_nuclear_mask,'PixelList');
                center_idx = randi(length(Nuclear_coordinates(1).PixelList));
                center_coord = [Nuclear_coordinates(1).PixelList(center_idx,1),Nuclear_coordinates(1).PixelList(center_idx,2)];%[Row,Column]
                roi_info_RC{roi_iter,1,1} = round(roi_list(roi_iter).RC_boundaries_centered(:,2) + center_coord(2));
                roi_info_RC{roi_iter,2,1} = round(roi_list(roi_iter).RC_boundaries_centered(:,1) + center_coord(1));
                temp_roi_placement = poly2mask(roi_info_RC{roi_iter,2,1},roi_info_RC{roi_iter,1,1},ImHeight,ImWidth);
                se = strel('diamond',7);
                temp_roi_placement = imdilate(temp_roi_placement,se);
                if max(max(temp_roi_placement - and(temp_roi_placement,temp_nuclear_mask))) == 0
                    temp_nuclear_mask = temp_nuclear_mask - temp_roi_placement;
                    roi_iter = roi_iter + 1;
                else
                    failed_attempt = failed_attempt + 1;
                    if failed_attempt > 3000
                        break
                    end
                end
            end
            roi_info_RC = roi_info_RC(~cellfun('isempty',roi_info_RC));
            roi_info_RC = reshape(roi_info_RC,[size(roi_info_RC,1)/2,2]);
            number_of_RC = size(roi_info_RC,1);
            
            %% Perfrom Drift correction
            final_nuclear_mask = zeros(ImHeight,ImWidth);
            
            %Generate a separate stack of same size as SPT_stack that contains
            %all of the ROI information about the replication compartments.
            temp_RC_mask = zeros(ImHeight,ImWidth,number_of_RC);
            Scrambled_mask_binary_stack = ones(ImHeight,ImWidth,number_of_frames);
            original_x = roi_info_nuc{1,1,1};
            final_x = roi_info_nuc{1,1,2};
            delta_x = final_x - original_x;
            original_y = roi_info_nuc{1,2,1};
            final_y = roi_info_nuc{1,2,2};
            delta_y = final_y - original_y;
            for slice = 1:number_of_frames
                for RC = 1:number_of_RC
                    original_x = roi_info_RC{RC,1};
                    original_y = roi_info_RC{RC,2};
                    current_frame_coords_x = original_x + (delta_x(1)*((slice-1)/number_of_frames)); %Use the same X and Y displacements as the nucleus
                    current_frame_coords_y = original_y + (delta_y(1)*((slice-1)/number_of_frames));
                    temp_RC_mask(:,:,RC) = poly2mask(current_frame_coords_x, current_frame_coords_y,ImHeight,ImWidth);
                end
                temp_RC_all_masks = sum(temp_RC_mask,3);
                temp_RC_all_masks = temp_RC_all_masks > 0;
                temp_RC_all_masks = imfill(temp_RC_all_masks, 'holes');
                Scrambled_mask_binary_stack(:,:,slice) = temp_RC_all_masks;
            end
            
            Scrambled_mask_binary_stack = logical(Scrambled_mask_binary_stack);
            if exist('FilepathOut','var')
                save([FilepathOut,'/Scrambled_masks_drift_corrected.mat'],'Scrambled_mask_binary_stack', '-v7.3');
            else
                save([FilepathIn,'/Scrambled_masks_drift_corrected.mat'],'Scrambled_mask_binary_stack', '-v7.3');
            end
            CompleteFlag = 1;
        else
            CompleteFlag = 0;
        end
    else
        CompleteFlag = 0;
        disp('No Library of RCs was provided');
    end
    
    
else
    CompleteFlag = 0;
end


end