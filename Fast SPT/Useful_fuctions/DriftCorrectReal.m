function [CompleteFlag] = DriftCorrectReal(FilepathIn,FilepathOut)
disp(FilepathIn);
if and(exist([FilepathIn,'/roi_metadata.mat'],'file'),exist([FilepathIn,'/SPT_raw_images_double.mat'],'file'))
    load([FilepathIn,'/roi_metadata.mat']);
    load([FilepathIn,'/SPT_raw_images_double.mat']);
    ImHeight = size(SPT_stack,1);
    ImWidth = size(SPT_stack,2);
    number_of_frames = size(SPT_stack,3);
    number_of_nuclei = size(roi_info_nuc,1);
    if exist('roi_info_RC','var')
        number_of_RC = size(roi_info_RC,1);
        temp_RC_mask = zeros(ImHeight,ImWidth,number_of_RC);
    end
    
    temp_nuclear_masks = zeros(ImHeight,ImWidth,number_of_nuclei);
    
    final_nuclear_stack = ones(ImHeight,ImWidth,number_of_frames);
    for slice = 1:number_of_frames
        %Correct for any drift in the nuclei individually assuming linear
        %movement between start and end, then apply that mask to the SPT
        %images.
        for nucleus = 1:number_of_nuclei
            original_x = roi_info_nuc{nucleus,1,1};
            final_x = roi_info_nuc{nucleus,1,2};
            delta_x = final_x - original_x;
            original_y = roi_info_nuc{nucleus,2,1};
            final_y = roi_info_nuc{nucleus,2,2};
            delta_y = final_y - original_y;
            current_frame_coords_x = original_x + delta_x.*((slice-1)/number_of_frames);
            current_frame_coords_y = original_y + delta_y.*((slice-1)/number_of_frames);
            temp_nuclear_masks(:,:,nucleus) = poly2mask(current_frame_coords_x, current_frame_coords_y,ImHeight,ImWidth);
        end
        final_nuclear_mask = sum(temp_nuclear_masks,3);
        final_nuclear_mask = final_nuclear_mask >0;
        final_nuclear_mask = imfill(final_nuclear_mask, 'holes');
        final_nuclear_stack(:,:,slice) = final_nuclear_mask;
    end
    final_nuclear_stack = logical(final_nuclear_stack);
    if exist('FilepathOut','var')
        save([FilepathOut,'/Nucleus_mask_drift_corrected.mat'],'final_nuclear_stack', '-v7.3');
    else
        save([FilepathIn,'/Nucleus_mask_drift_corrected.mat'],'final_nuclear_stack', '-v7.3');
    end
    
    
    %Generate a separate stack of same size as SPT_stack that contains
    %all of the ROI information about the replication compartments.
    RC_mask_binary_stack = ones(ImHeight,ImWidth,number_of_frames);
    for slice = 1:number_of_frames
        if exist('roi_info_RC','var')
            for RC = 1:number_of_RC
                original_x = roi_info_RC{RC,1,1};
                final_x = roi_info_RC{RC,1,2};
                delta_x = final_x - original_x;
                original_y = roi_info_RC{RC,2,1};
                final_y = roi_info_RC{RC,2,2};
                delta_y = final_y - original_y;
                current_frame_coords_x = original_x + delta_x.*((slice-1)/number_of_frames);
                current_frame_coords_y = original_y + delta_y.*((slice-1)/number_of_frames);
                temp_RC_mask(:,:,RC) = poly2mask(current_frame_coords_x, current_frame_coords_y,ImHeight,ImWidth);
            end
            temp_RC_all_masks = sum(temp_RC_mask,3);
            temp_RC_all_masks = temp_RC_all_masks > 0;
            temp_RC_all_masks = imfill(temp_RC_all_masks, 'holes');
            RC_mask_binary_stack(:,:,slice) = temp_RC_all_masks;
        end
    end
    if exist('roi_info_RC','var')
        RC_mask_binary_stack = logical(RC_mask_binary_stack);
        if exist('FilepathOut','var')
            save([FilepathOut,'/RC_masks_drift_corrected.mat'],'RC_mask_binary_stack', '-v7.3');
        else
            save([FilepathIn,'/RC_masks_drift_corrected.mat'],'RC_mask_binary_stack', '-v7.3');
        end
    end
    CompleteFlag = 1;
else
    CompleteFlag = 0;
end
end