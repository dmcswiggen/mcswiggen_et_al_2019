function [color1,color2] = FastND2Reader(Path2File,channels, Convert2Double, notifications)

%% Description
% PATH2File is the file path to the image
% Channels is a binary vector of which frames to keep. Nikon saves each
% time point as alternating colors, so [1,0] would keep all the images from
% color one, and discard all the slices from color 2.

%Add BioFormats to path
addpath(genpath('/Users/Davidmcswiggen/Documents/MATLAB/Anders_scripts/SLIMFAST_batch_fordist'));
%%% read nd2 files:
r = bfGetReader();
% Decorate the reader with the Memoizer wrapper
r = loci.formats.Memoizer(r);
% Initialize the reader with an input file
% If the call is longer than a minimal time, the initialized reader will
% be cached in a file under the same directory as the initial file
% name .large_file.bfmemo
r.setId(Path2File);

% First figure out how many frames:
TotSlices = r.getImageCount();
TotFrames = TotSlices / sum(channels);

% Read in the first frame to get size of images:
first_frame = bfGetPlane(r, 1);

% Define empty TIFF-like matrices. Very annoyingly, Nikon Elements make every other frame each color. So
% make two new tiff stacks for each color that are half the length in
% the 3rd dimension:
imagestack = zeros(size(first_frame,1), size(first_frame,2), TotSlices);

% now read in only one frame at a time to save on memory
if exist('notifications','var')
    
    for FrameIter = 1:size(imagestack,3)
        if mod(FrameIter,notifications)==0
            disp(['Getting plane ' num2str(FrameIter),'...'])
        end
        imagestack(:,:,FrameIter) = bfGetPlane(r,FrameIter);
    end
else
    for FrameIter = 1:size(imagestack,3)
        imagestack(:,:,FrameIter) = bfGetPlane(r,FrameIter);
    end
end

if exist('Convert2Double','var')
    if Convert2Double == 1
    imagestack = im2double(imagestack);
    end
end

if length(channels) > 1
    if channels == [1,0]
        color1 = imagestack(:,:,1:2:TotSlices);
        color2 = [];
    elseif channels == [0,1]
        color1 = [];
        color2 = imagestack(:,:,2:2:TotSlices);
    elseif channels == [1,1];
        color1 = imagestack(:,:,1:2:TotSlices);
        color2 = imagestack(:,:,2:2:TotSlices);
    else
        error('An unknown format was used fto specify which channels to keep')
    end
else
    color1 = imagestack;
    color2 = [];
end



% Close the reader
r.close()
end

