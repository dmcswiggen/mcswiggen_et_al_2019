NumberToSubsample = 25;
NumberIterations = 10000;
nTrajPerCell = [All_days_data(:,5),All_days_data(:,10)];
NumberOutside = zeros(NumberToSubsample,NumberIterations);
NumberInside = zeros(NumberToSubsample,NumberIterations);
for i = 1:NumberToSubsample
    for j = 1:NumberIterations
        X = sum(datasample(nTrajPerCell,i,1,'Replace',false),1,'omitnan');
        NumberOutside(i,j) = X(1);
        NumberInside(i,j) = X(2);
    end
end

NumPerSamplingInside = [mean(NumberInside,2),std(NumberInside,1,2)];
NumPerSamplingOutside = [mean(NumberOutside,2),std(NumberOutside,1,2)];