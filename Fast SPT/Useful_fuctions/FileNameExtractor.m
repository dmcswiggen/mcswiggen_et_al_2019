function Filenames = FileNameExtractor(input_path, Filetype, PALM_pfx, WF_pfx)

% Input path is the directory to process

% Filetype is a string. 'File' to look for individual .nd2 files, 'Dir' to
% look for directories

% PALM_sfx is the identifier for the PALM images (only relevent if 'File'
% is chosen

% WF_sfx is the identifier for the matched widefield images to a PALM
% image. May be omitted.
Length_PALMid = [];
if exist('PALM_pfx','var')
    if ~isempty(PALM_pfx)
        Length_PALMid = length(PALM_pfx);
        PALM_prefix = PALM_pfx;
    end
end

Length_widefieldID = [];
if exist('WF_pfx','var')
    if ~isempty(WF_pfx)
        Length_widefieldID = length(WF_pfx);
        WF_prefix = WF_pfx;
    end
end


Filenames = ''; %for saving the actual file name
FilenamesPALM = '';
FilenamesWF = '';

if strcmp(Filetype,'Dir')
    directory_contents=dir(input_path);
    Fname_counter = 1;
    for iter = 1:length(directory_contents)
        if and(directory_contents(iter).isdir, ~strcmp(directory_contents(iter).name(1),'.'))
            Filenames{Fname_counter,1} = directory_contents(iter).name;
            Fname_counter = Fname_counter + 1;
        end
    end
else
    directory_contents=dir([input_path,'*',Filetype]);
    if ~isempty(Length_widefieldID)
        PALMCounter = 1;
        WFCounter = 1;
        for iter = 1:length(directory_contents)
            if strcmp(directory_contents(iter).name(1:Length_PALMid),PALM_prefix)
                FilenamesPALM{PALMCounter,1} = directory_contents(iter).name(1:end-4);
                PALMCounter = PALMCounter + 1;
            elseif strcmp(directory_contents(iter).name(1:Length_widefieldID),WF_prefix)
                FilenamesWF{WFCounter,1} = directory_contents(iter).name(1:end-4);
                WFCounter = WFCounter + 1;
            end
        end
        
        if ~isempty(FilenamesWF)
            Fname_counter = 1;
            for i = 1:size(FilenamesPALM,1)
                for j = 1:size(FilenamesWF,1)
                    if strcmp(FilenamesWF{j}(Length_widefieldID+1:end), FilenamesPALM{i}(Length_PALMid+1:end))
                        Filenames{Fname_counter,1} = FilenamesPALM{i};
                        Filenames{Fname_counter,2} = FilenamesWF{j};
                        Fname_counter = Fname_counter + 1;
                    end
                end
            end
        else
            Filenames = FilenamesPALM;
        end
        
    else
        PALMCounter = 1;
        for iter = 1:length(directory_contents)
            if strcmp(directory_contents(iter).name(1:Length_PALMid),PALM_prefix)
                Filenames{PALMCounter,1} = directory_contents(iter).name(1:end-4);
                PALMCounter = PALMCounter + 1;
            end
        end
    end
end
end