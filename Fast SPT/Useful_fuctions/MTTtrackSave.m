function [CompleteFlag] = MTTLocalizeSave(FilepathIn,impars,locpars,fName,FilepathOut)
disp(['Localizing detections in file ', FilepathIn]);
if exist([FilepathIn,'/SPT_raw_images_double.mat'],'file')
    load([FilepathIn,'/SPT_raw_images_double.mat'])
    impars.name = fName;
    data = localizeParticles_ASH(input_path,impars, locpars, SPT_stack);
    %Remove detections from outside of the nucleus BEFORE tracking
    if exist([FilepathIn,'/Nucleus_mask_drift_corrected.mat'],'file')
        load([FilepathIn,'/Nucleus_mask_drift_corrected.mat']);
        new_index = 1;
        for detection = 1:length(data.ctrsX)
            if final_nuclear_stack(round(data.ctrsY(detection)),round(data.ctrsX(detection)),data.frame(detection)) == 1
                data_nuclear.ctrsX(new_index,1) = data.ctrsX(detection);
                data_nuclear.ctrsY(new_index,1) = data.ctrsY(detection);
                data_nuclear.signal(new_index,1) = data.signal(detection);
                data_nuclear.noise(new_index,1) = data.noise(detection);
                data_nuclear.offset(new_index,1) = data.offset(detection);
                data_nuclear.radius(new_index,1) = data.radius(detection);
                data_nuclear.frame(new_index,1) = data.frame(detection);
                new_index = new_index + 1;
            else
                continue
            end
        end
        data_nuclear.ctrsN = (histcounts(data_nuclear.frame,[1:size(SPT_stack,3)+1]))';
        data = data_nuclear;
    end
    if exist('FilepathOut','var')
        save([FilepathOut,'/Localizations.mat'],'data', '-v7.3');
    else
        save([FilepathIn,'/Localizations.mat'],'data', '-v7.3');
    end
    
    CompleteFlag = 1;
else
    CompleteFlag = 0;
end


end