function [CompleteFlag] = MTTLocalizeTrackSort(input_path,impars,locpars,trackpars,fName,FilepathOut)
FilepathIn = [input_path,fName];

if and(exist([FilepathIn,'/SPT_raw_images_double.mat'],'file'),~exist([FilepathIn,'/Localizations.mat'],'file'))
    load([FilepathIn,'/SPT_raw_images_double.mat'])
    disp(['Localizing detections in file ', FilepathIn]);
    impars.name = fName;
    %% Localization
    data = localizeParticles_ASH(input_path,impars, locpars, SPT_stack);
    %Remove detections from outside of the nucleus BEFORE tracking
    if exist([FilepathIn,'/Nucleus_mask_drift_corrected.mat'],'file')
        load([FilepathIn,'/Nucleus_mask_drift_corrected.mat']);
        new_index = 1;
        for detection = 1:length(data.ctrsX)
            if final_nuclear_stack(round(data.ctrsY(detection)),round(data.ctrsX(detection)),data.frame(detection)) == 1
                data_nuclear.ctrsX(new_index,1) = data.ctrsX(detection);
                data_nuclear.ctrsY(new_index,1) = data.ctrsY(detection);
                data_nuclear.signal(new_index,1) = data.signal(detection);
                data_nuclear.noise(new_index,1) = data.noise(detection);
                data_nuclear.offset(new_index,1) = data.offset(detection);
                data_nuclear.radius(new_index,1) = data.radius(detection);
                data_nuclear.frame(new_index,1) = data.frame(detection);
                new_index = new_index + 1;
            else
                continue
            end
        end
        data_nuclear.ctrsN = (histcounts(data_nuclear.frame,[1:size(SPT_stack,3)+1]))';
        data = data_nuclear;
    end
    if exist('FilepathOut','var')
        save([FilepathOut,'/Localizations.mat'],'data', '-v7.3');
    else
        save([FilepathIn,'/Localizations.mat'],'data', '-v7.3');
    end
elseif exist([FilepathIn,'/Localizations.mat'],'file')
    load([FilepathIn,'/Localizations.mat'])
else
    CompleteFlag = 0;
end

%% Tracking
if and(~exist([FilepathIn,'/Trajectories.mat'],'file'),exist([FilepathIn,'/Localizations.mat'],'file'))
    load([FilepathIn,'/SPT_raw_images_double.mat'])
    disp(['Tracking detections in file ', FilepathIn]);
    data_tracked =buildTracks2_ASH(input_path,data,impars,locpars,trackpars,data.ctrsN,SPT_stack);
    data_cell_array = data_tracked.tr;
    % save meta-data
    settings.Delay = impars.FrameRate;
    settings.px2micron = impars.PixelSize;
    settings.TrackingOptions = trackpars;
    settings.LocOptions = locpars;
    settings.AcquisitionOptions = impars;
    settings.Filename = impars.name;
    settings.Width = size(SPT_stack,2);
    settings.Height = size(SPT_stack,1);
    settings.Frames = size(SPT_stack,3);
    
    if exist('FilepathOut','var')
        save([FilepathOut,'/Trajectories.mat'],'data_tracked', '-v7.3');
    else
        save([FilepathIn,'/Trajectories.mat'],'data_tracked', '-v7.3');
    end
elseif exist([FilepathIn,'/Trajectories.mat'],'file')
    load([FilepathIn,'/Trajectories.mat'])
else
    CompleteFlag = 0;
end


%% Sorting the real annotations
if and(and(~exist([FilepathIn,fName,'_Tracked_and_masked.mat'],'file'),...
        exist([FilepathIn,'/Trajectories.mat'],'file')),...
        exist([FilepathIn,'/roi_metadata.mat']))
    
    load([FilepathIn,'/roi_metadata.mat']);
    load([FilepathIn,'/Nucleus_mask_drift_corrected.mat']);
    
    if ~exist('data_cell_array','var')
        load([FilepathIn,'/Trajectories.mat']);
        data_cell_array = data_tracked.tr;
    end
    
    if exist('roi_info_RC','var')
        load([FilepathIn,'/RC_masks_drift_corrected.mat']);
    end
    disp(['Sorting detections in file ', FilepathIn]);
    
    trackedPar = struct;
    for traj=1:length(data_cell_array)
        %convert to um:
        trackedPar(1,traj).xy =  impars.PixelSize .* data_cell_array{traj}(:,1:2);
        trackedPar(traj).Frame = data_cell_array{traj}(:,3);
        trackedPar(traj).TimeStamp = impars.FrameRate.* data_cell_array{traj}(:,3);
        trackedPar(traj).nuclear_trajectories = zeros(1,size(trackedPar(traj).Frame,1));
        trackedPar(traj).compartment = zeros(1,size(trackedPar(traj).Frame,1));
        for track = 1:size(trackedPar(1,traj).xy,1)
            if final_nuclear_stack(round(data_cell_array{traj}(track,2)),round(data_cell_array{traj}(track,1)),trackedPar(traj).Frame(track)) == 1
                trackedPar(traj).nuclear_trajectories(track) = 1;
            end
            
            if exist('RC_mask_binary_stack','var')
                if RC_mask_binary_stack(round(data_cell_array{traj}(track,2)),round(data_cell_array{traj}(track,1)),trackedPar(traj).Frame(track)) == 1
                    trackedPar(traj).compartment(track) = 1;
                end
            end
        end
    end
    
    trackedPar_nuclear = struct;
    trackedPar_insideRC = struct;
    trackedPar_outside = struct;
    counter_nuc = 1;
    counter_inside = 1;
    counter_outside = 1;
    for traj = 1:length(trackedPar)
        if min(trackedPar(traj).nuclear_trajectories) == 0
            continue
        else
            trackedPar_nuclear(counter_nuc).xy = trackedPar(traj).xy;
            trackedPar_nuclear(counter_nuc).Frame = trackedPar(traj).Frame;
            trackedPar_nuclear(counter_nuc).TimeStamp = trackedPar(traj).TimeStamp;
            trackedPar_nuclear(counter_nuc).nuclear_trajectories = trackedPar(traj).nuclear_trajectories;
            trackedPar_nuclear(counter_nuc).compartment = trackedPar(traj).compartment;
            
            if or(min(trackedPar(traj).compartment) == 1 , sum(trackedPar(traj).compartment) > 1)
                %Identify any trajectories in which it never leaves an
                %RC or spends at least two frames in the RC (can make
                %more stringent, but given sparsity of data, seems like
                %not a problem).
                trackedPar_insideRC(counter_inside).xy = trackedPar(traj).xy;
                trackedPar_insideRC(counter_inside).Frame = trackedPar(traj).Frame;
                trackedPar_insideRC(counter_inside).TimeStamp = trackedPar(traj).TimeStamp;
                trackedPar_insideRC(counter_inside).nuclear_trajectories = trackedPar(traj).nuclear_trajectories;
                trackedPar_insideRC(counter_inside).compartment = trackedPar(traj).compartment;
                counter_inside = counter_inside + 1;
            else
                trackedPar_outside(counter_outside).xy = trackedPar(traj).xy;
                trackedPar_outside(counter_outside).Frame = trackedPar(traj).Frame;
                trackedPar_outside(counter_outside).TimeStamp = trackedPar(traj).TimeStamp;
                trackedPar_outside(counter_outside).nuclear_trajectories = trackedPar(traj).nuclear_trajectories;
                trackedPar_outside(counter_outside).compartment = trackedPar(traj).compartment;
                counter_outside = counter_outside + 1;
            end
            counter_nuc = counter_nuc + 1;
        end
    end
    
    if exist('FilepathOut','var')
        save([FilepathOut,'/',fName,'_Tracked.mat'], 'trackedPar', '-v7.3');
        save([FilepathOut,'/',fName,'_Tracked_and_masked.mat'], 'trackedPar_nuclear', 'trackedPar_insideRC', 'trackedPar_outside', '-v7.3');
    else
        save([FilepathIn,'/',fName,'_Tracked.mat'], 'trackedPar',  '-v7.3');
        save([FilepathIn,'/',fName,'_Tracked_and_masked.mat'], 'trackedPar_nuclear', 'trackedPar_insideRC', 'trackedPar_outside', '-v7.3');
    end
end


%% Sorting scrambled annotations, if any
if and(and(~exist([FilepathIn,fName,'_Tracked_and_masked.mat'],'file'),...
        exist([FilepathIn,'/Trajectories.mat'],'file')),...
        exist([FilepathIn,'/Scrambled_masks_drift_corrected.mat'],'file'))
    load([FilepathIn,'/Scrambled_masks_drift_corrected.mat']);
    
    trackedPar = struct;
    for traj=1:length(data_cell_array)
        %convert to um:
        trackedPar(1,traj).xy =  impars.PixelSize .* data_cell_array{traj}(:,1:2);
        trackedPar(traj).Frame = data_cell_array{traj}(:,3);
        trackedPar(traj).TimeStamp = impars.FrameRate.* data_cell_array{traj}(:,3);
        trackedPar(traj).nuclear_trajectories = zeros(1,size(trackedPar(traj).Frame,1));
        trackedPar(traj).compartment = zeros(1,size(trackedPar(traj).Frame,1));
        for track = 1:size(trackedPar(1,traj).xy,1)
            if final_nuclear_stack(round(data_cell_array{traj}(track,2)),round(data_cell_array{traj}(track,1)),trackedPar(traj).Frame(track)) == 1
                trackedPar(traj).nuclear_trajectories(track) = 1;
            end
            if Scrambled_mask_binary_stack(round(data_cell_array{traj}(track,2)),round(data_cell_array{traj}(track,1)),trackedPar(traj).Frame(track)) == 1
                trackedPar(traj).compartment(track) = 1;
            end
        end
    end
    
    trackedPar_nuclear = struct;
    trackedPar_insideRC = struct;
    trackedPar_outside = struct;
    counter_nuc = 1;
    counter_inside = 1;
    counter_outside = 1;
    for traj = 1:length(trackedPar)
        if min(trackedPar(traj).nuclear_trajectories) == 0
            continue
        else
            trackedPar_nuclear(counter_nuc).xy = trackedPar(traj).xy;
            trackedPar_nuclear(counter_nuc).Frame = trackedPar(traj).Frame;
            trackedPar_nuclear(counter_nuc).TimeStamp = trackedPar(traj).TimeStamp;
            trackedPar_nuclear(counter_nuc).nuclear_trajectories = trackedPar(traj).nuclear_trajectories;
            trackedPar_nuclear(counter_nuc).compartment = trackedPar(traj).compartment;
            
            if or(min(trackedPar(traj).compartment) == 1 , sum(trackedPar(traj).compartment) > 1)
                %Identify any trajectories in which it never leaves an
                %RC or spends at least two frames in the RC (can make
                %more stringent, but given sparsity of data, seems like
                %not a problem.
                trackedPar_insideRC(counter_inside).xy = trackedPar(traj).xy;
                trackedPar_insideRC(counter_inside).Frame = trackedPar(traj).Frame;
                trackedPar_insideRC(counter_inside).TimeStamp = trackedPar(traj).TimeStamp;
                trackedPar_insideRC(counter_inside).nuclear_trajectories = trackedPar(traj).nuclear_trajectories;
                trackedPar_insideRC(counter_inside).compartment = trackedPar(traj).compartment;
                counter_inside = counter_inside + 1;
            else
                trackedPar_outside(counter_outside).xy = trackedPar(traj).xy;
                trackedPar_outside(counter_outside).Frame = trackedPar(traj).Frame;
                trackedPar_outside(counter_outside).TimeStamp = trackedPar(traj).TimeStamp;
                trackedPar_outside(counter_outside).nuclear_trajectories = trackedPar(traj).nuclear_trajectories;
                trackedPar_outside(counter_outside).compartment = trackedPar(traj).compartment;
                counter_outside = counter_outside + 1;
            end
            counter_nuc = counter_nuc + 1;
        end
    end
    if exist('FilepathOut','var')
        save([FilepathOut,'/',fName,'_Tracked_scrambled_and_masked.mat'], 'trackedPar_nuclear', 'trackedPar_insideRC', 'trackedPar_outside', '-v7.3');
    else
        save([FilepathIn,'/',fName,'_Tracked_scrambled_and_masked.mat'], 'trackedPar_nuclear', 'trackedPar_insideRC', 'trackedPar_outside', '-v7.3');
    end
end


CompleteFlag = 1;
end