%%%%%%FAST TRACKING DATA PREPARATION%%%%%%%%%%%%%%%%%
%%%%%%%Copyright (C) 2018 David McSwiggen

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.   
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
% 
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you 
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script uses reads in and opens Nikon .nd2 files from
%%%%%%% experiments as in McSwiggen, et. al. 2018. Specifically, movies
%%%%%%% with "Before" and "After" images as reference for SPT movies, to
%%%%%%% manually annotate with ROIs.

%%%%%%% For more information, or to address specific questions, please
%%%%%%% email dmcswiggen@berkeley.edu or direct them to the corresponding
%%%%%%% authors of the McSwiggen, et. al. manuscript.


%% OPEN STACKS IN MATLAB %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
clc
close all

addpath(genpath('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT'));

input_path = '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/';
output_path = '/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-TetR/';
Number_SPT_frames = 20000;
Number_still_frames = 100; %Number of still frames on each side, assumes they're the same (i.e. 100 before, 100 after).


%% Find which files to process
%Find all the ND2 files in a folder
nd2_files=dir([input_path,'*.nd2']);
Filenames = ''; %for saving the actual file name
placeholder = 1;
for iter = 1:length(nd2_files)
    if nd2_files(iter).name(1) == '.'
        continue
    else
        Filenames{placeholder} = nd2_files(iter).name(1:end-4);
        placeholder = placeholder + 1;
    end
    
end
InputParameters = whos;

%% Load the data
for iter = 1:length(Filenames)
    if exist([input_path,Filenames{iter},'/SPT_raw_images_double.mat'],'file') > 0
        disp('-----------------------------------------------------------------')
        disp([Filenames{iter},' has already been processed.'])
        disp('Skipping this file.')
        disp('-----------------------------------------------------------------')
        continue;
        
    else
        clear('except','InputParameters');
        disp('-----------------------------------------------------------------');
        tic;
        disp(['reading in nd2 file ', num2str(iter), ' of ', num2str(length(Filenames)), ' total files']);
        
        
        %%% read nd2 files:
        [imgs_3d_double,~] = FastND2Reader([input_path, Filenames{iter}, '.nd2'],1,1,1000);
        toc;


        if size(imgs_3d_double,3) < (Number_SPT_frames + Number_still_frames*2)
            disp([Filenames{iter},' is too short, and likely was aborter early.'])
            disp('Skipping this file.')
            continue;
            
        else
            [s,mess,messid] = mkdir(output_path,Filenames{iter});
            ImHeight = size(imgs_3d_double,1);
            ImWidth = size(imgs_3d_double,2);
            
            %Separate into widefield and single-molecule stacks
            Widefield_before_tracking = max(imgs_3d_double(:,:,1:Number_still_frames-1),[],3);
            Widefield_before_tracking_uint16 = uint16(Widefield_before_tracking);
            SPT_stack = imgs_3d_double(:,:,Number_still_frames:Number_still_frames +Number_SPT_frames-1);
            Widefield_after_tracking = max(imgs_3d_double(:,:,Number_still_frames +Number_SPT_frames:end-1),[],3);
            Widefield_after_tracking_uint16 = uint16(Widefield_after_tracking);
            
            

            disp('-----------------------------------------------------------------');
            disp('Separating stacks and saving data');
            tic;
            imwrite(Widefield_before_tracking_uint16,[output_path,Filenames{iter},'/widefield_before_tracking.tif']);
            imwrite(Widefield_after_tracking_uint16,[output_path,Filenames{iter},'/widefield_after_tracking.tif']);
            save([output_path,Filenames{iter},'/SPT_raw_images_double.mat'],'SPT_stack', '-v7.3');
            toc;
            disp('-----------------------------------------------------------------');
            close all;
        end
        
    end
end

