%Batch_Process_Annotated_Directory
%%%%%%%Copyright (C) 2018 David McSwiggen

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script uses files generated in the "Fast_Tracking_Data_Prep.m"
%%%%%%% Matlab script, and performs particle localization and tracking on
%%%%%%% the SPT movies, then sorts these trajectories according to the ROIs
%%%%%%% generated.

%%%%%%% The localization and tracking are based on the Multiple Target
%%%%%%% Tracking (MTT) algorithm from:

%%%%%%% Serg�, A., Bertaux, N., Rigneault, H. and Marguet, D., 2008.
%%%%%%% Dynamic multiple-target tracing to probe spatiotemporal cartography
%%%%%%% of cell membranes. Nature methods, 5(8), p.687.

%%%%%%% This code was originally implemented by Christian Richter under the
%%%%%%% name SLIMfast, and distributed by Davide Normanno, Zhe Liu, and
%%%%%%% Mohamed El-Beheiry. Mustafa Mir and Anders S. Hansen have provided
%%%%%%% this code in a GUI-free form, which has then been implemented to
%%%%%%% incorporate the annotations from "Fast_Tracking_Data_Prep.m" and
%%%%%%% loop through all files in a directory.

%%%%%%% For more information on setting parameters, please refer to the
%%%%%%% original Serg� et al paper. For more information on this code, or
%%%%%%% to address specific questions, please email dmcswiggen@berkeley.edu
%%%%%%% or direct them to the corresponding authors of the McSwiggen, et.
%%%%%%% al. manuscript.



%% Input information %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear; clc; close all; clearvars -global; delete(gcp('nocreate'));

%   DESCRIPTION
%   This script takes in data saved in "Fast_Tracking_data_prep" in order
%   to generate single-molecule trajectories. It will use the .nd2 file
%   name to identify the correct folder to select, and then load in two
%   files: "SPT_raw_images_double.mat" and "roi_metadata.mat" and use these
%   to generate trajectories and ROI masks to exclude cytoplasmic signal.

%%%%%%%%%%%%%%%%%%%% DEFINE INPUT AND OUTPUT PATHS %%%%%%%%%%%%%%%%%%%%%%%%
% specify input path with nd2 files:
input_path=('/Volumes/DRZQ_DATA_2/DM_3_87_SPT/Halo-NLS_test/');
path_to_RC_lib = '/Users/Davidmcswiggen/Google Drive/Lab_Stuff/Lab_notebook/DM_2_70_fastSPT/InfectedAll_segmented_RC.mat';
% Path to previously annotated ROIs for randomly placing annotations in scramble control.Not necessary if not doing this step
output_path = input_path; %path to the original parent directory with all of the processed .nd2 files AND the corresponding directory

LocalizationError = -6.25; % Localization Error: -6 = 10^-6
EmissionWavelength = 664; % wavelength in nm; consider emission max and filter cutoff
ExposureTime = 7.48; % in milliseconds
NumDeflationLoops = 0; % Generaly keep this to 0; if you need deflation loops, you are imaging at too high a density;
MaxExpectedD = 10; % The maximal expected diffusion constant for tracking in units of um^2/s;
NumGapsAllowed = 0; % the number of gaps allowed in trajectories
HSV1_Infected = 1;
ScrambleControl = 1; % Will randomly impose "RC" annotations outside of real RCs.
NumberOfWorkers = 4; %Number of processors dedicated to analysis. Cannot exceed the number of physical processors available.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% DEFINE STRUCTURED ARRAY WITH ALL THE SPECIFIC SETTINGS FOR LOC AND TRACK
% imaging parameters
impars.PixelSize=0.16; % um per pixel
impars.psf_scale=1.35; % PSF scaling
impars.wvlnth= EmissionWavelength/1000; %emission wavelength in um
impars.NA=1.49; % NA of detection objective
impars.psfStd= impars.psf_scale*0.55*(impars.wvlnth)/impars.NA/1.17/impars.PixelSize/2; % PSF standard deviation in pixels
impars.FrameRate= ExposureTime/1000; %secs
impars.FrameSize= ExposureTime/1000; %secs

% localization parameters
locpars.wn=9; %detection box in pixels
locpars.errorRate= LocalizationError; % error rate (10^-)
locpars.dfltnLoops= NumDeflationLoops; % number of deflation loops
locpars.minInt=0; %minimum intensity in counts
locpars.maxOptimIter= 50; % max number of iterations
locpars.termTol= -2; % termination tolerance
locpars.isRadiusTol=false; % use radius tolerance
locpars.radiusTol=50; % radius tolerance in percent
locpars.posTol= 1.5;%max position refinement
locpars.optim = [locpars.maxOptimIter,locpars.termTol,locpars.isRadiusTol,locpars.radiusTol,locpars.posTol];
locpars.isThreshLocPrec = false;
locpars.minLoc = 0;
locpars.maxLoc = inf;
locpars.isThreshSNR = false;
locpars.minSNR = 0;
locpars.maxSNR = inf;
locpars.isThreshDensity = false;

% tracking parameters
trackpars.trackStart=1;
trackpars.trackEnd=inf;
trackpars.Dmax= MaxExpectedD;
trackpars.searchExpFac=1.2;
trackpars.statWin=10;
trackpars.maxComp=3;
trackpars.maxOffTime=NumGapsAllowed;
trackpars.intLawWeight=0.9;
trackpars.diffLawWeight=0.5;


% add the required functions to the path:
addpath('/Users/Davidmcswiggen/Documents/MATLAB/Anders_scripts/SLIMFAST_batch_fordist');
addpath('/Users/Davidmcswiggen/Documents/MATLAB/Anders_scripts/SLIMFAST_batch_fordist/bfmatlab');
disp('added paths for MTT algorithm mechanics, bioformats...');

%% Read the directory information %%
%find all nd2 files:
nd2_files=dir([input_path,'*.nd2']);
Filenames = ''; %for saving the actual file name

for iter = 1:length(nd2_files)
    Filenames{iter} = nd2_files(iter).name(1:end-4);
end

parpool('local',NumberOfWorkers)
Input_Params = whos;
%% Drift correct real annotations
% the individual files and generate masked stacks for tracking:
total_files = 1;
CompleteFlag = zeros(length(Filenames),3);
tic;
parfor iter = 1:length(Filenames)
    if exist([input_path,Filenames{iter},'/roi_metadata.mat'],'file') == 0
        disp('-----------------------------------------------------------------')
        disp([Filenames{iter},' has not been processed. Skipping this file.'])
        disp('-----------------------------------------------------------------')
        continue;
        
    elseif exist([input_path,Filenames{iter},'/RC_masks_drift_corrected.mat'],'file') > 0
        disp('-----------------------------------------------------------------')
        disp([Filenames{iter},' has been processed for drift correction. Skipping this file.'])
        disp('-----------------------------------------------------------------')
        continue;
        
    else
        
        CompleteFlag(iter,1) = DriftCorrectReal([input_path,Filenames{iter}]);
        
    end 
end
toc;

%% Generate masks with randomly generated RCs and Drift correct for nucleus movement
% the individual files and generate masked stacks for tracking:
if ScrambleControl == 1
    tic;
    parfor iter = 1:length(Filenames)
        if ~exist([input_path,Filenames{iter},'/roi_metadata.mat'],'file')
            disp('-----------------------------------------------------------------')
            disp([Filenames{iter},' has not been processed. Skipping this file.'])
            disp('-----------------------------------------------------------------')
            
        elseif exist([input_path,Filenames{iter},'/Scrambled_masks_drift_corrected.mat'],'file') > 0
            disp('-----------------------------------------------------------------')
            disp([Filenames{iter},' has been processed for drift correction. Skipping this file.'])
            disp('-----------------------------------------------------------------')
            
        else
        CompleteFlag(iter,2) = DriftCorrectScramble([input_path,Filenames{iter}], path_to_RC_lib);
        end
    end
    toc;
end

%% Track particles and generate trajectories using MTT (Ander's and Mustafa's batch code)

filenumber = 1;
parfor iter = 1:length(Filenames)
    if ~exist([input_path,Filenames{iter},'/Nucleus_mask_drift_corrected.mat'],'file')
        disp('-----------------------------------------------------------------');
        disp(['File ', Filenames{iter}, ' was not processed. Skipping...']);
        
    else
        CompleteFlag(iter,3) = MTTLocalizeTrackSort(input_path,impars,locpars,trackpars,Filenames{iter});
    end
end

