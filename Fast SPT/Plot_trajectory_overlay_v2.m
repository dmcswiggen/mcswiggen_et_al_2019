%%%%%%%%%%%% Plot overlay of trajectories on widefield image %%%%%%%%%%%%%%
clear all; close all; clc
addpath(genpath('/Users/Davidmcswiggen/Documents/MATLAB/FastSPT'));

input_path = '/Volumes/LaCie_McS/Imaging/DM_2_70_fastSPT/PlotThis/';
output_path = '/Users/Davidmcswiggen/Google Drive/Lab_Stuff/Lab_notebook/DM_2_99_fastSPT/PAA_TRP_FLV_overlays/';
HSV_infected = 1;
PixelSize = 0.160;
minimum_traj_length = 3;

Inside_color_name = 'Greens';
Outside_color_name = 'Greys';
Transition_color_name = 'Greens';

% InsideRC_color = [256, 256, 256]; %RGB vector
% OutsideRC_color = [68, 68, 151]; %RGB vector
InsideRC_color = [127, 201, 127]; %RGB vector
OutsideRC_color = [151,151,151]; %RGB vector
binaryColorMap = colormap([256,256,256;OutsideRC_color; InsideRC_color]./256);
%binaryColorMap = colormap([linspace(InsideRC_color(1),OutsideRC_color(1),256)',linspace(InsideRC_color(2),OutsideRC_color(2),256)',linspace(InsideRC_color(3),OutsideRC_color(3),256)']./256);
close();

screen_size_vector = get(0,'ScreenSize');
% make sure the plots do not exceed the screen size:
plot_width = min([1200 screen_size_vector(3)]);
plot_height = min([800 screen_size_vector(4)]);

%% Read in all workspaces
nd2_files=dir([input_path,'*.nd2']);
Tracked_cells = ''; %for saving the actual file name
Widefield_image = '';
Filenames = '';
keep_file = 1;
for file = 1:length(nd2_files)
    if nd2_files(file).name(1) == '.'
        continue
    else
        Filenames{keep_file} = nd2_files(file).name(1:end-4);
        Tracked_cells{keep_file} = [input_path,nd2_files(file).name(1:end-4),'/',nd2_files(file).name(1:end-4),'_Tracked_and_masked.mat'];
        Widefield_image{keep_file} = [input_path,nd2_files(file).name(1:end-4),'/widefield_before_tracking.tif'];
        ROI_info{keep_file} = [input_path,nd2_files(file).name(1:end-4),'/roi_metadata.mat'];
        keep_file = keep_file + 1;
    end
end
Include = ones(1,length(Tracked_cells));
%% Sort the trajectories into three general structured arrays: inside, outside, or crossing
tic; disp(['loading in the data...']);

for dataset=1:length(Tracked_cells)
    tic;
    load(Tracked_cells{dataset});
    widefield = imread(Widefield_image{dataset});
    ImHeight = size(widefield,1);
    ImWidth = size(widefield,2);
    
    %% Plot the mask outline
    load(ROI_info{dataset});
    nucROI_x = roi_info_nuc{1,1,1};
    nucROI_y = roi_info_nuc{1,2,1};
    Nuc_masks = poly2mask(nucROI_x, nucROI_y,ImHeight,ImWidth);
    
    number_of_RC = size(roi_info_RC,1);
    temp_RC_mask = zeros(ImHeight,ImWidth,number_of_RC);
    for RC = 1:number_of_RC
        RC_ROI_x = roi_info_RC{RC,1,1};
        RC_ROI_y = roi_info_RC{RC,2,1};
        temp_RC_mask(:,:,RC) = poly2mask(RC_ROI_x, RC_ROI_y,ImHeight,ImWidth);
    end
    
    temp_RC_all_masks = sum(temp_RC_mask,3);
    temp_RC_all_masks = temp_RC_all_masks > 0;
    temp_RC_all_masks = imfill(temp_RC_all_masks, 'holes');
    RC_mask_binary = temp_RC_all_masks;
    Nuc_outside_binary = and(Nuc_masks,~RC_mask_binary);
    All_mask = Nuc_masks + RC_mask_binary;
    
    figure2 = figure('position',[100 100 3*ImWidth 3*ImHeight]);
    imagesc(All_mask)
    colormap(binaryColorMap)
    title('Nuclear and RC masks annotated for cell');
    set(figure2,'Units','Inches');
    pos = get(figure2,'Position');
    set(figure2,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(figure2,[input_path,Filenames{dataset},'/Mask_annotations.pdf'],'-dpdf','-r0');
    print(figure2,[output_path,'Mask_',Filenames{dataset},'.svg'],'-dsvg','-painters');
    
    
    
    
    %% Calculate the mean number of localizations per frame
    only_inside_traj = struct;
    boundary_cross_traj = struct;
    only_outside_traj = struct;
    
    FrameVect = zeros(20000,1);
    for i = 1:length(trackedPar_nuclear)
        for j = 1:length(trackedPar_nuclear(i).Frame)
            FrameVect(trackedPar_nuclear(i).Frame(j)) = FrameVect(trackedPar_nuclear(i).Frame(j)) + 1;
        end
    end
    
    for i = 1:length(trackedPar_nuclear)
        traj_length(i) = length(trackedPar_nuclear(i).compartment);
    end
    longest_traj = max(traj_length);
    traj_counts = histcounts(traj_length,[1:1:longest_traj+1],'Normalization','cumcount');
    traj_CDF = histcounts(traj_length,[1:1:longest_traj+1],'Normalization','cdf');
    traj_inverse_CDF = 1-traj_CDF;
    CDF_min_val = traj_inverse_CDF(longest_traj - 1);
    
    %Build individual trajs with XY coords
    if length(trackedPar_insideRC) > 1
        for traj = 1:length(trackedPar_insideRC)
            traj_length = length(trackedPar_insideRC(traj).Frame);
            compartment_sum = sum(trackedPar_insideRC(traj).compartment);
            if traj_length >= minimum_traj_length
                if traj_length == compartment_sum
                    if isfield(only_inside_traj, 'xy')
                        only_inside_traj = horzcat(only_inside_traj, trackedPar_insideRC(traj));
                    else
                        only_inside_traj = trackedPar_insideRC(traj);
                    end
                else
                    if isfield(boundary_cross_traj, 'xy')
                        boundary_cross_traj = horzcat(boundary_cross_traj, trackedPar_insideRC(traj));
                    else
                        boundary_cross_traj = trackedPar_insideRC(traj);
                    end
                end
            end
        end
    end
    
    for traj = 1:length(trackedPar_outside)
        traj_length = length(trackedPar_outside(traj).Frame);
        if traj_length >= minimum_traj_length
            if isfield(only_outside_traj, 'xy')
                only_outside_traj = horzcat(only_outside_traj, trackedPar_outside(traj));
            else
                only_outside_traj = trackedPar_outside(traj);
            end
            
        end
    end
    
    %% Generate a colormap for each set
    size_outside = length(only_outside_traj);
    size_inside = length(only_inside_traj);
    size_crossing = length(boundary_cross_traj);
    
    colormap_outside = cbrewer2(Outside_color_name,size_outside*3,'cubic');
    colormap_inside = cbrewer2(Inside_color_name,size_inside*3,'cubic');
    colormap_crossing = cbrewer2(Transition_color_name,size_crossing*3,'cubic');
    
    %% Loop through all trajectories and plot them over the original image
    %figure1 = figure('position',[100 100 min([1600 screen_size_vector(3)]) min([1200 screen_size_vector(4)])]);
    figure1 = figure;
    imshow(widefield, [],'InitialMagnification',400);
    axis image
    hold on
    if isfield(only_outside_traj, 'xy')
        for traj = 1:size_outside
            x_coords = only_outside_traj(traj).xy(:,1) ./ PixelSize;
            y_coords = only_outside_traj(traj).xy(:,2) ./ PixelSize;
            plot(x_coords(:),y_coords(:),'Color',colormap_outside(traj + floor((3/2)*size_outside),:),'LineWidth',1);
            scatter(x_coords(:),y_coords(:),4,'MarkerEdgeColor',colormap_outside(traj + floor((3/2)*size_outside),:),'MarkerFaceColor',colormap_outside(traj,:));
        end
    end
    
    if isfield(only_inside_traj, 'xy')
        for traj = 1:size_inside
            x_coords = only_inside_traj(traj).xy(:,1) ./ PixelSize;
            y_coords = only_inside_traj(traj).xy(:,2) ./ PixelSize;
            plot(x_coords(:),y_coords(:),'Color',colormap_inside(traj + 2*size_inside,:),'LineWidth',1);
            scatter(x_coords(:),y_coords(:),4,'MarkerEdgeColor',colormap_inside(traj + 2*size_inside,:),'MarkerFaceColor',colormap_inside(traj + size_inside,:));
        end
    end
    
    if isfield(boundary_cross_traj, 'xy')
        for traj = 1:size_crossing
            x_coords = boundary_cross_traj(traj).xy(:,1) ./ PixelSize;
            y_coords = boundary_cross_traj(traj).xy(:,2) ./ PixelSize;
            plot(x_coords(:),y_coords(:),'Color',colormap_crossing(traj + size_crossing,:),'LineWidth',1);
            scatter(x_coords(:),y_coords(:),4,'MarkerEdgeColor',colormap_crossing(traj + size_crossing,:),'MarkerFaceColor',colormap_crossing(traj,:));
        end
    end
    
    title('Widefield image of Pol II with sorted trajectories overlayed');
%     print(figure1,[input_path,Filenames{dataset},'/overlayed_traj_GyGn.pdf'],'-dpdf','-r0')
%     print(figure1,[input_path,Filenames{dataset},'/overlayed_traj_GyGn.svg'],'-dsvg','-painters');
    set(figure1,'Units','Inches');
    pos = get(figure1,'Position');
    set(figure1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(figure1,[input_path,Filenames{dataset},'/overlayed_traj_GyGn.pdf'],'-dpdf','-r0')
    print(figure1,[output_path,'TrajOverlay_GyGn_',Filenames{dataset},'.svg'],'-dsvg','-painters');
    
    if length(traj_counts)> 5
        SPT_text(1) = {'Tracking statistics for trajectories'};
        SPT_text(2) = {['Mean trajectory length = ',num2str(mean(traj_length)),' frames']};
        SPT_text(3) = {['Median trajectory length = ',num2str(median(traj_length)),' frames']};
        SPT_text(4) = {['Number of trajectories longer than 3 = ',num2str(traj_counts(longest_traj) - traj_counts(3)),' trajectories']};
        SPT_text(5) = {['Number of trajectories longer than 5 = ',num2str(traj_counts(longest_traj) - traj_counts(5)),' trajectories']};
        SPT_text(6) = {['Longest trajectory = ',num2str(longest_traj),' frames']};
        
        figure3 = figure('position',[100 100 min([1600 screen_size_vector(3)]) min([1200 screen_size_vector(4)]);]);
        plot(1:longest_traj,traj_inverse_CDF,'Color',[150/255,150/255,150/255],'LineWidth',2)
        text(4, 0.1, SPT_text,'HorizontalAlignment','Left', 'FontSize',8, 'FontName', 'Helvetica');
        ax = gca;
        ax.YScale = 'log';
        ax.XLim = [1 longest_traj]; ax.YLim = [CDF_min_val 1];
        xlabel('Trajectory length (frames)', 'FontSize',12, 'FontName', 'Optima');
        ylabel('1 - CDF', 'FontSize',12, 'FontName', 'Optima');
        
        title('Tracking stats');
        set(figure3,'Units','Inches');
        pos = get(figure3,'Position');
        set(figure3,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
        print(figure3,[input_path,Filenames{dataset},'/TrackingStats.pdf'],'-dpdf','-r0')
        print(figure3,[output_path,'TrackingStats_',Filenames{dataset},'.svg'],'-dsvg','-painters');
    end
    
    
    
     %% Loop through all trajectories and plot them over the original image
%     figure4 = figure('position',[100 100 min([1600 screen_size_vector(3)]) min([1200 screen_size_vector(4)])]);
%     imshow(widefield, [],'InitialMagnification',400);
%     axis image
%     hold on
%     if isfield(trackedPar_nuclear, 'xy')
%         for traj = 1:length(trackedPar_nuclear)
%             x_coords = trackedPar_nuclear(traj).xy(:,1) ./ PixelSize;
%             y_coords = trackedPar_nuclear(traj).xy(:,2) ./ PixelSize;
%             scatter(x_coords(:),y_coords(:),4,'MarkerEdgeColor','k','MarkerFaceColor',[150/256,150/256,150/256]);
%         end
%     end
%     
%     title('Widefield image of Pol II with all trajectories overlayed');
%     set(figure4,'Units','Inches');
%     pos = get(figure4,'Position');
%     set(figure4,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
%     print(figure4,[input_path,Filenames{dataset},'/overlayed_all_traj.pdf'],'-dpdf','-r0')
%     print(figure4,[input_path,Filenames{dataset},'/overlayed_all_traj.esp'],'-depsc','-r0')
%     toc;
     close all;
end