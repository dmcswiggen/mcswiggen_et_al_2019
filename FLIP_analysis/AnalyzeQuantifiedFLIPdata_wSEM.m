%%%%%% Analyze Quantified FLIP Data %%%%%%%%%%%%%%%%%
%%%%%%%Copyright (C) 2018 David McSwiggen

% modified from AnalyzeQuantifiedFRAPdata.m, Anders Sejr Hansen, Jan 2016

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script aggregates the files from many FLIP image sequences for
%%%%%%% determining the average, variance, and model fitting.

%%
%AnalyzeQuantifiedFRAPdata.m
%David McSwiggen (edited from Anders Hansen) July 2016
clear; clc; close all; 

%Define what to analyze
AnalyzeWhat = 8;%1 = uninfected, optimus
                %2 = infected, optimus, 3-4hpi, Neighboring Replication Compartment
                %3 = infected, optimus, 3-4hpi, Nucleoplasmic bleaching
                %4 = infected, optimus, 4-5hpi, Neighboring Replication Compartment
                %5 = infected, optimus, 4-5hpi, Nucleoplasmic bleaching
                %6 = infected, optimus, 5-6hpi, Neighboring Replication Compartment
                %7 = infected, optimus, 5-6hpi, Nucleoplasmic bleaching
                %8 = Unbleached cells; Pooled samples with an unbleached cell in the field of view
DoModelFitting = 1;
FirstFitFrame =16;
SaveVal = 1;


if AnalyzeWhat == 1 %uninfected, optimus
    %Workspaces:
    Workspaces = {'Uninfected_D1_001_NeighborRC','Uninfected_D1_002_NeighborRC','Uninfected_D1_003_NeighborRC','Uninfected_D1_004_NeighborRC','Uninfected_D1_005_NeighborRC','Uninfected_D1_006_NeighborRC','Uninfected_D1_007_NeighborRC','Uninfected_D1_008_NeighborRC','Uninfected_D2_001_NeighborRC','Uninfected_D2_002_NeighborRC','Uninfected_D2_003_NeighborRC','Uninfected_D2_004_NeighborRC','Uninfected_D2_005_NeighborRC','Uninfected_D3_001_NeighborRC','Uninfected_D3_002_NeighborRC','Uninfected_D3_003_NeighborRC','Uninfected_D3_004_NeighborRC','Uninfected_D3_005_NeighborRC','Uninfected_D3_006_NeighborRC','Uninfected_D3_007_NeighborRC','Uninfected_D3_008_NeighborRC'};
    ToKeep = [1,1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,3,3,3];
    Trials = [1,2,3];
    Conditions_per_trial = [8,5,8];
    ConditionName = 'Uninfected_mean';
    
elseif AnalyzeWhat == 2 %infected, optimus, 3-4hpi, Neighboring Replication Compartment 
    %Workspaces:
    Workspaces = {'3-4hpi_D1_001_NeighborRC','3-4hpi_D1_002_NeighborRC','3-4hpi_D1_003_NeighborRC','3-4hpi_D1_004_NeighborRC','3-4hpi_D1_005_NeighborRC','3-4hpi_D1_006_NeighborRC','3-4hpi_D1_007_NeighborRC','3-4hpi_D1_008_NeighborRC','3-4hpi_D1_009_NeighborRC','3-4hpi_D2_001_NeighborRC','3-4hpi_D2_002_NeighborRC','3-4hpi_D2_003_NeighborRC','3-4hpi_D2_004_NeighborRC','3-4hpi_D2_005_NeighborRC','3-4hpi_D2_006_NeighborRC','3-4hpi_D2_007_NeighborRC','3-4hpi_D3_001_NeighborRC','3-4hpi_D3_002_NeighborRC','3-4hpi_D3_003_NeighborRC','3-4hpi_D3_004_NeighborRC','3-4hpi_D3_005_NeighborRC','3-4hpi_D3_006_NeighborRC'};
    ToKeep = [1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3,3,3];
    Trials = [1,2,3];
    Conditions_per_trial = [9,7,6];
    ConditionName = '3hpi_NeighborRC_mean';

elseif AnalyzeWhat == 3 %infected, optimus, 3-4hpi, Nucleoplasmic bleaching 
    %Workspaces:
    Workspaces = {'3-4hpi_D1_001_Nuc','3-4hpi_D1_002_Nuc','3-4hpi_D1_003_Nuc','3-4hpi_D1_004_Nuc','3-4hpi_D1_005_Nuc','3-4hpi_D1_006_Nuc','3-4hpi_D1_007_Nuc','3-4hpi_D1_008_Nuc','3-4hpi_D1_009_Nuc','3-4hpi_D2_001_Nuc','3-4hpi_D2_002_Nuc','3-4hpi_D2_003_Nuc','3-4hpi_D2_004_Nuc','3-4hpi_D2_005_Nuc','3-4hpi_D2_006_Nuc','3-4hpi_D2_007_Nuc','3-4hpi_D3_001_Nuc','3-4hpi_D3_002_Nuc','3-4hpi_D3_003_Nuc','3-4hpi_D3_004_Nuc','3-4hpi_D3_005_Nuc','3-4hpi_D3_006_Nuc'};
    ToKeep = [1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,3,3,3,3,3,3];
    Trials = [1,2,3];
    Conditions_per_trial = [9,7,6];
    ConditionName = '3hpi_Nucleus_mean';

elseif AnalyzeWhat == 4 %infected, optimus, 4-5hpi, Neighboring Replication Compartment 
    %Workspaces:
    Workspaces = {'4-5hpi_D1_001_NeighborRC','4-5hpi_D1_002_NeighborRC','4-5hpi_D1_003_NeighborRC','4-5hpi_D1_004_NeighborRC','4-5hpi_D1_005_NeighborRC','4-5hpi_D1_006_NeighborRC','4-5hpi_D1_007_NeighborRC','4-5hpi_D1_008_NeighborRC','4-5hpi_D2_001_NeighborRC','4-5hpi_D2_002_NeighborRC','4-5hpi_D2_003_NeighborRC','4-5hpi_D2_004_NeighborRC','4-5hpi_D2_005_NeighborRC','4-5hpi_D2_006_NeighborRC','4-5hpi_D2_007_NeighborRC','4-5hpi_D2_008_NeighborRC','4-5hpi_D3_001_NeighborRC','4-5hpi_D3_002_NeighborRC','4-5hpi_D3_003_NeighborRC','4-5hpi_D3_004_NeighborRC','4-5hpi_D3_005_NeighborRC','4-5hpi_D3_006_NeighborRC','4-5hpi_D3_007_NeighborRC','4-5hpi_D3_008_NeighborRC'};
    ToKeep = [1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3];
    Trials = [1,2,3];
    Conditions_per_trial = [8,8,8];
    ConditionName = '4hpi_NeighborRC_mean';

elseif AnalyzeWhat == 5 %infected, optimus, 4-5hpi, Nucleoplasmic bleaching
    %Workspaces:
    Workspaces = {'4-5hpi_D1_001_Nuc','4-5hpi_D1_002_Nuc','4-5hpi_D1_003_Nuc','4-5hpi_D1_004_Nuc','4-5hpi_D1_005_Nuc','4-5hpi_D1_006_Nuc','4-5hpi_D1_007_Nuc','4-5hpi_D1_008_Nuc','4-5hpi_D2_001_Nuc','4-5hpi_D2_002_Nuc','4-5hpi_D2_003_Nuc','4-5hpi_D2_004_Nuc','4-5hpi_D2_005_Nuc','4-5hpi_D2_006_Nuc','4-5hpi_D2_007_Nuc','4-5hpi_D2_008_Nuc','4-5hpi_D3_001_Nuc','4-5hpi_D3_002_Nuc','4-5hpi_D3_003_Nuc','4-5hpi_D3_004_Nuc','4-5hpi_D3_005_Nuc','4-5hpi_D3_006_Nuc','4-5hpi_D3_007_Nuc','4-5hpi_D3_008_Nuc'};
    ToKeep = [1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3];
    Trials = [1,2,3];
    Conditions_per_trial = [8,8,8];
    ConditionName = '4hpi_Nucleus_mean';
    
elseif AnalyzeWhat == 6 %infected, optimus, 5-6hpi, Neighboring Replication Compartment 
    %Workspaces:
    Workspaces = {'5-6hpi_D1_001_NeighborRC','5-6hpi_D1_002_NeighborRC','5-6hpi_D1_003_NeighborRC','5-6hpi_D1_004_NeighborRC','5-6hpi_D1_005_NeighborRC','5-6hpi_D1_006_NeighborRC','5-6hpi_D1_007_NeighborRC','5-6hpi_D1_008_NeighborRC','5-6hpi_D2_001_NeighborRC','5-6hpi_D2_002_NeighborRC','5-6hpi_D2_003_NeighborRC','5-6hpi_D2_004_NeighborRC','5-6hpi_D2_005_NeighborRC','5-6hpi_D2_006_NeighborRC','5-6hpi_D2_007_NeighborRC','5-6hpi_D2_008_NeighborRC','5-6hpi_D3_001_NeighborRC','5-6hpi_D3_002_NeighborRC','5-6hpi_D3_003_NeighborRC','5-6hpi_D3_004_NeighborRC','5-6hpi_D3_005_NeighborRC','5-6hpi_D3_006_NeighborRC','5-6hpi_D3_007_NeighborRC','5-6hpi_D3_008_NeighborRC','5-6hpi_D3_009_NeighborRC'};
    ToKeep = [1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3];
    Trials = [1,2,3];
    Conditions_per_trial = [8,8,9];
    ConditionName = '5hpi_NeighborRC_mean';
    
elseif AnalyzeWhat == 7 %infected, optimus, 5-6hpi, Nucleoplasmic bleaching
    %Workspaces:
    Workspaces = {'5-6hpi_D1_001_Nuc','5-6hpi_D1_002_Nuc','5-6hpi_D1_003_Nuc','5-6hpi_D1_004_Nuc','5-6hpi_D1_005_Nuc','5-6hpi_D1_006_Nuc','5-6hpi_D1_007_Nuc','5-6hpi_D1_008_Nuc','5-6hpi_D2_001_Nuc','5-6hpi_D2_002_Nuc','5-6hpi_D2_003_Nuc','5-6hpi_D2_004_Nuc','5-6hpi_D2_005_Nuc','5-6hpi_D2_006_Nuc','5-6hpi_D2_007_Nuc','5-6hpi_D2_008_Nuc','5-6hpi_D3_001_Nuc','5-6hpi_D3_002_Nuc','5-6hpi_D3_003_Nuc','5-6hpi_D3_004_Nuc','5-6hpi_D3_005_Nuc','5-6hpi_D3_006_Nuc','5-6hpi_D3_007_Nuc','5-6hpi_D3_008_Nuc','5-6hpi_D3_009_Nuc'};
    ToKeep = [1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3];
    Trials = [1,2,3];
    Conditions_per_trial = [8,8,9];
    ConditionName = '5hpi_Nucleus_mean';
    
elseif AnalyzeWhat == 8 %Unbleached cells; Pooled samples with an unbleached cell in the field of view
    %Workspaces:
    Workspaces = {'3-4hpi_D1_005_NoBleach','3-4hpi_D1_008_NoBleach','4-5hpi_D1_002_NoBleach','4-5hpi_D1_005_NoBleach','4-5hpi_D1_007_NoBleach','5-6hpi_D1_002_NoBleach','Uninfected_D1_002_NoBleach','Uninfected_D1_007_NoBleach','4-5hpi_D2_003_NoBleach', '4-5hpi_D2_006_NoBleach', '5-6hpi_D2_001_NoBleach', 'Uninfected_D2_003_NoBleach'};
    ToKeep = [1,1,1,1,1,1,1,1,1,1,1,1];
    Trials = [1,2,3];
    Conditions_per_trial = [8,8,9];
    ConditionName = 'No_bleach';    
end

length(Workspaces)
length(ToKeep)
n_cells = length(ToKeep);


    %Load the data
    iter = 1;
    figure('position',[100 100 1400 1000]); %[x y width height]
    for i=1:length(Workspaces)
        if ToKeep(i) >= 1
            load(['./FRAP_workspaces_v2/', Workspaces{i}]);
           if iter == 1
                %FRAP_data = Init_norm_smallFRAP;
                xtime = FLIP_time;
                FLIP_bleach = yFLIP_bleach;
                FLIP_outside = yFLIP_outside;
                Distance_bt_regions = Region_dist;
                Bleach_Fit_A = Bleach_fit_A;
                Bleach_Fit_B = Bleach_fit_B;
                Bleach_Fit_ka = Bleach_fit_ka;
                Bleach_Fit_kb = Bleach_fit_kb;
                Outside_Fit1_A = Outside_fit1_A;
                Outside_Fit1_ka = Outside_fit1_ka;
                Outside_Fit1_R2 = Outside_fit1_R2;
                Outside_Fit_params = zeros(length(ToKeep),4);
                Outside_Fit_params(i,1) = Outside_fit1_A;
                Outside_Fit_params(i,2) = Outside_fit1_ka;
                Outside_Fit_params(i,3) = Outside_fit1_R2;
                Outside_Fit_params(i,4) = Region_dist;
                AnalysisRadii = AnalysisRadius;
                Bleach_fit_line = Bleach_Fit_A(i)*exp(-Bleach_Fit_ka(i)* xtime) + Bleach_Fit_B(i)* exp(-Bleach_Fit_kb(i)* xtime);
                Outside_fit_line = Outside_Fit1_A(i)*exp(-Outside_Fit1_ka(i)* xtime);
%                 if Time_to_50 < 4000
%                     Time_to_50_percent = Time_to_50;
%                 else
%                     Time_to_50_percent = NaN;
%                 end

            else
                %FRAP_data = vertcat(FRAP_data, Init_norm_smallFRAP);
                FLIP_bleach = horzcat(FLIP_bleach, yFLIP_bleach);
                FLIP_outside = horzcat(FLIP_outside,yFLIP_outside);
                Distance_bt_regions = vertcat(Distance_bt_regions,Region_dist);
                Bleach_Fit_A = vertcat(Bleach_Fit_A, Bleach_fit_A);
                Bleach_Fit_B = vertcat(Bleach_Fit_B, Bleach_fit_A);
                Bleach_Fit_ka = vertcat(Bleach_Fit_ka, Bleach_fit_ka);
                Bleach_Fit_kb = vertcat(Bleach_Fit_kb, Bleach_fit_kb);
                Outside_Fit1_A = vertcat(Outside_Fit1_A, Outside_fit1_A);
                Outside_Fit1_ka = vertcat(Outside_Fit1_ka, Outside_fit1_ka);
                Outside_Fit1_R2 = vertcat(Outside_Fit1_R2, Outside_fit1_R2);
                Outside_Fit_params(i,1) = Outside_fit1_A;
                Outside_Fit_params(i,2) = Outside_fit1_ka;
                Outside_Fit_params(i,3) = Outside_fit1_R2;
                Outside_Fit_params(i,4) = Region_dist;
                Outside_Fit_params_min = [min(Outside_Fit_params(:,1)),min(Outside_Fit_params(:,2)),min(Outside_Fit_params(:,3)),min(Outside_Fit_params(:,4))];
                Outside_Fit_params_max = [max(Outside_Fit_params(:,1)),max(Outside_Fit_params(:,2)),max(Outside_Fit_params(:,3)),max(Outside_Fit_params(:,4))];
                AnalysisRadii = vertcat(AnalysisRadii, AnalysisRadius);
                Bleach_fit_line = horzcat(Bleach_fit_line, Bleach_Fit_A(i) * exp(-Bleach_Fit_ka(i)  * xtime) + Bleach_Fit_B(i)  * exp(-Bleach_Fit_kb(i)  * xtime));
                Outside_fit_line = horzcat(Outside_fit_line, Outside_Fit1_A(i) * exp(-Outside_Fit1_ka(i)  * xtime));
%                 if Time_to_50 < 4000
%                     Time_to_50_percent = vertcat(Time_to_50_percent, Time_to_50);
%                 else
%                     Time_to_50_percent = vertcat(Time_to_50_percent, NaN);
%                 end

            end
            iter = iter + 1;
        
        subplot(5,7,i);
        hold on;
        %plot(time, Init_norm_smallFRAP, 'ko', 'MarkerSize', 3);
        plot(xtime, yFLIP_bleach, 'ro', 'MarkerSize', 3);
        plot(xtime, yFLIP_outside, 'bo', 'MarkerSize', 3);
        title([num2str(i), ': FRAP for ', Workspaces{i}], 'FontSize',8, 'FontName', 'Helvetica');
        ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
        xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
        legend('small circle', 'Location', 'SouthEast');
        axis([min(xtime) max(xtime) 0 1.2]);
        hold off;
        
        end
        
        
    end
    
    
iter = 1;
current_val = 1;
prev_val = 0;
FLIP_bleach_rep_mean = zeros(length(xtime),length(Trials));
FLIP_outside_rep_mean = zeros(length(xtime),length(Trials));
for i=1:length(Workspaces)
    current_val = ToKeep(i);
    if current_val == prev_val
        FLIP_bleach_rep_array = horzcat(FLIP_bleach_rep_array, yFLIP_bleach);
        FLIP_outside_rep_array = horzcat(FLIP_outside_rep_array, yFLIP_outside);
        FLIP_bleach_rep_mean(:,ToKeep(i)) = mean(FLIP_bleach_rep_array,2);
        FLIP_bleach_rep_std(:,ToKeep(i)) = std(FLIP_bleach_rep_array,1,2);
        FLIP_outside_rep_mean(:,ToKeep(i)) = mean(FLIP_outside_rep_array,2);
        FLIP_outside_rep_std(:,ToKeep(i)) = std(FLIP_outside_rep_array,1,2);
    else
        FLIP_bleach_rep_array = yFLIP_bleach;
        FLIP_outside_rep_array = yFLIP_outside;
    end
    prev_val = ToKeep(i);
    iter = iter + 1;
end

    
median_Bleach_fit_line = (median(Bleach_Fit_A))*exp(-(median(Bleach_Fit_ka))* xtime) + (median(Bleach_Fit_B))* exp(-(median(Bleach_Fit_kb))* xtime);
median_Outside_fit_line = (median(Outside_Fit1_A))*exp(-(median(Outside_Fit1_ka))* xtime);
% median_Time_to_50 = median(Time_to_50_percent,2,'omitnan');
% std_Time_to_50 = std(Time_to_50_percent,1,2,'omitnan');


figure('position',[100 100 450 300]); %[x y width height]
hold on;
for i=1:length(Workspaces)
    plot(xtime, Outside_fit_line(:,i), 'k--', 'LineWidth', 1);
    plot(xtime, Bleach_fit_line(:,i), 'k-', 'LineWidth', 1);
end
plot(xtime, median_Bleach_fit_line, 'k-', 'LineWidth', 3,'Color', [237/255, 28/255, 36/255]);
plot(xtime, median_Outside_fit_line, 'k--', 'LineWidth', 3,'Color', [237/255, 28/255, 36/255]);
title(['Individual fits for ', num2str(length(ToKeep)), ' cells,', ConditionName], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(xtime) max(xtime) 0 1.2]);
hold off;


%PLOT the average data
transTime = transpose(xtime);
meanFLIP_bleach = transpose(mean(FLIP_bleach, 2));
stdFLIP_bleach = transpose(std(FLIP_bleach,0,2));
SEM_FLIP_bleach = transpose(std(FLIP_bleach,0,2)/sqrt(length(Workspaces)));
meanFLIP_outside = transpose(mean(FLIP_outside, 2));
stdFLIP_outside = transpose(std(FLIP_outside,0,2));
SEM_FLIP_outside = transpose(std(FLIP_outside,0,2)/sqrt(length(Workspaces)));

if DoModelFitting == 1
    %Do a simply two-exponential fitting:
    %FRAP(t) = 1 - A*exp(-ka*t) - B*exp(-kb*t)
    
    %Variable for fitting
    [xtime, meanFLIP_bleach] = prepareCurveData(xtime,meanFLIP_bleach);
    [xtime, meanFLIP_outside] = prepareCurveData(xtime,meanFLIP_outside);

    fit_mean_bleach = fittype('A*exp(-ka*x) + B*exp(-kb*x)');
    [TwoExp_mean_bleach, TwoExp_param] = fit(xtime, meanFLIP_bleach, fit_mean_bleach, 'Lower', [0 0 0 0], 'Upper', [inf inf inf inf], 'StartPoint', [0.5 0.5 0.5 0.001]); 
    TwoExp_CI = confint(TwoExp_mean_bleach);
    fit_mean_bleach_A = TwoExp_mean_bleach.A;
    fit_mean_bleach_B = TwoExp_mean_bleach.B;
    fit_mean_bleach_ka = TwoExp_mean_bleach.ka;
    fit_mean_bleach_kb = TwoExp_mean_bleach.kb;

    
    fit_mean_outside = fittype('A*exp(-ka*x)');
    [OneExp_mean_outside, TwoExp_param] = fit(xtime, meanFLIP_outside, fit_mean_outside, 'Lower', [0 0], 'Upper', [inf inf], 'StartPoint', [0.5 0.5]); 
    TwoExp_CI = confint(OneExp_mean_outside);
    fit_mean_outside_A = OneExp_mean_outside.A;
    fit_mean_outside_ka = OneExp_mean_outside.ka;
    
    yFit_bleach = TwoExp_mean_bleach.A .* exp(-TwoExp_mean_bleach.ka .* xtime) + TwoExp_mean_bleach.B .* exp(-TwoExp_mean_bleach.kb .* xtime);

    yFit_outside = OneExp_mean_outside.A .* exp(-OneExp_mean_outside.ka .* xtime);
   

figure('position',[100 100 450 300]); %[x y width height]
hold on;
errorbar(xtime, meanFLIP_bleach, stdFLIP_bleach, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 2);
errorbar(xtime, meanFLIP_outside, stdFLIP_outside, 'o', 'Color', [40/255, 240/255, 180/255], 'MarkerSize', 2);
plot(xtime, yFit_bleach, 'k-', 'LineWidth', 3);
plot(xtime, yFit_outside, 'k--', 'LineWidth', 3);
title(['FLIP for ', num2str(length(ToKeep)), ' cells +/- STD'], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(xtime) max(xtime) 0 1.2]);
legend('mean with error', 'smoothed mean', 'Location', 'SouthEast');
legend boxoff;
hold off;

figure('position',[100 100 450 300]); %[x y width height]
hold on;
errorbar(xtime, meanFLIP_bleach, SEM_FLIP_bleach, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 2);
errorbar(xtime, meanFLIP_outside, SEM_FLIP_outside, 'o', 'Color', [40/255, 240/255, 180/255], 'MarkerSize', 2);
plot(xtime, yFit_bleach, 'k-', 'LineWidth', 3);
plot(xtime, yFit_outside, 'k--', 'LineWidth', 3);
title(['FLIP for ', num2str(length(ToKeep)), ' cells +/- SEM'], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(xtime) max(xtime) 0 1.2]);
legend('mean with error', 'smoothed mean', 'Location', 'SouthEast');
legend boxoff;
hold off;


figure('position',[100 100 1400 1000]); %[x y width height]
hold on;
lineProps.width = 1;
lineProps.col ={[0/255, 153/255, 204/255],[237/255, 28/255, 36/255], [0/255, 161/255, 75/255], [102/255, 45/255, 145/255], [157/255, 157/255, 54/255]};
mseb(transTime, meanFLIP_bleach, stdFLIP_bleach,lineProps,1)
lineProps.col ={[237/255, 28/255, 36/255]};
mseb(transTime, meanFLIP_outside, stdFLIP_outside,lineProps,1)
plot(xtime, yFit_bleach, 'k-', 'LineWidth', 1);
plot(xtime, yFit_outside, 'k--', 'LineWidth', 1);
title(['FLIP of Adjacent RC fit to 2 exponential decay', num2str(length(ToKeep)),' cells'], 'FontSize',10, 'FontName', 'Helvetica');
ylabel('fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
axis([min(xtime) max(xtime) 0 1.2]);
legend('Bleached Region', 'Adjacent Replication Compartment','2exp fit to bleach', '1exp fit to RC', 'Location', 'NorthEast');
legend boxoff;
hold off;

end

%%%%%%%%%%%%%%%%%%%%%%%%%% SAVE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if SaveVal == 1;
    clearvars -except ConditionName SEM_FLIP_outside SEM_FLIP_bleach meanFLIP_bleach stdFLIP_bleach meanFLIP_outside stdFLIP_outside xtime fit_mean_bleach_A fit_mean_bleach_B fit_mean_bleach_ka fit_mean_bleach_kb fit_mean_outside_A fit_mean_outside_ka yFit_bleach yFit_outside AnalysisRadii Distance_bt_regions Outside_Fit1_A Outside_Fit1_ka Outside_Fit1_R2 Outside_Fit_params Outside_Fit_params_min Outside_Fit_params_max Time_to_50_percent median_Time_to_50 std_Time_to_50
    
    save(['./FRAP_workspaces_v2/', ConditionName, '.mat']);
    MakeMovie = 0;
end
