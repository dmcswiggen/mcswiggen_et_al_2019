%%%%%% FLIP Quant %%%%%%%%%%%%%%%%%
%%%%%%%Copyright (C) 2018 David McSwiggen

% modified from AnalyzeQuantifiedFRAPdata.m, Anders Sejr Hansen, Jan 2016

%%%%%%%%%%%%%%%%%%%%%%%% GNU LICENSE OVERVIEW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or any later
% version.
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
%
% If you modify this Program, or any covered work, by linking or combining it
% with Matlab or any Matlab toolbox, the licensors of this Program grant you
% additional permission to convey the resulting work.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, please see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%% This script requires .tiff files of FLIP image sequences. One
%%%%%%% manually identifies the center pixel of the bleach spot, the center of the spot they want to analyze,
%%%%%%% the nuclear intensity, and a background pixel. Then one manually accounts 
%%%%%%% for drift by changing the "movement" vector. The inputs here match the raw data from McSwiggen et at 2019 FLIP data.

%%

%SegmentQuantifyMovie_v2.m
%Anders Sejr Hansen, Jan 2016
clear; clc; close all;

%The purpose of this movie is to analyze FRAP data and quantify the
%intensity of a circular bleached region that is moving a little bit, using
%just manual adjustment of the circle movement. 
%imshow(images(:,:,1), []);

%VERSION 2 - SEGMENT THE WHOLE NUCLEUS
%Use inital fluorescence in the bleach spot region to normalize the final
%recovery - use a slightly larger circle to correct for noise. Use an extra
%radius of:
ExtraRadius = 0;
%Adjust the intensity threshold used across the movie to account for
%photobleaching. Assume ~18% photobleach over the 300 frames.
ExpFactor = 5; %exp(-endFrame/(ExpFactor*endFrame) = 0.8187

%Use WhatToDo = 1 to find BleachCentre. Use WhatToDo = 2 to quantify BleachCentre Movement
WhatToDo = 0;
SaveVal = 1; %Change to 1 if you want to save

%Muller, Wach, McNally, BioPhys J, 2008: Can ignore Gaussian shape of photobleach spot when the recovery is slow. 

SampleName = 'Uninfected_D1_001';
filename = 'Uninfected_Halo-rpb1-jf549_250msec_8pxbleach_001';
fn_suffix = '.tif';
file = strcat(filename,fn_suffix);
MakeMovie = 0;

%Define the FRAP circle:
FirstBleachFrame = 16;
FrameRate = 4; 
BleachRadius = 12; 
AnalysisRadius = 8;
NewTime = 0;
%Define FRAP Cirlce movement
FrameUpdate = 40; %Update every 40th frame

%For some of the U2-OS movies, the background was hidden. Need to adjust
%the background manually (Otherwise FRAP recovery is negative in first
%time-point). Use:
U2OS_background = 0;

path = '/Volumes/DARZACQ_1/Imaging/DM_2_15_FLIP/Tiff_stacks/Day_1/';

%SAMPLE SPECIFIC INFORMATION

%U2OS, uninfected, Day 1
if strcmp(SampleName, 'Uninfected_D1_001');
    display(SampleName);
    BleachCentre = [215, 296];
    AnalysisCentre = [171,250];
    IntThres = 40; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [440, 230];
    Movement = [0,0; 0,0; 0,0; 0,0; 0,0;
                0,0; 0,0; 0,0; 0,0; 0,0;
                0,0; 0,0; 0,0; 0,0; 0,0;
                0,0; 0,0; 0,0; 0,0; 0,0;
                0,0; 0,0; 0,0; 0,0; 0,0];

elseif strcmp(SampleName, 'Uninfected_D1_002');
    display(SampleName);
    BleachCentre = [357, 348];
    AnalysisCentre = [250,250];
    IntThres = 50; %define an intensity threshold for the whole nucleus
    BackgroundCentre = [487, 382];
    Movement = [0,0; 0,0; 0,0; -1,0; 0,0; 0,0; 0,0; -1,0; 0,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; 0,0; -1,0; 0,0; -1,0; 0,0; -1,0; 0,0; -1,0; 0,0; -1,0; 0,0];



else
    error('SampleName was not found. Please check your SampleName again');
end 
%%%%%%%%%%%%%% LOAD THE DATA %%%%%%%%%%%%%



%Open the file using the BioFormats package
%temp = bfopen([path, file]);
[stack, nbImages] = tiffread([path, file]);

%Convert to a matrix
%images = zeros(size(data{1,1},1), size(data{1,1},2), size(data,1));
images = zeros(stack(1,1).height, stack(1,1).width, size(stack,2));
ImHeight = stack(1,1).height; ImWidth = stack(1,1).width; 
for i=1:size(images,3)
    images(:,:,i) = stack(1,i).data;
end
if NewTime == 0
    time = -(FirstBleachFrame-1)/FrameRate:1/FrameRate:(size(images, 3)-FirstBleachFrame)/FrameRate;
    timepoint = time.*4;
end

%Make smoothed images for segmentation of the nucleus
%Perform gaussian smoothing
smooth_filter = fspecial('gaussian', [6 6], 2);
smooth_images = zeros(stack(1,1).height, stack(1,1).width, size(stack,2));
for i=1:size(images,3)
    smooth_images(:,:,i) = imfilter(images(:,:,i), smooth_filter);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%WhatToDo = 1: determine bleach region
if WhatToDo == 1
    %Define the circle:
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    BW_bleach_circle = sqrt((rr-BleachCentre(1,1)).^2+(cc-BleachCentre(1,2)).^2)<=BleachRadius;
    BW_outside_circle = sqrt((rr-AnalysisCentre(1,1)).^2+(cc-AnalysisCentre(1,2)).^2)<=AnalysisRadius;
    %imshow(BW_circle) 
    figure('position',[100 100 700 250]); %[x y width height]
    subplot(2,2,1);
    imshow(images(:,:,FirstBleachFrame), []);
    
    subplot(2,2,3);
    overlay1 = imoverlay(mat2gray(images(:,:,FirstBleachFrame)), bwperim(BW_bleach_circle), [.3 1 .3]);
    imagesc(overlay1);
    title('Matching a circle to the bleach region');
    
    subplot(2,2,4);
    overlay2 = imoverlay(mat2gray(images(:,:,FirstBleachFrame)), bwperim(BW_outside_circle), [.3 1 .3]);
    imagesc(overlay2);
    title('Matching a circle to the analysis region');
    
    %Make circles for background
    %Threshold to identify the nucleus
    Nucleus_binary = smooth_images(:,:,FirstBleachFrame) > IntThres*exp(-FirstBleachFrame/(ExpFactor*length(time)));
    %Now fill in any small holes in the middle of the nucleus:
    Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
    subplot(2,2,2);
    overlay2 = imoverlay(mat2gray(images(:,:,FirstBleachFrame)), bwperim(Nucleus_binary_filled), [1 0 0]);
    imagesc(overlay2);
    title('Nucleus segmentation');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%WhatToDo = 2: Determine Bleach Movement
if WhatToDo == 2
    FLIP_circle = zeros(1, round(size(images,3)/FrameUpdate));
    Outside_circle = zeros(1, round(size(images,3)/FrameUpdate));
    %Figure out how much the bleach circle moved
    for n=1:length(FLIP_circle)
        CumMove = Movement(1:n,:); %The cumulative movement of the circle
        CurrCentre = AnalysisCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
        currBackgroundCentre = BackgroundCentre;
        
        %Update the circle
        [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
        BW_bleach_circle = sqrt((rr-BleachCentre(1,1)).^2+(cc-BleachCentre(1,2)).^2)<=BleachRadius;
        BW_outside_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=AnalysisRadius;

        
        %Find the pixels inside the bleach circle:
        [row,col,val] = find(BW_bleach_circle); clear val
        Bleach_vals = zeros(1,length(row));
        for i=1:length(row)
            Bleach_vals(1,i) = images(row(i), col(i), n*FrameUpdate);
        end
        FLIP_circle(1,n) = mean(Bleach_vals);
        
        %Find the pixels inside the analysis circle:
        [row2,col2,val] = find(BW_outside_circle); clear val
        Outside_vals = zeros(1,length(row2));
        for i=1:length(row2)
            Outside_vals(1,i) = images(row2(i), col2(i), n*FrameUpdate);
        end
        Outside_circle(1,n) = mean(Outside_vals);
        
        %Plot the circle fit
        figure('position',[100 100 500 250]); %[x y width height]
        subplot(1,2,1);
        overlay1 = imoverlay(mat2gray(images(:,:,n*FrameUpdate)), bwperim(BW_bleach_circle), [.3 1 .3]);
        imagesc(overlay1);
        title(['FRAP spot in frame ', num2str(n*FrameUpdate)]);
        
        %Threshold to identify the nucleus
        Nucleus_binary = smooth_images(:,:,n*FrameUpdate) > IntThres*exp(-n*FrameUpdate/(ExpFactor*length(time)));
        %Now fill in any small holes in the middle of the nucleus:
        Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
        
        subplot(1,2,2);
        overlay1 = imoverlay(mat2gray(images(:,:,n*FrameUpdate)), bwperim(BW_outside_circle), [.3 1 .3]);
        imagesc(overlay1);
        title(['FRAP spot in frame ', num2str(n*FrameUpdate)]);
        
        print('-dpng',['./images/', filename, '_FRAME_', num2str(n), '.png' ]);
        close;
    end
    figure; 
    hold on;
    plot(1:length(FLIP_circle), FLIP_circle);
    plot(1:length(Outside_circle), Outside_circle);
    hold off;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DETERMINE THE INITIAL FLUORESCENCE IN THE BLEACH AREA %%%
InitBleachSpotIntensity = zeros(1,FirstBleachFrame-1);
for n = 1:FirstBleachFrame-1
    %Update the circle
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    InitSpot_circle = sqrt((rr-BleachCentre(1,1)).^2+(cc-BleachCentre(1,2)).^2)<=BleachRadius;
    %Find pixels
    [row,col,val] = find(InitSpot_circle); clear val
    InitSpot1_vals = zeros(1,length(row));
    for i=1:length(row)        
        InitSpot1_vals(1,i) = images(row(i), col(i), n);
    end
    InitBleachSpotIntensity(1,n) = mean(InitSpot1_vals);
end


%%% DETERMINE THE INITIAL FLUORESCENCE IN THE ANALYSIS AREA %%%
InitOutsideSpotIntensity = zeros(1,FirstBleachFrame-1);
for n = 1:FirstBleachFrame-1
    CumMove = Movement(1:max([round(n/FrameUpdate) 1]),:); %The cumulative movement of the circle
    CurrCentre = AnalysisCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    %Update the circle
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    InitSpot_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=BleachRadius+ExtraRadius;
    %Find pixels
    [row,col,val] = find(InitSpot_circle); clear val
    InitSpot2_vals = zeros(1,length(row));
    for i=1:length(row)        
        InitSpot2_vals(1,i) = images(row(i), col(i), n);
    end
    InitOutsideSpotIntensity(1,n) = mean(InitSpot2_vals);
end

%%%%%% PLOT FRAP CURVES %%%%
Bleach_intensity = zeros(1, size(images, 3));
Fluor_decay = zeros(1, size(images, 3));
Outside_intensity = zeros(1, size(images, 3));
Outside_decay = zeros(1, size(images, 3));
Nuc_intensity = zeros(1, size(images, 3)); 
Black_intensity = zeros(1, size(images, 3));
log_timepoint = unique(floor(logspace(0, log10(length(timepoint)), 233)))-(FirstBleachFrame);
log_time = log_timepoint.*0.25;

for n = 1:size(images, 3)
    CumMove = Movement(1:max([round(n/FrameUpdate) 1]),:); %The cumulative movement of the circle
    CurrCentre = AnalysisCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    %currBackgroundCentre = BackgroundCentre - [sum(CumMove(:,1)), sum(CumMove(:,2))];
    currBackgroundCentre = BackgroundCentre;
    
    %Update the circle
    [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
    BW_bleach_circle = sqrt((rr-BleachCentre(1,1)).^2+(cc-BleachCentre(1,2)).^2)<=BleachRadius;
    BW_outside_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=AnalysisRadius;
    Black_circle = sqrt((rr-BackgroundCentre(1,1)).^2+(cc-BackgroundCentre(1,2)).^2)<=BleachRadius;
    %Find the pixels inside the circle:
    [row,col,val] = find(BW_bleach_circle); clear val
    Bleach_vals = zeros(1,length(row));
    for i=1:length(row)        
        Bleach_vals(1,i) = images(row(i), col(i), n);
    end
    Bleach_intensity(1,n) = mean(Bleach_vals);
    Fluor_decay(1,n) = InitBleachSpotIntensity(1,15)./(InitBleachSpotIntensity(1,15)*exp(-n/(ExpFactor*length(time))));
    
    [row2,col2,val] = find(BW_outside_circle); clear val
    Outside_vals = zeros(1,length(row2));
    for i=1:length(row2)        
        Outside_vals(1,i) = images(row2(i), col2(i), n);
    end
    Outside_intensity(1,n) = mean(Outside_vals);
    
    %Threshold to identify the nucleus
    Nucleus_binary = smooth_images(:,:,n) > IntThres*exp(-n/(ExpFactor*length(time)));
    %Now fill in any small holes in the middle of the nucleus:
    Nucleus_binary_filled = imfill(Nucleus_binary, 'holes');
    
    
    [nuc_row,nuc_col,val] = find(Nucleus_binary_filled); clear val
    [row4,col4,val] = find(Black_circle); clear val
    nuc_vals = zeros(1,length(nuc_row)); 
    Black_vals = zeros(1,length(row4));
    for i=1:length(nuc_row)        
        nuc_vals(1,i) = images(nuc_row(i), nuc_col(i), n);
    end
    for i=1:length(row4)        
        Black_vals(1,i) = images(row4(i), col4(i), n);
    end
    Nuc_intensity(1,n) = mean(nuc_vals);
    if U2OS_background > 0
        Black_intensity(1,n) = mean(Black_vals) - U2OS_background; 
    else
        Black_intensity(1,n) = mean(Black_vals);
    end
end
%PLOT the results
figure('position',[200 200 800 450]); %[x y width height]
subplot(1,3,1);
hold on;
plot(time, Bleach_intensity, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, Outside_intensity, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);
plot(time, Black_intensity, 'ko', 'MarkerSize', 6);
plot(time, Nuc_intensity, 'o', 'Color', [100/255, 151/255, 75/255] , 'MarkerSize', 6);

plot(time(FirstBleachFrame:end), Bleach_intensity(FirstBleachFrame:end), '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 1);
plot(time(FirstBleachFrame:end), Outside_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Black_intensity(FirstBleachFrame:end), 'k-', 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back1_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back2_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
%plot(time(FirstBleachFrame:end), Back3_intensity(FirstBleachFrame:end), '-', 'Color', [0/255, 161/255, 75/255], 'LineWidth', 1);
legend('FRAP: r=10', 'FRAP: r=6', 'Black bg', 'whole Nuc', 'Location', 'NorthEast');
axis([min(time) max(time) 0 1.55*max(Bleach_intensity)]);
title('Raw FRAP quantification', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('Halo-mCTCF TMR fluorescence (AU)', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%% PERFORM ANALYSIS AND PHOTOBLEACHING CORRECTION %%%%%%%%%%%%%%%%

%Rationale for the analysis: first of all, subtract the black background.
%Second, use the total nuclear fluorescence to correct for
%photobleaching and reduction in cell fluorescence overall. Furthermore, the cell 
%moves a bit so it is important to take into account that cell total and
%local fluorescence can change a bit. In some of the movies, cell moves
%quite a bit so this is important to do. 

%So do the following:
%1. subtract black background from all. 
%2. Adjust values for photobleaching DUE TO THE SCANNING LASER (measured
%from an unbleached cell)

%1. SUBTRACT BLACK BACKGROUND
bsFLIP_intensity = Bleach_intensity - Black_intensity;
bsOutside_intensity = Outside_intensity - Black_intensity;
bsNuc_intensity = Nuc_intensity - Black_intensity;
InitBleachSpotIntensity = InitBleachSpotIntensity - Black_intensity(1:FirstBleachFrame-1);
InitOutsideSpotIntensity = InitOutsideSpotIntensity - Black_intensity(1:FirstBleachFrame-1);

%3. ADJUST FOR PHOTOBLEACHING

adjust_FLIP = bsFLIP_intensity ./smooth(Fluor_decay, 3)';
adjust_Outside = bsOutside_intensity ./smooth(Fluor_decay, 3)';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%% PLOT THE ADJUSTED FLIP CURVES %%%%%%%%%%%%%%%%%%%%%%%
subplot(1,3,2);
hold on;
plot(time, adjust_FLIP, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, adjust_Outside, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);

plot(time(FirstBleachFrame:end), smooth(adjust_FLIP(FirstBleachFrame:end),7)', '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 2);
plot(time(FirstBleachFrame:end), smooth(adjust_Outside(FirstBleachFrame:end),7)', '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 2);
legend('Bleach Spot', 'Analyzed Region',  'Location', 'NorthEast');
axis([min(time) max(time) 0 1.05*max(adjust_Outside)]);
title('Background-subtracted FLIP', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('Relative JF549 fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;


%%%%%%%%%%%%%%%%%%%%% NORMALIZE BASED ON INITIAL FLUORESCENCE %%%%%%%%%%%%%%%%%%%%%%%
Init_norm_Bleach_Spot = adjust_FLIP./mean(InitBleachSpotIntensity);
Init_norm_Outside_Spot = adjust_Outside./mean(InitOutsideSpotIntensity);

subplot(1,3,3);
hold on;
plot(time, Init_norm_Bleach_Spot, 'o', 'Color', [237/255, 28/255, 36/255], 'MarkerSize', 6);
plot(time, Init_norm_Outside_Spot, 'o', 'Color', [0/255, 153/255, 204/255] , 'MarkerSize', 6);
plot([min(time) max(time)], [1 1], 'k--', 'LineWidth', 1);
plot(time(FirstBleachFrame:end), smooth(Init_norm_Bleach_Spot(FirstBleachFrame:end),7)', '-', 'Color', [237/255, 28/255, 36/255], 'LineWidth', 2);
plot(time(FirstBleachFrame:end), smooth(Init_norm_Outside_Spot(FirstBleachFrame:end),7)', '-', 'Color', [0/255, 153/255, 204/255], 'LineWidth', 2);
legend('Bleached Spot', 'Analyzed Region',  'Location', 'NorthEast');
axis([min(time) max(time) 0 1.15]);
title('Normalized FLIP', 'FontSize',10, 'FontName', 'Helvetica');
ylabel('relative JF549 fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
hold off;



%%%%%%%%%%%%%%%%%%%%%%%%%% SAVE THE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if SaveVal == 1;
    clearvars -except AnalysisRadius Black_intensity Bleach_Radius Bleach_intensity FirstBleachFrame Fluor_decay InitBleachSpotIntensity InitOutsideIntensity Init_norm_Bleach_Spot Init_norm_Outside_Spot Outside_intensity adjust_FLIP adjust_Outside bsFLIP_intensity bsOutside_intensity bsNuc_intensity log_time log_timepoint time timepoint
    
    save(['./FRAP_workspaces_v2/', SampleName, '.mat']);
    MakeMovie = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%% MAKE A MOVIE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if MakeMovie == 1
    %Adjust CumMove:
    FullCumMove = zeros(length(time), 2);
    for i=1:size(CumMove,1)
        CurrIndex = FrameUpdate + (i-1)*FrameUpdate;
        FullCumMove(CurrIndex,:) = CumMove(i,:);
    end
    prebleach = mean(adjust_Outside(1:FirstBleachFrame-1));
    adjust_smallFRAP2 = [adjust_Outside(1:FirstBleachFrame-1)./prebleach adjust_Outside(FirstBleachFrame:end)];
           
    %Figure out how much the bleach circle moved
    for n=1:size(FullCumMove,1)
        %Update the FRAP circle centre
        CurrCentre = BleachCentre - [sum(FullCumMove(1:n,1)), sum(FullCumMove(1:n,2))];

        %Update the circle
        [rr cc] = meshgrid(1:ImWidth,1:ImHeight);
        BW_bleach_circle = sqrt((rr-CurrCentre(1,1)).^2+(cc-CurrCentre(1,2)).^2)<=BleachRadius;
        
        %Plot the circle fit
        figure('position',[100 100 650 250]); %[x y width height]
        subplot(1,2,1);
        overlay1 = imoverlay(mat2gray(images(:,:,n)), bwperim(BW_bleach_circle), [.3 1 .3]);
        imagesc(overlay1);
        title(['Halo-mCTCF FRAP image at ', num2str(n-FirstBleachFrame), ' seconds']);
        set(gca,'XTick',[]); set(gca,'YTick',[]);
        
        subplot(1,2,2);
        hold on;
        plot(time(1:n), Init_norm_Outside_Spot(1:n), 'o', 'Color', [237/255, 28/255, 36/255] , 'MarkerSize', 6);
        axis([min(time) max(time) 0 1.15]);
        title('Quanfied FRAP recovery', 'FontSize',10, 'FontName', 'Helvetica');
        ylabel('relative TMR fluorescence', 'FontSize',10, 'FontName', 'Helvetica');
        xlabel('time (seconds)', 'FontSize',10, 'FontName', 'Helvetica');
        hold off;
        print('-dpng', '-r300',['./images/', filename, '_FRAP_Movie_', num2str(n), '.png' ]);
        close;
    end
end